Conescan
--------

A conescan is a possibly useful procedure when setting up a single
crystal diffraction experiment. The first thing which needs to be done
when starting a single crystal experiment is the determination of the UB
matrix. In order to do that at least two reflections need to be located
through a search procedure. Now, consider the situation when one
reflection has been found and indexed. Then it is known that other
reflections can be found on a cone with an opening angle determined by
the lattice parameters and the indices of the target reflection around
the first (center) reflection. The SICS conescan module allows to do
just that, do a scan around a given center reflection. The syntax is:

conescan list

lists all the parameters of the conescan

conescan cell

prints the cell constants.

conescan cell a b c alpha beta gamma

sets new cell constants.

conescan center

prints the current values for the center reflection

conescan center h k l

uses h, k, l as the indices of the center reflection. Motor positions
are read from motors.

conescan center h k l stt om chi phi

defines a center position complete with all angles.

conescan target

prints the current target for the conescan

conescan target h k l

defines the target indices for the conescan.

conescan qscale

prints the Q scale for the conescan. The conescan module calculates the
length of then scattering vector from the lattice parameters and the
indices. When the lattice constants are only approximatly known it might
be useful to vary the scattering vector length for the conescan a
little. This can be doen with the qscale factor.

conescan qscale value

sets a new value for the qscale factor

conescan run step mode preset

starts a conescan with the nstep width step, the couent mode mode and
the preset preset.

conescan run

starts a conescan with defaults: step = .5, mode = monitor, preset =
10000

This is the simple usage of the conescan. In fact cone is implemented as
a virtual motor. This means that arbitray scans can be performed on cone
as with any other motor. As with any other motor, cone can also be
driven to a cone angle.

### Implementation Reference

The conescan commands are just wrapper routines around the cone and
ubcalc module which actually work together to implement the conescan.
The ubcalc module, documented elsewhere, holds the cell constants and
the center reflection.

The cone module does the actual cone calculation. Cone can be configured
with commands:

cone center

prints the number of the reflection in ubcalc to use as a center.

cone center num

sets the reflection in ubcalc to use a the center for driving on the
cone. Set to 0 to use the first reflection in ubcalc.

cone target

prints the target reflection indices for the cone.

cone target h k l

sets the target reflection indices.

cone qscale

prints the current value of the scattering vector scale.

cone qscale val

sets a value for the scaling factor for the scattering vector. Values
should be close to 1.0;
