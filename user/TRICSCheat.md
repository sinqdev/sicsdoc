### Four Circle Cheat Sheet

#### Setup

| Command              |Command                       |Command                |
|----------------------|------------------------------|-----------------------|
|projectdir [dirname]  | title [newtitle]             |  user [username]|
|phone [phoneno]       |address [newaddress]          | sample [samplename] |
|lambda [val]          |cell [a b c alpha bet gamma]  |spgrp [HM-symbol] |


#### TRICS Special Commands

|Command             |Command      |
|------------------|-----------------------|
|  instrmode [bi nb tas] |  detmode [single|area] |

#### Driving and Scanning

|Command       | Command                            |
|--------------|------------------------------------|
|drive motor value |            drive hkl h k l      |
|sscan mot start end np preset  | cscan mot center step np preset|


#### Motors

* h
* k
* l
* cone
* stt
* om
* chi
* phi
* nu
 
#### Reflection List


| Command                                   |Command               | Command                       |
|-----------------------------|---------------------------|---------------------------------------|
|  refclear                   |   reflist                 |    refsave filename                     |
|  refload filename            |   refindex               |     refdel id                            |
|  refadd ang [stt om chi phi] |  refadd idx hk k l        |   refadd idxang h k l [stt om chi phi ]  |
|  refhkl id h k l             |  refang id [stt om ch phi] |                                         |
       

#### Centering

    centerref preset [mode]   

    centerlist preset [mode]


#### Reflection Search and Indexing

|Command                                        |Command                                       |Command                       |
|-------------------------------------------------|------------------------------------------|------------------------------|
|  confsearch [min2t step2t max2t stepchi stepphi] |   confsearchnb [min2t max2t stepom stepnu] |  search preset maxpeak [mode] |
|  indexhkl [stt]                                  |  coneconf [id th tk tl [qscal]]            | findpeaksinscan               |
|  indexdirax                                      |                                             

#### UB Matrix

|Command                  |Command                   |Command                                  |
|-------------------------|--------------------------|-----------------------------------------|
|  ubcalc id1 id2 [id3]   |ubrefine [cell]           | ubshow                                 |
|  loadub                 |ubrec                   | ub [u11 u12 u13 u21 u22 u23 u31 u32 u33] |
|  hkltoang h k l          |angtohkl [stt om chi phi]  |                                          |


#### Data Collection

|Command                                   |Command                                     |Command            |Command                        |
|------------------------------------------|--------------------------------------------|-------------------|-------------------------------|
|  tablist                                 |  tabclear                                  | tabdel no         | tabadd sttend scanvar step np preset |
|  loadx filename                          |  hkllimit [hmin kmin lmin hmax kmax lmax]  | hklgen [sup]      | icmgen hw kw lw                        |
|  testx [del][sym]                        |  hklsort                                   | hklsave filename  | hkllist                              |
|  collconf [countmode weak weakthreshold] |  collect [skip]                            |                   |                                      |


