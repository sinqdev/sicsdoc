MAD Commands in SICS
====================

The TASMAD program (hereafter referred to as MAD) is used to control all
spectrometer activity on triple-axis instruments. It includes motor
movement, scans in Q-E space and, where appropriate, control of power
supplies, flippers, temperature, etc.

Terminology and Conventions
---------------------------

In this chapter of the documentation, the following terms are used:

\<xxxx\>

This has two functions. One is to indicate the key with the label
*xxxx*, e.g. \<Return\> indicates the key labelled *Return*. The other
is to indicate a parameter field which should be substituted, e.g.
*\<\>* could represent *lnsw02.psi.ch* or whatever.

*Enter ...*

The word *enter*, when used in connection with something which must be
typed on the keyboard such as command, indicates that the action must be
terminated by hitting the \<Return\> or \<Enter\> key.

*Hit ...*

The word *hit* indicates that the single key (or key combination)
indicated should be pressed once. The action should **not** be
terminated by hitting the \<Return\> or \<Enter\> key.

Examples

Whenever examples are shown, anything which is actually typed by the
user is shown *like this*. It will generally be shown in lower case.
E.g.

       sc a1=0 da1=1 np=6

indicates that everything between the *s* and *6*, inclusive, is to be
typed by the user.

Optional Arguments

Square brackets, [ ], indicate optional arguments .

Syntax
------

        There are a few rules :

          - An input line must always begin with a command. 
          - A command MUST always be abbreviated to its first TWO relevant letters, 
            e.g. CO is equivalent to COU or COUNT  
            but for SetZero type SZ or SZERO and so on.
          - A command must be separated from information which follows on the same 
            line by at least one space.
          - Only one command may be given per line of input. 
            However this does not exclude using, for example, the DRIVE command
            to drive several or even all motors.
          - All command lines are terminated by a carriage return <CR>. So do not 
            forget to put one <CR> ( or more) at the end of your jobfile.

        Most commands are followed either by:

        Syntax type (A) 

        (A)     a sequence of variable names 
        e.g. DM,DA,SS (carry out command given on variables DM, DA, SS)
        Note : that for this type of syntax (type A) the only acceptable 
        variable separators are ' ' (i.e. a space), ',' and '-' (' ' and ',' 
        are equivalent).


        Syntax type (B) 

        (B)  a sequence of variable names and values 
        e.g. AS=3.24,CC=90 (AS is set to value 3.24 and CC to 90)
        e.g. QH=1,0,2.0 (variable QH takes the value 1 and the following 
        variables in storage [QK, QL] take the values 0 and 2 )
        e.g. QH=1,0,2.0,AS=3.24,CC=90 (a combination of the above)
        
        In commands involving this construction type (B) the program echoes
        the variable names and values it has understood. 
        Possible separators are ',' and ' ' ('space')

        There is a third type of commands which requires no parameters. These 
        commands are:-
            AUTO, EXIT, HELP, LIST, LL, LE, LL, LM, LS, LT, LZ, LD, PAL,
            SAVE and SWITCH.

Commands
--------


    CL  CLear         : Unfixes one or more motors or power supplies.
    CO  COunt         : Counts for given preset TIme or MoNitor.
    DO  DO            : Runs a jobfile without testing the syntax first.
    DR  DRive         : Changes a variable and drives spectrometer to
                its new position.
    FI  FIx           : Fixes a given motor or power supply, FIx without
                argument will give a list of fixed motors and
                power supplies.
    FM  FindMax       : As FindZero but SetZero is not performed, the 
                spectrometer is only driven to the maximum.
    FZ  FindZero      : Scans a simple variable, finds maximum, drives to 
                maximum and performs a SetZero with the given
                value.
    LI  LIst          : Listing of variables and parameters.
                LE ListEnergies    Energies, k and Q values.
                LL ListLimits      Limits and zeros. Same as lz.
                LZ ListZero        Limits and zeros. Same as ll.
                LM ListMach        Machine parameters.
                LS ListSample      Sample parameters.
                LT ListTargets     Targets and positions.
    LO  LOg           : Controls terminal logging.
    OF  OFf           : Turns flipper off.
    ON  ON            : Turns flipper on.
    OU  OUtput        : Defines output variables.
    PA  Pol.An.       : Defines a polarization analysis file (default
                file ext'n is .PAL).
    PR  PRint         : Prints one ore more variables or parameters.
    SC  SCan          : Scans a variable with given or previously
                defined increment, number of points and
                time interval or monitor count.
    SE  SEt           : Sets a parameter value.
    FS  ScanFast      : Scans a variable quickly.
    SW  SWitch        : Sets some switches.
    SZ  SetZero       : Sets the zero point offset of a variable. 

### CLEAR


        CL(EAR) : The CLEAR command un-fixes a previously fixed motor or power
        supplies. Issued alone it un-fixes all previously fixed motors and 
        power supplies. CLEAR is a command with type A syntax. In all cases 
        the motors or supplies which have been cleared are listed by THE 
        Program.

        e.g.     CL I3,RA,I4<CR>
             CL<CR>

### COUNT


        CO(UNT) : Counts for a given preset TIme or MoNitor.
        This is a command of type B syntax. If the command is issued alone, 
        the preset used will be that most recently set. However, the preset 
        may also be specified on the same line as the COUNT command.
        (For use of COnt in a P.A. file, see SCan and PA).

        e.g.     CO TI=10<CR> (count for 10 seconds)
             CO MN=100<CR>    (count for 100 M1 counts)
             CO<CR>

### DO


        DO : Runs a jobfile without testing the syntax.
        The jobfile system enables a series of commands to be executed in 
        sequence without the operator being present.
         The DO command is identical to the RUN command except that the 
        'dry run', i.e. the syntax and limit violation  check, is not performed.
        Those who like forests prefer the DO command to RUN

         e.g.  DO MYJOB<CR>

        The DO command is also used to change monochromators automatically , 
        respectively to change zeroes and d-spacing after a (manual!) change 
        of the analyzer. For each type of monochromator or analyzer available 
        there is a .JOB file.
        After changing the monochromator (analyzer), do not forget to drive the
        incident (final) wavevector to the required value as this is not done 
        in the .JOB file.
        For analysers, do not forget to SEt SA (sense of diffusion on the 
        analyser) to the required value as it may have been changed by the job 
        file in order to set the proper zero of A5.

         e.g.    DO PG002M<CR> for graphite 002 monochromator
             DO HEUSLA<CR> for heusler analyzer

         A list of available analyzers and monochromators can be found in the 
        userguide, or be obtained by typing on  any terminal connected to the 
        instrument computer "DIR/PROT .JOB".

### DRIVE


        DR(IVE) : Changes variables which describe the spectrometer 
        configuration in some way. These variables have always a current 
        position (actual value) and a target value. Under normal circumstances, 
        targets and positions are equal within a small error.

        Definition of wavevectors : Ki, Q, and Kf are defined by the current 
        values of the Two-Theta angles , A2, A4, A6 respectively. Therefore, 
        it is possible (after an abort or an explicit motor drive) that the 
        spectrometer is not really at the given position in Q-energy space, 
        but every unperturbed drive of Q-energy will restore the correct 
        position in Q-energy space: that means that all the four variables Qh, 
        Qk, Ql and EN will be driven (explicitly or implicitly) to their 
        target values if (at least) one of them is explicitly driven or scanned.

        Motor angles, wavevectors, energies, temperature, power-supply and 
        Helmholtz-field values must all be driven.
        DRIVE is a command of type B syntax. The spectrometer is driven to its
        new position and the appropriate variable is altered in the memory.

        A DRIVE command will fail (non destructively) if:
        - a motor or power supply is protected or fixed
        - a software or hard limit is exceeded; the soft limits may be changed 
          if necessary using the SET command provided the value desired is 
          within the allowed range.
        - there is ambiguity among the driven variables.
          e.g.   DR KI=2.662,A2=40<CR>
             sets two different targets for A2 and fails.


        Examples of valid DRive commands: 
             DR A1=10,A2=20<CR> 
            move A1 to 10 and A2 to 20 degs

             DR EI=14<CR> 
            moves A1 and A2 to give an incident energy of 14 whichever 
            unit was chosen at start-up.

             DR QH=1,0,0,0<CR> 
            moves the spectrometer at fixed KI or KF depending on the 
            value of FX - so that QH=1,QK=0, QL=0 and EN=0

        If one of QH, QK, QL and EN is driven, ALL the others are taken into 
        consideration.
        e.g.     DR EN=5<CR> 
            uses the value of EN specified and the existing target values 
            of QH, QK and QL and makes the appropriate 
            spectrometer movements taking account of the value of FX.
        It is thus a good habit to specify all four variables (QH, QK, QL
        & EN) each time one or several of them is to be changed.

### FIX


        FI(X) : Fixes a simple variable
        (i.e. a variable that is directly connected with the  position of a 
        spectrometer motor , or the value of a current)
        The variable is fixed at its current value so that subsequent attempts 
        to change the value will fail unless a CLEAR command is issued first.
        FIX is a command of type A syntax. The FIX command  issued with no 
        variable name gives a list of motors and supplies which are fixed.

        e.g.     FI A3<CR>

### FindMaximum

        FM : Find Maximum

        This is the same as FindZero except that the zero of the
        variable is not changed at the end of the scan.

        The syntax is the same as for SCAN.

### FindZero

        FZ : Find Zero

        Scans a simple variable, finds the maximum (or minimum)
        intensity within the scanned region, drives to the
        maximum and then sets the zero of the scanned variable so
        that the zero-corrected current position of the variable
        is zero.

        More precisely, the command drives to the centre of gravity
        of the intensity distribution. The routine for finding the
        extremum is not that good and MAD frequently finds
        no extremum even when there is obviously one. In that case
        the variable is left at the last point of the scan.

        Example:

          MAD > fz a4 0 da4 0.2 np 7 ti 5

        The syntax is as for SCAN.

### LIST

        LI(ST) : LIST gives a listing of all spectrometer variables and 
             parameters. There are "subsidiary" commands 
             LE, LL, LM, LS, LT and LZ as described below.

        LE : List Energies: Ei,ki,Ef,kf,QH,QK,QL,EN,QM,TT,TRT

        LL : List Limits. Gives a listing of all motor limits and zeros.
            For clarity, only non-zero zero-offsetss are displayed.
            The current motor position is also shown.

        LM : List Machine. Gives a listing of all machine parameters. 

        LS : List Sample. Gives a listing of all sample parameters. 

        LT : Lists Targets. Gives a listing of all target values (i.e.
            where the motors ought to be) and actual positions of
            the motors. If a motor has a zero-offset, this is also
            listed. The positions of disabled motors are indicated
            by "-".

        LD : List Diaphragms: Gives a listing of all diaphragm
            positions.

        LZ : Lists Zeros. Equivalent to LL (ListLimits).

        Note:   The target values usually describe the actual
            spectrometer configuration and should be equal
            (within a certain tolerance) to the positions.
            Clear exceptions are for a power supply which has
            been turned disabled, the abort of a DRive via
            Interrupt and, for instance, the incident wavevector
            after a drive of A1 or A2.

### LOG

It is possible for the user's terminal dialogue with MAD to be logged to
disk file. Disk logging goes via the auxilliary process, NSWITCH. This
process must therefore be active for logging to be possible. The
commands which control disk logging are:

        LOg start : Starts the logging of the MAD terminal
                dialogue to file.
        LOg stop  : Stops  the logging of the MAD terminal
                dialogue to file.
        LOg new   : Closes the current log file (if open)
                and opens a new log file. 

### ON/OFF


        ON and OF(F) : Both flippers may be turned ON or OFF. In all  cases, 
        the OFF command simply turns off the relevant power supplies. Thus

             OFF F1<CR> 
            turns off flipper 1

        If a flipper is already on, the ON command will have no effect.If it is
        off, the ON command will cause the most recent target values to be 
        achieved.

        -For flippers F1 and F2, the OFF command sets the currents in both 
        the vertical and horizontal-field coils of the flipper to zero.
        
        The ON command yields the following currents:
        F1:vertical-field current = IF1V
           horizontal-field current = KI*IF1H
        F2:vertical-field current = IF2V
           horizontal-field current = KF*IF2H

        The quantities IF1V etc can be changed by the SET command when 
        appropriate values have been determined from calibration scans. 
        Note that MAD Program automatically divides the horizontal current for
        you by the incident or final wavevector respectively.

        If the current in one of the supplies connected to a flipper is changed
        by an explicit DRIVE command of the power supply, the flipper may have 
        the logical value OFF even though the currents in its coils are 
        non-zero.(This is because it no longer behaves as a flipper.)

        Note that the ON and OFF commands are the only ones which can be used 
        to change F1 and F2. Both ON and OFF are of type A syntax.

### OUT


        OU(TPUT) :Defines extra variables to be output.
        The teletype and disk-file output produced by a SCAN command usually 
        consists of values of variables which are 
        scanned in addition to values of M1, M2, time and detector counts.
        The OUTPUT command may be used to force the output of additional
        variables. The total number of variables which can be output 
        (including those which are scanned and those which are referred to in 
        a .PAL file but excluding monitor, time and detector counts) is ten 
        (10) . Thus setting a variable to be output means that its value will 
        be printed for every point in every scan until disabled.
        Typing OU with NO following variables will stop the output of ALL 
        variables apart from scanned ones.
        Type A syntax. A variable that has to be output because it is scanned 
            and has also been selected with the OUT command will only be output 
            once.

        e.g.     OU A3,A4<CR>
            A3 & A4 will be printed in addition to the scan variables.
             OU<CR> Stops all extra output

### PA


        PA (Polarization Analysis): This command allows a polarization  analysis
        loop to be carried out for each point in a scan. The commands for the 
        polarization analysis loop are contained in a file whose default 
        extension is .PAL .Thus the command:

             PA POLL<CR>

        causes the commands in file POLL.PAL to be executed for each subsequent
        scan. If no file name is given after the PA command, MAD Program asks 
        for one.

        If several scans are to be run from the console with the same 
        polarization analysis loop the 'PA loop retention' switch should be set 
        to ON (see SWITCHES). In a jobfile every SCAN command should always be 
        preceded by its own PA command.

        A .PAL file may contain ONLY the commands SE, CO, DR, ON and OFF i.e. 
        those commands which refer to flippers, Helmholtz coils and counting.


         -For example, if POLL.PAL contains:
        
         OFF F1,F2 <CR>
         DR HX=5,0,5 <CR>
         CO MN=200 <CR>
         ON F1 <CR>
         CO MN=20 <CR>
         DR HX=0 10 0 <CR>
         OFF F1 <CR>
         CO MN=500 <CR>
         ON F1 <CR>
         CO MN=50 <CR>

         four counts will be carried out for each scan point:

        # cnt   F1  F2  HX  HY  HZ  MN
        (i) off off 5   0   5   200
        (ii)    on  off 5   0   5   20
        (iii)   off off 0   10  0   500
        (iv)    on  off 0   10  0   50

        A preset monitor or time given in a .PAL file overrides that given in 
        a SCAN command for which the .PAL file is executed.
        When the command PA is issued, the contents of the relevant .PAL file 
        is listed at the terminal and its syntax is checked.

        A .PAL file is cancelled after a scan, except for scans from the 
        terminal if the PA retention switch is set.
        Therefore, if scans are performed within a job file, the .PAL file 
        required must be specified for each scan. 

        When a .PAL file is executed, the position variables used in the file 
        are included in the scan output at the terminal and in the .SCN file if 
        space permits. Preference is given to variables defined in the SCAN 
        command but .PAL file variables take precedence over variables 
        requested by the OUTPUT command (see OUTPUT).

        A concatenated version of the .PAL file is written on lines 10 and 11 
        of the .SCN file. Up to 240 characters can be written in this way 
        (see section VI).

        When a .PAL file is used, scan point output to the terminal or the .SCN
        file are labelled 1.1,1.2,1.3....1.n where n is the number of count 
        instructions in the .PAL file.
        A .PAL file may not contain more than 6 COUNT commands.


### PRINT


        PR(INT) : Prints the current value of one or more variables or  
        parameters. PRINT is a command of type A syntax:

        e.g.     PR A1,A5<CR>
             PR QH-EN,GM<CR>

### SCAN


        SC(AN) : Scans a variable. All variables which may be driven may also 
        be scanned.

        There are three major items to be known about the use of the scan 
        command
        1) syntax :
        2) data files 
        3) scan output 

        1) Syntax :

        The SCan command is of type B syntax . The scan-increment, preset 
        monitor (MN) (or time, TI) and the number of scan points (NP) may be 
        specified by a SET command (done before the scan), by default (the most
        recently used values) or on the same line as the SCAN command. Any 
        number (less than 10) of variables may be scanned  in a single command.
        The value of the scanned variable given in the SCAN command is  the 
        CENTRAL point of the scan.
        The maximum number of points per scan is 100.
        For odd NP this means that the centre is at the middle of the scan; 
        for even NP the centre is the first point after the middle


        example:    SC A1=0,DA1=1,NP=3  --> A1= -1, 0, +1
            SC A1=0,DA1=1,NP=6  --> A1=-3, -2, -1, 0, 1, +2

        2) data files : 

        
        All tas####.dat files are copied to the mainframe computer 
            automatically.

        3) Scan output :

        The SCAN command has type B syntax. The values of M1, M2, counting time 
        and detector counts are printed on the teletype for each point as are 
        the scanned variables (i.e. the variables given in the SCan command 
        line).
         Any additional variables requested (see OUTPUT ) are also printed.

        When a scan terminates, MAD Program examines the data and tries to find
        the centre of a peak or dip which may have ocurred in the detector.
        The method involves moments of the measured count distribution and may
        not be reliable if the peak/dip is ill-defined or if there is a sloping
        background. Specially the width is calculated from the first and second
        moment and is not necessary identical to the FWHM.
        For information on scans involving polarization analysis loops see the 
        PA command.
        4) examples :

        e.g.     SC A2=-40,DA2=0.1,NP=11,TI=10<CR> 
            scans A2, in steps of 0.1�, about A2=-40 degrees for 10 
            seconds per point.
             SC A3=20.2,A4=40.4,DA3=-0.1,DA4=-0.2<CR>
            gives a theta-two-theta scan.
            If NP and TI have not been changed since the example above, 
            there will be 11 points each counted for 10 seconds.

             SC QH=1,0,0,0,DQH=0,0,0,0.1,NP=31,MN=100<CR>
            causes a constant-Q scan, with an energy step of 0.1 
            (meV or THz) depending on the start-up conditions)
            to be carried out at Q=(1,0,0). There are 31 points each 
            counted for 100 monitor counts. If the user now types

             SC EN=1.1<CR>
            The constant-Q scan will be repeated, centered at an energy 
            transfer of 1.1 (meV or THz).

        As with the DRIVE command, scans in Q-E space are carried out at fixed 
        KI (FX=1) or fixed KF (FX=2). During a scan with Kf fixed (i.e.FX=2) 
        the program will automatically check and adjust A5 and A6; for Ki 
        fixed (FX=1) however, MAD Program will not adjust at check and adjust 
        at every point A1 and A2 because these variables are not likely to 
        move in a Ki-fix scan.

             SC EI=14,DEI=0.1,NP=9,TI=1<CR>
            causes a scan of the incident energy to be performed.Such a 
            scan may be carried out independently of the value of FX.

### SET


        SE(T) : This command is used to change the values of parameters which 
        do not directly alter the spectrometer configuration  (variables which 
        do alter the spectrometer configuration must be changed with the DRIVE
        command).
        The parameters which may be changed using SET are given in section V; 
        in general these are instrument parameterssuch as monochromator and 
        analyzer d-spacings, sample parameters, motor limits and zeroes, and 
        steps for scans
         
        MAD Program echoes the values which it has understood. SET is a 
        command of type B syntax.

        e.g.        SE DM=3.355,DA=3.355<CR>
                sets dM and dA to 3.355 �

### FS

**ScanFast**

Scans a simple variable quickly. The variable is driven from start to
end without stopping and measurements are made on the fly. Only one
simple parameter may be scanned and it moves at the speed as set up in
the parameters in the motor controller.

The parameters are similar to those for SCAN. The TI parameter sets the
period between reads of the neutron counter. Reading stops when the
motor stops moving. The values of NP and the motor increment are used
merely to calculate the start and end points of the scan. At a later
date, the syntax may be improved to allow these values to be specified
directly.

Example:

        FS A1=6,DA1=1,NP=13,TI=2   --> A1 = 0 to +12 with
                        readings every 2 secs.

All of the data is output to a disk file as with the SCAN command.

Any additionally requested variables (see OUTPUT) are also output.

### SETZERO


        SZ : (SetZero.) This command sets the zero for a variable such that 
        its current value of the zer point offset is change into the specified value.
        Obviously this command works only for variables that have a zero.
        e.g.     PR A3
            A3  -45.42
             SZ A3= 45
            Old values for A5 Lower=-182.11 Upper= 125.00 Zero=  25.00
                          Posn= - 45.42 Target= - 45.40
            New values for A5 Lower=-162.11 Upper= 145.00 Zero=  45.00
                          Posn= - 20.42 Target= - 20.40

### SWITCHES

        SW(ITCHES) : This command allows the user to set the switches
        described below. In response to the command SW, MAD
        generates output of the following form:
        
        1 Powder Mode                       OFF
        2 Polarization mode                 OFF
        Give Switch Number to change or RETURN to finish >

        To change a value of one switch, enter the appropriate number
        (from 1 to 2) and hit <Return>. To make no change, type
        only <Return>. Please note, that due to a bug in the Macintosh 
           JDK at least two characters have to be entered which can be spaces.

        To change the value of switches without being prompted, issue
        a command of the form:

            sw <sn> <val> <sn> <val> ...

        where <sn> is a switch number in the range 1 to 2 and <val>
        is its new value. <val> may be either On, Off or Flip.

### Examples


    Example of phonon scan:

     SC QH=2.1 3.2 0 12 DQH=0 0 0 .1 NP=11 MN=1000
     this means that the scan will be centered at hkl=2.1,3.2,0  and omega=12, 
        with steps of .1 in omega and 11 points with monitor 1000

     SC QH=2.1 3.2 0 12 DQH=.02 0 0 0 NP=11 MN=1000
     scan centered at same position but scanned in the QH-direction with 
        steps of 0.02

        N.B.!!!

        1. Always define increments for scanned variables (as DQH,DQK,DQL and 
        DEN in example above) unless you know what you are doing (i.e MAD
        Program stores the old value of the steps, and will use these by 
        default in a new scan, unless you define new steps).

        2. The incoming neutron wavevector is defined in MAD Program by A2 and
        the monochromator d-spacing, under normal circumstances A1 should have 
        the correct (i.e.half the value of A2)  position, if not MAD Program 
        will issue a warning message; the  same holds for the definition of the 
        final neutron wavevector  by A6 and the position of A5.

Special commands
----------------

MAD recognises the following special commands:

*set title ...*

Sets the title string (up to 72 characters) to be written to the data
file header.

*set user ...*

Sets the experiment user's name.

*set local ...*

Sets the local contact's name.

Standard hints
--------------


        Check of a graphite filter :
            Before aligning a PG-filter it is essential to set up the 
        spectrometer in a configuration where most of the detected neutrons 
        are 2*ki neutrons. This provides for the best count-rate sensitivity 
        to filter misalignments. In practice one may proceed as follows:

        1/      DR KI=2.662
                SE DA=6.71
                DR KF=2.662

    This will result in 2*ki neutrons being reflected by PG(002) at the analyser 
    position, while first-order neutrons are not reflected since PG(001) is extinct.

         2/         DR QH=0.5,0.5,1.5,0

    This will result in 2*Ki neutrons being reflected by a strong Bragg reflection 
    at the sample position. Here it is assumed that the sample is a single crystal 
    with a strong (113) Bragg peak, and no coherent elastic scattering at 
    (0.5 0.5 1.5).
            
        One may then proceed with the alignment itself. In general it is  
        sufficient to verify the orientation of the filter in the horizontal 
        plane. Once this is done:

           3/       SE DA=3.355
                DR KF=2.662

    N.B. If one attempts to align a PG-filter using a beam which contains a  
    significant proportion of Ki neutrons, experience shows that one finds a 
    transmission minimum for a filter orientation which is misset by a few degrees 
    from the nominal orientation (c-axis parallel to neutron beam). This position,
    however, corresponds to a transmission minimum for Ki neutrons.

Measurements Modes
------------------


        l :  Two-axis mode : If you want to work in TWO-AXIS mode, just SEt SA
        to  0 ! This will change the zero of A5 by 90� and any following drive 
        of Ki or Kf will drive  the detector to zero and the  analyser 
        perpendicular to the beam. Due to the change of A5 zero the 
        value of A5 will be ZERO (0!) with a analyser orthogonal to the 
        scatterred beam.

        l :  Constant QM Mode: If you have a powder sample and want to work at 
            a given QM ( modulus of Q that you cannot drive), just SEt the 
        sample lattice parameters (AS, BS, CS ) to 2 PI (6.2832) and lattice angles  
        (AA, BB, CC ) to  90. Any subsequent drive of QH will drive the 
        machine to the correct QM value. Use the powder switch to inhibit the 
        A3 (q) movement.

        E.G.        SE AS=6.2832 6.2832 6.2832
                SE AA = 90 90 90
                SE AX=1 0 0 0 1 0
                FI A3
                DR QH=.1 0 0 0 
                PR QM
                QM=.1

        l :  Constant DEN Mode : . An other tricky mode of operation is the 
        constant DEN mode, not constant Ki or Kf mode but a mixture with 
        either aconstant DEN mode or ( more tricky) a mode which keeps the 
        difference between energy transfer and the final energy constant  
        This gives a varying wavelength scan for scanning through a bragg peak
        or checking a filter transmission or a (fast) varying energy resolution 
        useful on IN1 to have a good energy resolution at small EN [pfor 
        phonons] and broad energy resolution at higher energy transfer 
        [magnons]).
        

        For example :

        E.G.        se FX=1
                sc QH= 1 0 0  100  DQH=0 0 0 10 DEI=10

        l :  AUto command: If you have strange or obscure warnings or messages
        on the teletype, try first the AU(to) command. It will reinitialize 
        all motor modules. This may cure the problem.


Variables
---------


        Variables are divided into five groups:  

        (i) parameters which define some aspect of the instrument configuration
        but are not directly related to a motor angle or power supply value. 
        These variables are changed by the SET command.

        (ii) parameters which relate to the sample.These are also changed by 
        SET.

        (iii) limits and zeroes for motors and power supplies, also changed by 
        SET.

        (iv) Variables which are explicitly or implicitly related to a motor 
        position or power supply value. These variables are changed by the 
        DRive command.

        (v) Increments (steps) for the variables of type (iv); these are changed
         by SET.

        The following list gives the variable identifiers and definitions, 
        where the order is as the variables are stored in the program.


         P.A Variables : Variables marked with an asterisk are not recognized 
        unless THE  Program is run in polarization analysis mode(see SWitch). 

### Instrument variables


    DM  Monochromator d-spacing         [�].
    DA  Analyzer d-spacing          [�].
    SM  Scattering sense at Mono        (+ve to the left)
    SS  Scattering sense at Sample      (+ve to the left)
    SA  Scattering sense at Analyzer        (+ve to the left)
    ALF1    Horizontal collimation before mono  [minutes FwHm]
    ALF2    Horizontal collimation mono to sample   [minutes FwHm]
    ALF3    Horizontal collimation sample to anal.  [minutes FwHm]
    ALF4    Horizontal collimation before detector  [minutes FwHm]
    BET1    Vertical collimation before mono    [minutes FwHm]
    BET2    Vertical collimation mono to sample     [minutes FwHm]
    BET3    Vertical collimation sample to analyzer [minutes FwHm]
    BET4    Vertical collimation before detector    [minutes FwHm]
    ETAM    Monochromator mosaic            [minutes FwHm]
    ETAA    Analyzer mosaic             [minutes FwHm]
    FX  =1 for constant Ki; =2 for constant Kf
    NP  Number of points in a scan
    TI  Preset time [seconds] for a COunt or SCan
    MN  Preset monitor for a COunt or SCan
    DTL lower temperature error allowed     [Kelvin]
    DTU upper temperature error allowed     [Kelvin]

    *IF1V   IF1V and IF2V are currents [Amps] in the vertical-field
    *IF2V   coils for Flipper 1 and Flipper 2.
    *IF1H   Horizontal-field currents are KI*IF1H for Flipper1 and
    *IF2H   KF*IF2H for F2.
    *HELM  Angle between axis of Helmholtz pair one and KI.

    remark:  ALF1 to ETAA are not used by MAD Program but stored for your own 
        convenience.
        Please DO NOT FORGET to update ALF1-ALF4 variable after collimator 
        change to avoid confusion when you analyse your data after one or 
        two years!

### Sample variables


    AS  -\
    BS    +--  Sample unit-cell edges   [�]
    CS  -/

    AA  -\
    BB    +--  Sample unit-cell angles  [degrees]
    CC  -/

    ETAS    Sample mosaic           [minutes FwHm]

    AX  -\
    AY    +--  Components of a recip. lattice vector in scattering plane
    AZ  -/       of the sample. A3 is the angle between KI and (AX,AY,AZ).

    BX  -\
    BY    +--  Components of a second distinct recip. lattice vector in
    BZ  -/       the sample's scattering plane.

### Limits and Zeros


        Lower and upper limits and zeros for all variables given in (iv) 
        below. L, U and Z are appended as a prefix to the variable names to 
        indicate Lower limit, Upper limit and Zero.
        Storage order is the same as for the corresponding variables, i.e. : 
        LA1, UA1, ZA1, LA2, UA2, ZA2, LA3 ... 
        (see (iv) below).

### Targets and Positions


    A1  Monochromator angle         (Bragg angle in degrees)
    A2  Scattering angle at mono.   (twice Bragg angle in degrees)
    A3  Sample angle (degs)         (A3=0 when (AX,AY,AZ) is along KI)
    A4  Scattering angle at sample  [degrees]
    A5  Analyzer angle          (Bragg angle in deg, TOPSI: not used)
    A6  Scattering angle at analyzer    (twice A5 in deg.,   TOPSI: not used)

        SINQ Instruments:
    MCV Mono curvature vertical
    SRS Sample table second ring
    ACH Anal curvature horizontal
    MTL Mono   lower translation
    MTU Mono   upper translation
    STL Sample lower translation
    STU Sample upper translation
    ATL Anal   lower translation
    ATU Anal   upper translation
    MGL Mono   lower goniometer     (Reserved)
    SGL Sample lower goniometer
    SGU Sample upper goniometer
    AGL Anal   lower goniometer     (Reserved)
    SRO     Sample sample table ring rotation. 

    D1T D1B D1R D1L Diaphragm 1 (top/bottom/right/left)
    D2T D2B D2R D2L Diaphragm 2 (top/bottom/right/left)
    D3T D3B D3R D3L Diaphragm 3 (top/bottom/right/left)

        ILL Instruments:
        CH  Monochromator changer position  [degrees or mm]
        GM  Monochromator goniometer angle  [1 unit = 4�]
        RM  Monochromator curvature
        GL  Sample goniometer angle; lower arc  [1 unit = 4�]
        GU  Sample goniometer angle; upper arc  [1 unit = 4�]
        TA  Analyzer translation        [ ? mm]
        GA  Analyzer goniometer angle   [ .4degrees]
        RA  Analyzer curvature

    EI  Incident neutron energy     [THz or meV]
    KI  Incident neutron wavevector     [ �-1]
    EF  Final neutron energy        [THz or meV]
    KF  Final neutron wavevector    [ �-1]

    QH  -\
    QK    +--  Components of Q in Reciprocal Lattice Units [R.L.U.]
    QL  -/

    EN  Energy transfer; +ve neutron energy loss    [THz or meV]
    QM  Length of Q                 [ �-1]
    TT (T)  Temperature of sample thermometer   [K]
    TRT(RT) Temperature of regulation thermometer   [K]
           (can only be printed out)

### Polarisation Analysis Variables

    *I1   -\
    *I2     \
    *I3      +--  power supply current values [A]
     .      /
    *I6  -/

    *HX   -\     Components of Helmholtz fields at sample in Oersteds.
    *HY     +--  HX is parallel to Q and HY is perpendicular to Q in
    *HZ   -/     the scattering plane.

    *F1   -\     Status of flippers one and two; these variables take the 
    *F2   -/     values ON or OFF.

### Increments Variables

        For all variables A1 through T in the  list of type (iv) variables 
        above, the identifier for the step used with a SCan command is obtained
        by prefixing the variable name with the letter D.
        Storage order is DA1, DA2, DA3....etc as for type (iv) variables above.


