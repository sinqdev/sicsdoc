### Connecting to SICS

SICS is a client server data acquisition system. This means that in order to run the DAQ, at least 
two programs need to be running:

* A SICS server
* A SICS client which talks to the SICS server and makes it do data acquisition tasks.

For the purpose of this user introduction, we assume that the SICS server is already 
running. This was achieved by advanced system administration magic.

In terms of client, three main ones are available:

* A very basic command line client which can be invoked by typing **six** at a unix prompt. 
  Normally six will automatically connect to the local instrument. If this does not work, type **six instrumentname** 
  to force six to connect to the choosen instrument.
* A standalone graphical command line client named **sics**
* A graphical user interface called **gtse**

The latter two need to be started using the given unix commands. In order to use them 
they have to be connected to an instrument. Thus both have a connect menu which allows to 
choose the instrument. 

SICS has a three level user role system:

* **spy** allows to look at everything but change nothing
* **user** allows to do most things
* **manager** allows to do everything

The default is to be logged in as spy. In order to change that both graphical clients have 
a menu selection named authorize. Use that one to increase your privilege using the user name 
and password provided to you by the instrument scientist. 

Besides the control clients there are a number of instrument specific graphical status clients:

* **scanstatus** for all instruments doing scans
* **sansstatus** for all instruments with area detectors, namely SANS, SANS2 and RITA-2
* **powderstatus** for the powder diffractometers DMC and HRPT
* **focusstatus** for FOCUS

All these need to be connected to the respective instrument after startup through the 
connect menu. 

If connecting does not work, then the SICS server is not running. In this case aks the 
instrument scientist for assistance. 

 

