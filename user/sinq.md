### The SINQ Module

On some instruments a module is installed which listens to the broadcast
messages of the accelerator division. This gives information about the
rinag and sinq status. The following commands are recognized:

    sinq

Prints the last full text ring status message. Please note that this may
be empty for some minutes right after a restart of SICS.

    sinq beam

Returns the current beam on SINQ

    sinq ring

Returns the acclerator ring current.
