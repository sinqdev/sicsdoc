TRICS PSD Peak Search
=====================

For almost any measurement at TRICS a UB matrix has to be determined
beforehand. In order to do this a couple of peak must be located by some
means. This section describes how the computer can help in finding an
initial set of peaks.

The algorithm is quite simple: It consists of a big loop over ranges of
the four circle angles two theta, omega, chi and phi. At each position a
counting operation is performed. Then peaks are located on all three
detectors through a local maximum search. For this, the [local maximum
search](lowmax.htm) module is used. If a candidate peak is found, it is
refined in omega and written to a file. The tricky bit is the
adjustement of the local maximum search parameters in order to minimize
false maxima caused by a spicky background or powder lines.

The peak search facility need a lot of parameters in order to operate.
This includes angle ranges, count parameters and the maximum search
parameters. Commands are provided for adjusting these parameters. The
general operation of these commands follow a pattern: typing the command
alone prints the current values of the parameters. In order to set new
values the command name must be typed plus new values for all the
parameters listed by this command. An Example:

    ps.sttrange

prints the range in two theta for the peaksearch.

    ps.sttrange startval endval step

sets new values for the two theta range and prints them afterwards. The
following commands are provided:

ps.sttrange

adjustment of the two theta range for the peak search.

ps.omrange

adjustment of the omega range for the peak search.

ps.chirange

adjustment of the chi range for the peak search.

ps.phirange

adjustment of the phi range for the peak search.

ps.countpar

adjustment of the counting parameters for the peak search.

ps.scanpar

adjustment of the parameters used by ps.scanlist for scanning located
peaks. See below.

ps.maxpar

Adjusts the maximum finding parameters for the peak search. These
parameters need some explanation:

window

window is the size of the quadratic area which will be searched around
each point in order to determine if it is a local maximum.

threshold

This is a minimum intensity a candidate local maximum must have before
it is accepted as a peak. The value given is multiplied with the average
counst on the data frame before use. This threshold is the strongest
selection parameter.

steepness

A candidate peak should drop of towards the sides. This is tested for by
checking if the pixels on the borders of the local maximum detection
window are below maximum value - steepness.

cogwindow

In order to refine the peaks position a center of gravity calculation is
perfomed. For this calculation pixels within the cogwindow around the
candidate peak position are considered.

cogcontour

In order not to base the COG calculation on background pixels, only
pixels above cogcontour \* maxvalue are used for the calculation. With
the spicky background at TRICS .5 seems a good value.

ps.list

lists all parameters for the peak search.

ps.listpeaks

lists all the peaks already found.

ps.run filename

starts the peak search and stores peaks identified in file filename.

ps.continue

continues a peak search which was interrupted for one reason or another.

ps.scanlist

performs an omega scan for each reflection found in the current peak
list.
