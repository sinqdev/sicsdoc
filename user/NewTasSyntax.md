


## New Common Triple Axis Syntax


This software is active at CAMEA/RITA2 and EIGER at SINQ as of 09/2020

* * *


### Drive Command

The command name is **DR**. The command **drive** as used at other instruments is available but behaves differently. 


Motors are driven as on other SICS instruments. With a single drive command several motors may be driven. 

_Driving in reciprocal lattice units_

Driving Q: 

`> DR QH` _qh qk ql en_

If _en_ (the energy transfer value) is omitted, the energy transfer is assumed as zero (in contrast to TASMAD, where it is the last used value). It is an error to give less than 3 numbers. 

WARNING: The new system allows to drive Q out of plane within the limits of the sample goniometers sgu and sgl. If there is some sample device on the table which may not be tilted, restrict the movements of sgu and sgl through software limits. 


For powders (no more powder mode switch, qm is in -1): 

`> DR QM` _qm_ `EN` _en_

Driving the energy transfer alone is also possible, (using the last given q values): 

`> DR EN` _en_

Analyzer and monochromator energies are represented by the virtual motors **EI**, **KI**, **EF** and **KF**, as in TASMAD. 

An error is signaled: 

* when real motors are mixed with virtual motors like **QH**, **EN** or **KF**
* when incompatible virtual motors are driven (like **QH** and **QM**) 
* * *


### Scans

The scan syntax is like on TASMAD. 

Arguments of **SC**: 

1. any driveable variable, and its center value 
2. any step (**D** immediately followed by the variable name) 
3. **NP** followed by the number of points (note: if **NP** is even, the scan center is between the two points in the middle). 
4. **MN** or **TI** followed by the monitor or time preset value 

The presence of 4 argument parts is mandatory, but the order of the parts is not relevant. For every step variable, the corresponding center value must be given. If no step is given for a center variable, a step 0 is used. 

Special cases are the variables **QH** and **Q3** , and their corresonding steps **DQH** and **DQ3** which have 3 or 4 values. 

Examples: 

* energy scan (the following two beeing equivalent)) `   
> SC QH 1 0 0 0 DQH 0 0 0 0.01 NP 5 MN 1000   
> SC QH 1 0 0 EN 0 DEN 0.01 NP 5 MN 1000 `
* q scan (the following two beeing equivalent)) `   
> SC QH 1 0 0 0 DQH 0 0.01 0 0 NP 5 MN 1000   
> SC QH 1 0 0 0 DQH 0 0.01 0 NP 5 MN 1000 `
* motor scan with time preset `   
> SC A3 25 A4 50 DA3 0.1 DA4 0.2 NP 11 ti 1 `

## Warning

The new UB matrix system allows to drive Q out of plane within the limits of the sample goniometers sgu and sgl. If there is a sample environment device on the sample table which may not be tilted, please restrict the movement of motors sgu and sgl through software limits. 

### Polarised scans

`> POLSCAN` _script_ _"scan variables and steps"_ `NP` _np_ `MN` _mn_

The script contains all what has to be done at one scan point. Its exact syntax is still open. 

* * *


### Commands from TASMAD
    
    
    CO
    

count (arguments: either `MN mon` or `TI time`) 
    
    
    FI (FIX)
    

without argument: show which motors are fixed  
with arguments: motors to fix. 
    
    
    CL
    

without argument: show which motors are fixed  
`CL ALL`: clear all motors  
with arguments: motors to unfix. 
    
    
    DO
    

alias to **exe** in SICS. This command runs a batch file. 
    
    
    LE
    

summarize q and energy infromation 
    
    
    LZ, LL
    

summarize zero points and limits informaiton 
    
    
    LM
    

summarize machine parameters 
    
    
    LS
    

summarize sample information (the format may change due to the ub matrix system) 
    
    
    LT
    

show targets and positions 
    
    
    LI
    

the same as LM, LS, LZ, LT, LE executed one after another 
    
    
    OUT
    

set additional output variables to be printed into the data file and on command history 
    
    
    PR (PRINT)
    

print variable values.  
_remark: the values of sics objects may be printed also by directly entering the name, but this does not work for _`L`_mm, _`U`_mm, _`Z`_mm, _`SS, SA, SM and FX`_ (where mm is any motor)_
    
    
    SE (SET)
    

set variable values.  
_remark: the values of sics variables may be set also by directly entering the name and the value, but this does not work for _`L`_mm, _`U`_mm, _`Z`_mm, _`SS, SA, SM and FX`_ (where mm is any motor)_

### Other Commands

Naturally, all general SICS commands are valid also. 

* * *


### UB Matrix for Triple Axis Instruments 

`> CELL _a b c alpha beta gamma_`

set unit cell constants. If b or c are omitted, the same value as for a is used, if angles are omitted, they are set to 90 degress. Without arguments, the cell constants are listed. 

`> addauxref _qh qk ql_`

Define the vectors for the scattering plane. 

`> REF _qh qk ql a3 a4 sgl sgu Ei Ef_`

Make an entry in the list of peaks. For angles and k-values not given, the current motor positions are used. The peak number and the peak data are returned. 

`> REF AUX _qh qk ql_` Add an auxiliary reflection to the list of reflections. A4 is calculated from the cell constants, the current ei and ef values are used. A3 is left alone or is calculated to have the right angular difference to the previous reflection. Using auxiliary reflections is a help during setup of the instrument or in powder mode. 

The sequence: 
    
    
    ref clear all
    cell aa bb cc alpha beta gamma
    addauxref ax ay az
    addauxref bx by bz
    makeub 1 2
    

with ax, ay, az being the first plane vector and bx, by, bz being the second plane vector creates a UB matrix which allows to drive Q positions and qm. But be aware that a3, sgu and sgl values are still invalid. 

`> REF CLEAR _peak_`

Remove a peak. 

`> REF CLEAR ALL`

Remove all peaks. 

`> REF`

List all peaks. 

`> MAKEUB _peak1 peak2_`

Calculate, activate and print out the UB matrix, as well as the peak list with qhkl values calculated back. _peak1_ is used as the main peak, i.e. driving to the angles given for this peak will correspond to a QHKL which may only differ by a scalar factor of about one, if the cell constants are not correct. _peak2_ is used only for determining the scattering plane. 

_It has to be checked that this is fully compatible with the TASMAD specification. In principle, AX,AY,AZ should correspond to peak1 and BX,BY,BZ to peak2._

`>MAKEUBFROMCELL`

Calculate and activate a UB which has been calculated from the cell constants alone. This is useful to get a4 when no reflection has yet been found to calculate a proper UB. 

`>MAKEAUXUB qh qk ql`

Calculate a UB matrix from the first reflection and an auxiliary second reflection specified through the miller indices qh , qk, ql. The auxiliary reflection will have the same angles as the first reflection, except a3 and a4 will be adjusted to match the requirements from the crystalls geometry. 


`> LISTUB`

List the UB matrix, the cell constants and all stored reflections. 

#### Out Of Plane Operation

This formalism allows to drive out of the scattering plane using the tilt motors of the sample stage. Some cryostats cannot stand this. Therefore driving out of plane can be switched off and on. 

`> TASUB OUTOFPLANE 1`

This command switches out of plane operation **on**. 

`> TASUB OUTOFPLANE 0`

This command switches out of plane operation **off**. 

`> TASUB OUTOFPLANE`

lists the current value of the outofplane variable 

**Example session:**   
(Some numbers might not be correct, as this module does not yet work in this form). 
    
    
    We enter cell values
    
       > CELL 2.88 2.88 2.88 90 90 90
    
       > MAKEUBFROMCELL
    
    MAKEUBFROMCELL builds a UB matrix from the cell only. With this you can drive to 2 0 0
    
       > drive qh 2 0 0
    
    This will get you the correct two-theta == a4. You have to search the peak in a3, sgu and sgl and optimize it.
    
    Once we are at the maximum:
    
       > REF 2 0 0
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       1    2.00   0.00   0.00   65.31  -87.21    1.25   -5.22
    
       > makeauxub 0 4 4
       > dr qh 0 4 4
    Make an axiliary UB and drive to the theoretical position for 0 4 4
    
    Find the peak 0 4 4 and optimize it.
    
       ...
    
    Once we are at its maximum:
    
       > REF 0 4 4
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       1    0.00   4.00   4.00   65.31  -87.21    1.25   -5.22
    
    Show again the peak list:
    
       > REF
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       1    2.00   0.00   0.00   65.31  -87.21    1.25   -5.22
       2    0.00   4.00   4.00   99.45   20.39    4.25   -1.22
    
    Calculate the UB matrix, with 2 0 0 as primary peak and 0 4 4 as
    secondary peak.
    
       > MAKEUB 1 2
       UB   2.456   0.530  -0.200
           -0.530   2.456  -0.090
            0.200   0.090   2.456
       Peak    QH     QK     QL      A4      A3     SGL     SGU     QH     QK     QL
       1 A   2.00   0.00   0.00   65.31  -87.21    1.25   -5.22   2.03   0.00   0.00
       2 B   0.00   4.00   4.00   99.45   20.39    4.25   -1.22   0.02   3.95   4.03
    
    the last 3 columns above show the Q values calculated back from the angles.
    We drive now to the 2 0 0 peak
    
       > DR QH 2 0 0
       > PR A4 A3 SGL SGU
       A4 =  64.218
       A3 = -87.515
       SGL =  2.258
       SGU = -3.217
    
    The value of A4 has changed, because the cell constants do not match the values
    given for the first peak. The value of A3 has changed by the same reason, and
    in addition, because the the plane given by 2 0 0 and 0 4 4 is now tilted back
    into in the scattering plane. For the latter reason SGU and SGL have changed also.
    
       > DR QH 3 3 3
       > PR A4 A3 SGL SGU
       A4 =  82.516
       A3 = -31.215
       SGL =  2.258
       SGU = -3.217
    
    note that the values for SGU and SGU have not changed. The will not change
    as long as we stay in the scattering plane (no drives to Q3) and as long
    as the UB matrix has not changhed.
    
       > REF 3 3 3
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       3    3.00   3.00   3.00   82.32  -31.25    2.26   -3.22
       > MAKEUB C B
       UB   2.456   0.530  -0.200
           -0.530   2.456  -0.090
            0.200   0.090   2.456
       Peak    QH     QK     QL      A4      A3     SGL     SGU     QH     QK     QL
       1     2.00   0.00   0.00   65.31  -87.21    1.25   -5.22   2.03   0.00   0.00
       2 B   0.00   4.00   4.00   99.45   20.39    4.25   -1.22   0.02   3.95   4.03
       3 A   3.00   3.00   3.00   82.32  -31.25    2.26   -3.22   3.00   3.00   3.00
    
    We have calculated a new UB matrix, based on the peak 3 3 3 as primary and the
    0 4 4 as secondary peak. The UB matrix has not changed, as we were already exactly
    at 3 3 3.
    

* * *


### Configuration

DM,DA
:  Monochromator/Analyser d-spacing [Angstroem].

SM,SS,SA
:  Scattering sense at Mono (new: as text LEFT/RIGHT/STRAIGHT)

FX
:  new as text: KI or KF

_informational only:_

ALF1,ALF2,ALF3,ALF4
:  Horizontal collimation before mono, before sample, before analyser, before detector [minutes FwHm]

BET1,BET2,BET3,BET4
:  Vertical collimation before mono, before sample, before analyser, before detector [minutes FwHm]

ETAM,EATS,ETAA
:  Monochromator/Sample/Analyser mosaic [minutes FwHm]

### Limits and Zeros

Lower and upper limits and zeros for all motors. L, U and Z are appended as a prefix to the variable names to indicate Lower limit, Upper limit and Zero. 

### Motors
    
    
    A1      Monochromator angle             (Bragg angle in degrees)
    A2      Scattering angle at mono.       (twice Bragg angle in degrees)
    A3      Sample angle (degs)             (A3=0 when (AX,AY,AZ) is along KI)
    A4      Scattering angle at sample      [degrees]
    A5      Analyzer angle                  (Bragg angle in deg.)
    A6      Scattering angle at analyzer    (twice A5 in deg.)
    
    MCV     Mono curvature vertical
    SRS     Sample table second ring
    ACH     Anal curvature horizontal
    MTL     Mono   lower translation
    MTU     Mono   upper translation
    STL     Sample lower translation
    STU     Sample upper translation
    ATL     Anal   lower translation
    ATU     Anal   upper translation
    MGL     Mono   lower goniometer         (Reserved)
    MGU     Mono   upper goniometer
    SGL     Sample lower goniometer
    SGU     Sample upper goniometer
    AGL     Anal   lower goniometer         (Reserved)
    AGU     Anal   upper goniometer
    
    D1T D1B D1R D1L Diaphragm 1 (top/bottom/right/left)
    D2T D2B D2R D2L Diaphragm 2 (top/bottom/right/left)
    D3T D3B D3R D3L Diaphragm 3 (top/bottom/right/left)
    

### Virtual motors
    
    
    EI      Incident neutron energy         [meV]
    KI      Incident neutron wavevector     [Angstroem-1]
    EF      Final neutron energy            [meV]
    KF      Final neutron wavevector        [Angstroem-1]
    
    QH      Q in Reciprocal Lattice Units (3 components!)
    
    EN      Energy transfer; +ve neutron energy loss        [meV]
    QM      Length of Q                             [Angstroem-1]
    

### Polarisation analysis

not yet specified 

* * *


