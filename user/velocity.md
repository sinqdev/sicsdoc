### The Velocity Selector


The velocity selector is a turbine rotating at high speed. This turbine
annihilates all neutrons which do not manage to travel through the
turbine in a time intervall defined by the rotation speed of the
turbine. Thus neutrons in a certain speed range (energy range) are
selected. The distribution of neutrons is also dependent of a tilt angle
between the rotation axis of the turbine and the neutron beam. A high
speed device like a velocity selector has to account for gyroscopic
forces when moving the device. In praxis this means that the selector
must be stopped before the tilt angle can be changed. Furthermore there
are forbidden areas of rotation speeds. In these areas the velocity
selector is in destructive resonance with itself or its bedding. The
command set for a velocity selector is defined below. The name of the
velocity selctor object is assumed to be nvs.

    nvs rottolerance par

Sets or requests the tolerance the rotation speed may have before an
error is issued.

    nvs list

Displays rotation speed and tilt angle of the velocity selctor.

    nvs add min max

This command adds a forbiden region between min and max to the list of
forbidden regiosn of the velocity selector. These values are usually
configured in the configuration file.

    nvs rot = newval tilt = newval

This command sets a new tilt angle and/or rotation speed for the
velocity selector. Either one or both of the keywords tilt or rot may be
given, followed by a number.

    nvs interrupt val

Requests or sets an interrupt value. This interrupt will be issued when
an error on this velocity selector ocurred. With a parameter a new value
is set, without the current value is printed.

    nvs userrights val

Requests or sets a userright value. This userright value defines who may
drive this velocity selector. With a parameter a new value is set,
without the current value is printed.

    nvs tilttolerance val

Requests or sets a tilttolerance value. This parameter defines the
permissable tolerance for the tilt motor. With a parameter a new value
is set, without the current value is printed.

    nvs rotinterest

Enables printing of status messages about the current state of the
selector when it is driven.

    nvs loss

Starts a loss current measurement on the velocity selector and prints
the result.

    nvs forbidden

Prints a list of forbidden speed regions for this selector.

    nvs status

Prints a status summary of the velocity selector.

The commands described so far cover the actual handling of the velocity
selector. During a measurement users might want to use further functions
such as:

-   Monitor the rotation speed of the velocity selector.
-   Log the rotation speeds of the velocity selector.
-   Initiate error handling when the velocity selector fails to stay
    within a predefined tolerance of rotation speeds.

Now, these are tasks usually connected with sample environment devices.
Now, the SICS programmers have been lazy. Moreover they wanted to avoid
duplicating code (and bugs). Consequently, they tricked the velocity
selector to be a sample environment device as well. This means besides
the actual velocity selector object (in this case called nvs) there
exists another object for monitoring the velocity selector. The name of
this device is the name of the velocity selector object with the string
watch appended. For example if the velocity selector has the SICS name
nvs, the monitor object will be nvswatch. The commands understood by the
watch object are fully decribed in the section about [sample environment
devices](samenv.htm#all). Please note, that all driving commands for the
watch object have been disabled. Driving can only be achieved through
the velocity selector object.
