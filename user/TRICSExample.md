### Four Circle Example Run

This text shows the use of the new command set for TRICS at the example
of the hypothetical substance Hugosulfid, HugoS~2~. This does not show
the total command set, but rather gives an example usage. For more
details on the commands used, consult the reference manual.

#### Setting Up

Do this carefully! The parameters entered here are either crucial for
SICS to run properly or for you to discover in 5 years time what your
data is all about!

> projectdir /home/tricslnsg/uni\_user/hugosulfid 
>  cell 2.3 2.3 5 90 90 90 
>  spgrp P4 
>  lambda 1.178 
>  mode bi 
>  detmode single 
>  title Hugosulfid at room temperature 
>  sample hugosulfid 
>  user Gwendolin User 
>  address "Hugo University, 24 Hugo close, 2433 Hugohatten, Hugoland" 
>  e-mail Gwendolin@hugo.edu 
>  phone +44-55-334456 

* * * * *

#### Finding your first Reflection

Carefully mount your sample and align it visually.

Clear the reflection list:

> refclear

Calculate two theta for a strong reflection you wish to find, assuming 1
0 0:

> calcstt 1 00

Drive the instrument there, assuming the value is 24.8:

> dr stt 24.8 om 12.4

Scan chi, phi, om to find the reflection. Look at the count rate monitor
besides the computer monitor, it will tell you when you located a strong
reflection.

> sscan phi -180 180 180 10000 
> sscan chi -90 90 90 10000 

Once you locate something which looks like a peak, let SICS center it:

> center 10000

When this is done, enter the reflection into the reflection list, check
the possible index:

> indexhkl

Enter the reflection into the reflection list:

> refadd idxang 1 0 0

#### Find further Reflections

The fastest is to do a cone scan for further reflections.

> coneconf 00000 0 0 1

This means use the reflection with the id 00000 in the reflection list
as center and 0,0,1 as the target. If you are unsure about reflection
IDs: type reflist to see all stored reflections.

Do a conescan:

> sscan cone 0 360 10000

Somewhere on a cone angle there may have been intensity, drive there:

> dr cone 133.

Center the reflection:

> center 10000

Add it to the reflection list:

> refadd idxang 0 0 1

For proper indexing it is advised to find at least another reflection in
the same way.

This is the way how to locate reflections by cone scans. Of course, you
may use the same procedure as described in section 2, to locate
reflections.

* * * * *

#### Initial UB Matrix and Indexing


If you are sure about the indexing, calculate the UB:

> ubcalc 0000 00001 00002

The parameters are reflection indices as can be seen when printing the
reflection list with:

> ublist

If you are less sure about the indexing, let SICS suggest an indexing to
you:

> index

Will try and index your reflection set. It will print a list of possible
solutions. Decide for one and have a UB calculated with:

> indexub 0

The parameter is the number of the solution printed by index.

* * * * *

#### UB Matrix Refinement


Add more reflections to the reflection list along the following
criteria:

-   Strong reflections
-   Symmetrically equivalents
-   All directions of space
-   At high, medium and low two theta

> refadd idx 4 0 0 
>  refadd idx -4 0 0 
>  refadd idx 0 4 0 
>  refadd idx 0 -4 0 
>  refadd idx 0 0 4 
>  refaddidx 0 0 -4 
>  . 
>  . 
>  . 

Have all of them centered by SICS, this time with better statistics:

> centerlist 30000

Have a coffee while SICS is doing this for you. It is advisable to have
two centering runs: a rough one, with all slits open and collimators
open and a precission one, with collimators in and slits reasonably
closed.

Refine the UB:

> ubrefine

If you like the result, load the refined UB and cell constants into
SICS:

> ubload

* * * * *

#### Data Collection

Configure the limits for generating reflections to measure:

> hkllimit 0 20 0 20 0 20

This means in h,k,l from 0 to 20

Generate reflections to measure:

> hklgen sup

This suppresses symmetrically equivalent reflections.

Remove reflections which are not measurable from the list:

> testx del

Sort the reflection list by two theta:

> hklsort

Check the list of reflections to measure with:

> hkllist

Reflections are measured in different ways, depending on two theta. This
table should have been set up for you by your lovely instrument
scientist. Check the table of these parameters:

> tablist

When necessary, delete entries in this table with:

> tabdel 2

And add new ones with:

> tabadd 40 om .2 30 30000

This says, reflections up to two theta 40 are measured by omega scans
with a stepwidth of .2. 30 steps are made and the preset count is 30000.

Actually collect data with:

> collect

Go and have a nap.

#### Finish

In due time come and collect your data, analyse and publish them and win
the Nobel prize. Do not forget to mention the author of this fine
software and the manual in your laudatio.
