### Second Generation SICS Objects

All newly developed functionality and many devices in SICS are now second 
generation SICS objects. Second generation SICS objects are easier to program. 
But they are also a little more user friendly then the old style SICS 
objects. Because all second generation SICS object obey the list command:

    object list

Where object is a placeholder for the object name. This command will print a list of 
all the parameters and functions associated with the object. They are sort of self 
documenting. 

Function nodes may look like this:

    som sethardlim       = FUNCTION
    som sethardlim/low   = 0
    som sethardlim/high  = 0

There are nodes underneath the function node. These nodes are the parameters to the 
function. The example thus shows a function som sethardlim which takes two parameters: 
low and high. Which in this context are the lower and upper hardware limits. 

#### Second Generation Device Objects

Second generation device objects in SICS have the same parameters with the same meaning 
as first generation SICS objects. The difference is in the internal implementation: the 
way how second generation objects communicate with the hardware is different.  
Thus the documentation for first generation device objects 
is still largely true. There are two new common fields however:

**status** 

This field can be idle, run, error etc and shows what the device is currently up too.

**error**

This field gives a description of the last error encountered when operating the device. 

Besides the standard fields, second generation device objects can have additional fields. These fields 
are usually device dependent driver parameters. A casual user should normally leave these fields 
alone. The meaning of these fields should be documented in the driver documentation for the device. 


##### Second Generation Counters

One thing which changed is the way counting thresholds are set in second generation counter objects. 
The first generation counters used the counter setthreshold function. This function has been replaced by 
two parameters:

**thresholdcounter**

This parameter sets the monitor number to which the threshold should be applied. This is usually 1.

**threshold**

This parameter sets the actual threshold. 

##### Second Generation Motors

Second generation SICS motors have another interesting field:

**targetposition**

This is the position where the motor should be as requested 
by the last run or drive command. 


