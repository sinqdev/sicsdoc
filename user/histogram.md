### Histogram memory

* * * * *

Histogram memories are used in order to control large area sensitive
detectors or single detectors with time binning information. Basically
each detector maps to a defined memory location. The histogram memory
wizard takes care of putting counts detected in the detector into the
proper bin in memory. Some instruments resolve energy (neutron flight
time) as well, than there is for each detector a row of memory locations
mapping to the time bins. As usual in SICS the syntax is the name of the
histogram memory followed by qualifiers and parameters. As a placeholder
for the histogram memories name in your system, HM will be used in the
following text.

A word or two has to be lost about the SICS handling of preset values
for histogram memories. Two modes of operation have to be distinguished:
counting until a timer has passed, for example: count for 20 seconds.
This mode is called timer mode. In the other mode, counting is continued
until a control monitor has reached a certain preset value. This mode is
called Monitor mode. The preset values in Monitor mode are usually very
large. Therefore the counter has an exponent data variable. Values given
as preset are effectively 10 to the power of this exponent. For instance
if the preset is 25 and the exponent is 6, then counting will be
continued until the monitor has reached 25 million. Note, that this
scheme with the exponent is only in operation in Monitor mode.

#### Configuration


A HM has a plethora of configuration options coming with it which define
memory layout, modes of operation, handling of bin overflow and the
like. Additionally there are HM model specific parameters which are
needed internally in order to communicate with the HM. In most cases the
HM will already have been configured at SICS server startup time.
However, there are occasion where these configuartion option need to
enquired or modified at run time. The command to enquire the current
value of a configuration option is: **HM configure option**, the command
to set it is: **HM configure option newvalue**. A list of common
configuration options and their meaning is given below:

**HistMode**

HistMode describes the modes of operation of the histogram memory.
Possible values are:

-   Transparent, Counter data will be written as is to memory. For
    debugging purposes only.
-   Normal, neutrons detected at a given detector will be added to the
    apropriate memory bin.
-   TOF, time of flight mode, neutrons found in a given detector will be
    put added to a memory location determined by the detector and the
    time stamp.
-   Stroboscopic mode. This mode serves to analyse changes in a sample
    due to an varying external force, such as a magnetic field,
    mechanical stress or the like. Neutrons will be stored in memory
    according to detector position and phase of the external force.

**OverFlowMode**

This parameter determines how bin overflow is handled. This happend when
more neutrons get detected for a particular memory location then are
allowed for the number type of the histogram memory bin. Possible values
are:

-   Ignore. Overflow will be ignored, the memory location will wrap
    around and start at 0 again.
-   Ceil. The memory location will be kept at the highest posssible
    value for its number type.
-   Count. As Ceil, but a list of overflowed bins will be maintained.

**Rank**

Rank defines the number of dimensions the detector has, minus the time
channel when applicable. 1 is a linear detector, 2 a area detector etc.

**BinWidth**

determines the size of a single bin in histogram memory in bytes.

**dim0, dim1, dim2, ... dimn**

define the logical dimensions of the histogram.

**extrachan**

Extra time channels as used at AMOR and SANS for time-of-flight
monitors. They get appended to the main hm data but are treated
separately.

In addition to these common options there exist additional options for
the EMBL position sensitive detectors (PSD) installed at TRICS and AMOR.
These PSDs can be operated at different pixel resolutions. The position
of a neutron event on these detectors is encoded in a delay time value
which is digitized into a range between 0 to 4096. This resolution
exceeds the resolution available from instrument physics by far. Useful
resolutions are obtained by dividing this raw range by a factor. In
addition, the coordinates of the center of the detector have to given as
well (usually size/2).This is done through the configuration options:

**xFac**

x direction division factor

**yFac**

y direction division factor

**xOff**

Offset of the detector center in x.

**yOff**

Offset of the detector center in y.

Do not forget to change the standard options dim0, dim1 and length as
well when changing the PSD resolution.

For time of flight mode the time binnings can be retrieved and modified
with the following commands. Note that these commands do not follow the
configure syntax given above. Please note, that the usage of the
commands for modifying time bins is restricted to instrument managers.

**HM timebin**

Prints the currently active time binning array.

**HM genbin *start step n***

Generates a new equally spaced time binning array. Number n time bins
will be generated starting from start with a stepwidth of step (example:
HM genbin 10 1 5).

**HM setbin *inum value***

Sometimes unequally spaced time binnings are needed. These can be
configured with this command. The time bin iNum is set to the value
value.

**HM clearbin**

Deletes the currently active time binning information.

**HM notimebin**

returns the number of currently configured timebins.

#### Histogram Memory Commands

Besides the configuration commands the HM understands the following
commands:

**HM preset *[newval]***

with a new value sets the preset time or monitor for counting. Without a
value prints the current value.

**HM exponent *[newval]***

with a new value sets the exponent to use for the preset time in Monitor
mode. Without a value prints the current value.

**HM CountMode *[mode]***

with a new values for *mode* sets the count mode. Possible values are
Timer for a fixed counting time and Monitor for a fixed monitor count
which has to be reached before counting finishes. Without a value print
the currently active value.

**HM init**

after giving configuration commands this needs to be called in order to
transfer the configuration from the host computer to the actual HM.

**HM count**

starts counting using the currently active values for CountMode and
preset. This command does not block, i.e. in order to inhibit further
commands from the console, you have to give Success afterwards.

**HM countblock**

starts counting using the currently active values for CountMode and
preset. This command does block, i.e. you can give new commands only
when the counting operation finishes.

**HM initval *val***

initialises the whole histogram memory to the value val. Ususally 0 in
order to clear the HM.

**HM get *i iStart iEnd***

retrieves the histogram number i. A value of -1 for i denotes retrieval
of the whole HM. iStart and iEnd are optional and allow to retrieve a
subset of a histogram between iStart and iEnd.

**HM sum *d1min d1max d2min d2max .... dnmin dnmax***

calculates the sum of an area on the detector. For each dimension a
minimum and maximum boundary for summing must be given.
