### SANS command summary



#### Most Used Commands


    bs X = xval Y = yval

drive beam stop to xval yval

    bs out

drive beamstop to safe position.

    bs back

drive beamstop back

    dt X = xval Y = yval psi = angle

drive detector to xval,yval, angle

    count mode preset

Counts in mode with a preset value of preset. Stores data automatically.

    Repeat num mode preset

Calls count num times.

    exe filename

Runs a batch file with the specified filename.


#### General commands


    Success

wait for the last operation to finish.

    wait time

wait for time to pass....

    Dir

lists all objects in the system.


#### Velocity Selector

    nvs list

lists tilt angle and speed of the velocity selector.

    nvs rot

prints rotation speed of the velocity selector.

    nvs rot val

sets rotation speed of the velocity selector.

    nvs tilt

prints tilt angle of the velocity selector.

    nvs tilt val

sets tilt angle of the velocity selector.

    nvs tilt val rot val2

sets both tilt angle and rotation speed of the velocity selector.

    lambda

prints current wavelength.

    drive lambda val

drives velocity selector to a new wavelength.

#### BeamStop

    bs X = xval Y = yval

drive beam stop to xval yval

    bsout

drive beamstop to safe position.

    bsin

drive beamstop back

    bs pos name

assign current position to name

    bs drop name

delete named position name

    bschange par

With parameter change beam stop, without: inquire about current beam
stop.

#### detector

    dt X = xval Y = yval psi = angle

drive detector to xval,yval, angle

    dt back

drive detector back

    dt pos name

assign current position to name

    dt drop name

delete named position name


#### Variables

Each variable can be inquired by just typing its name. It
can be set by typing the name followed by the new value. Currently
available variables are:

-   Title
-   User
-   Comment1
-   Comment2
-   environment

