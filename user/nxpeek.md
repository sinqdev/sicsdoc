NXPEEK: An Tcl-API to NeXus
===========================

NXPEEK is an interface for reading NeXus files from Tcl. NXPEEK was
written by by Nick Maliszewski at NIST. It works in the usual way of a
widget command in Tcl. One special command, *nxopen* is added to Tcl
which opens a NeXus file. The command returns a handle which in itself
is again a Tcl command. The Usage looks like this:

set handle [nxopen filename]

opens the NeXus file, craetes a new handle and returns it.

\$handle ls

lists the contents of the current vGroup.

\$handle cd {name class}

changes into the vGroup name of class class. Works much like a change
directory on a file system.

\$handle cat name

Returns the contents of the SDS name as a Tcl-list or a list of
Tcl-lists.

\$handle info name

Returns information about the SDS name. This includes rand, dimensions
and data type.

\$handle attributes name

Returns the attributes of the SDS name. If no SDS name is specified a
list of global attributes is returned.

\$handle create VG {name class}

creates a new vGroup with name name and class class at the current level
in the NeXus vGroup hierarchy.

\$handle create SDS name datatype rank dims

This call creates an SDS. A name argument is required. The data type
must be one of the NexUs datatypes: DFNT\_INT8, DFNT\_UNIT8,
DFNT\_INT16, DFNT\_UINT16, DFNT\_INT32, DFNT\_UINT32, DFNT\_FLOAT32 and
DFNT\_FLOAT64. Rank is currently to 2 and below. The dimensions are
given as a Tcl-list.

\$handle attrib SDSname attribname attribvalue

Adds the attribute attribname with a value of attribvalue to the SDS
SDSname.

\$handle write SDSname data offset

Writes data to the SDS SDSname. The data must be give as a Tcl list (
one dimensional case) or as a list of Tcl-lists (the two dimensional
case). Offset is a Tcl list which describes at which position the data
shall be written. Usually this is {0} or {0 0}.
