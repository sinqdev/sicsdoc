### SICS motor handling

* * * * *

In SICS each motor is an object with a name. Motors may take commands
which basically come in the form

*motorname command*; example: D2HR list.

Most of these commands deal with the plethora of parameters which are
associated with each motor. The syntax for manipulating variables is,
again, simple.

*Motorname parametername*

will print the current value of the variable.

*Motorname parametername newval*

will set the parameter to the new value specified. A list of all
parameters and their meanings is given below. The general principle
behind this is that the actual (hardware) motor is kept as stupid as
possible and all the intracacies of motor control are dealt with in
software. Besides the parameter commands any motor understands these
basic commands:

-   ***Motorname list*** gives a listing of all motor parameters.
-   ***Motorname* reset** resets the motor parameters to default values.
    This is software zero to 0.0 and software limits are reset to
    hardware limits.
-   ***Motorname* position** prints the current position of the motor.
    All zero point and sign corrections are applied.
-   ***Motorname* hardposition** prints the current position of the
    motor. No corrections are applied. Should read the same as the
    controller box.
-   ***Motorname* interest** initiates automatic printing of any
    position change of the motor. This command is mainly interesting for
    implementors of status display clients.

Please note that the actual driving of the motor is done via the
[drive](drive.htm) command.

* * * * *

#### The motor parameters

-   **HardLowerLim** is the hardware lower limit. This is read from the
    motor controller and is identical to the limit switch welded to the
    instrument. Can usually not be changed.
-   **HardUpperLim** is the hardware upper limit. This is read from the
    motor controller and is identical to the limit switch welded to the
    instrument. Can usually not be changed.
-   **SoftLowerLim** is the software lower limit. This can be defined by
    the user in order to restrict instrument movement in special cases.
-   **SoftUpperLim** is the software upper limit. This can be defined by
    the user in order to restrict instrument movement in special cases.
-   **SoftZero** defines a software zero point for the motor. All
    further movements will be in respect to this zeropoint.
-   **Fixed** can be greater then 0 for the motor being fixed and less
    then or equal to zero for the motor being movable.
-   **InterruptMode** defines the interrupt to issue when the motor
    fails. Some motors are so critical for the operation of the
    instrument that all operations are to be stopped when there is a
    problem. Other are less critical. This criticallity is expressed in
    terms of interrupts, denoted by integers in the range 0 - 4
    translating into the interrupts: continue, AbortOperation,
    AbortScan, AbortBatch and Halt. This parameter can usually only be
    set by managers.
-   **Precision** denotes the precision to expect from the motor in
    positioning. Can usually only be set by managers.
-   **AccessCode** specifies the level of user privilege necessary to
    operate the motor. Some motors are for adjustment only and can be
    harmful to move once the adjustment has been done. Others must be
    moved for the experiment. Values are 0 - 3 for internal, manager,
    user and spy. This parameter can only be changed by managers.
-   **Sign** reverses the operating sense of the motor. For cases where
    electricians and not physicists have defined the operating sense of
    the motor. Usually a parameter not to be changed by ordinary users.
-   **failafter**This is the number of consecutive failures of
    positioning operations this motor allows before it thinks that
    something is really broken and aborts the experiment.
-   **maxretry**When a motor finishes driving, SICS checks if the
    desired position was reached. If the position read back from the
    motor is not within precision to the desired value, the motor is
    restarted. This is done at max maxretry times. After maxretry
    retries, the motor throws an error.
-   **ignorefault**If this is bigger then 0, positioning faults from the
    motor will be ignored.

This list of parameters may be enhanced buy driver specific parameters.
motor list will show all parameters.

#### Motor Error Handling Concepts

As mechanical components motors are prone to errors. SICS knows about
two different classes of motor errors:

HWFault

This is when there is a problem communicating with the motor, a limit is
violated etc. SICS assumes that such errors are so grave that no fix is
possible. If such a HWFault is detected a configurable interrupt (see
parameter InterruptMode) is set which can be used by upper level code to
act upon the problem.

HWPosFault

This is a positioning failure, i.e. The motor did not reach the desired
position. Such a positioning problem can come from two sources:

-   The positioning problem is reported by the motor driver. SICS then
    assumes that the driver has done something to solve the problem and
    promotes this problem to a HWFault.
-   The motor driver reported no error and SICS figures out by itself,
    that the desired position has not been reached. SICS thinks that
    this is the case if the difference between the desired position and
    the position read from the motor controller is greater then the
    parameter precision. If SICS detects such a problem it tries to
    reposition the motor. This is done for the number of times specified
    through the parameter maxretries. If the position has not been
    reached after maxretries repositionings, a HWFault is assumed.

In any case lots of warnings and infos are printed.

If SICS tries to drive an axis which is broken hardware damage may occur
(and HAS occurred!). Now, SICS has no means to detect if the
mispositioning of a motor is due to a concrete block in the path of the
instrument or any other reason. What SICS can do though is to count how
often a motor mispositions in sequence. This means SICS increments a
mispositioning counter if it cannot drive a motor, if the motor is
driven succesfully, the mispositioning counter is cleared. If the count
of mispositionings becomes higher then the parameter failafter, SICS
thinks that there is something really, really wrong and aborts the
measurement and prints an error message containing the string: MOTOR
ALARM.

There are some common pitfalls with this scheme:

You want upper level code to be signalled when your critical motor
fails.

Solution: set the parameter interruptmode to something useful and check
for the interrupt in upper level code.

SICS falsly reports mispositionings.

Solution: increase the precision parameter.

You know that a motor is broken, you cannot fix it, but you want to
measure anyway.

Solution: increase the precision parameter, if SICS finds the
positioning problem, increase maxretries, increase the failafter
parameter. In the worst case set the ignorefault parameter to greater 0,
this will prevent all motor alarms.
