### Counting Commands


    count *[mode preset]*

Does a count operation in mode with a given preset. *Mode* can be
*timer* or *monitor*. The parameters mode and preset are optional. If
they are not given the count will be started with the current setting in
the histogram memory object.

    repeat num mode preset.

Calls count num times. num is a required parameter. The other two are
optional and are handled as described above for count.

Both commands make sure that measured data is written to files.
