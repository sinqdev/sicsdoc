### Shutter

All instruments ahve an experiment shutter. For many instruments, the shutter can be 
controlled from the DAQ system. The command set is fairly standardized:

    shutter

Asks the status of the shutter. Which can be open, closed or enclosure broken. 
The latter means that the door to the experimental area is open and the shutter 
cannot be opened.

    shutter open

Will open the shutter. This only works when the door to the experimental area is 
properly closed.

    shutter close

Will close the shutter.