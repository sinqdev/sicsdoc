Simulation Mode
===============

For testing batch files or in order to check the movement of the
instrument it may be helpful to run SICS in simulation mode. You must
theb connect to a special simulation SICS server which may have been
setup for you. In the simulation server, everything is like in the
actual SICS server, except that no hardware is moved, co counts
collected and no data file written. There is one speciality, however.
The command:


       sync

synchronizes the parameters and limits in the simulation server with
those in the instrument server.
