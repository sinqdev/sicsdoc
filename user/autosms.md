Automatic SMS Notification
==========================

On some instruments an automatic notification system is installed which
can be configured to send an SMS when the instrument is stopped. An
instrument is considered stopped when there has been no counting or
driving activity for a configurable amount of time and there is beam at
SINQ. This system can be controlled with the follwoing commands:

autosms

Shows if the autosms system is enabled or disabled

autosms on | off

Switches automatic notfications on or off.

autosms number [val]

Without a parameter, displays the telphone number for the SMS, with a
parameter configures the telephone number. Telephone numbers must be all
numbers without hyphens or anything else.

autosms maxidle [val]

Without a parameter, queries the idle time before a SMS is sent. With a
parameter sets a new value for the inactivity period which triggers a
message.

Please use the configurable settle time with environment controllers
such as temperature controllers in order to avoid false messages while
waiting for the temperature to settle.
