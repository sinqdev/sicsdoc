### The New SICS Four Circle Command Set

This is the documentation for the new integrated four circle single
crystal command set to be implemented at ZEBRA, ORION and MORPHEUS. This
command set is very much modelled according to the examples given by
CAD-4 control software and other similar four circle control programs.

#### Tutorial

There is a small tutorial in the form of a [four circle example
run](TRICSExample.md) which shows how the commands work together to
create a four circle user experience.

* * * * *

#### User Commands


All the parameter setting commands honour a common pattern:

    commandname

will print the current value of the parameter

    commandname newvalue

will set the parameter to the new values(s) specified.

Commands following this pattern will be shown as command [val] in the
following lists.

* * * * *

#### Setup


    projectdir [dir]

query or set the current project directory.

    title [newtitle]

set or query the experiment title

    user [username]

set or query the user of the instrument

    phone [phone-number]

user phone number.

    address [user-address]

user address.

    sample [samplename]

set or query the sample name

    lambda [val]

set or query the wavelength

    cell [a b c alpha beta gamma]

set or query the cell constants

    spgrp [spacegroupname]

Query or set the space group. Input is a space group name as in
Internation Tables Volume A. The space group is used to determine
systematic extinctions when generating indices.

    savexxx filename

saves all the crystallographic settings into a file. The resulting file
can be executed as a normal SICS batch file in order to recover all
settings.

#### ZEBRA Special Setup Commands

    zebraconf

This command will investigate the state of ZEBRA by querying the SPS, motor controller etc. and
adjust ZEBRA accordingly. It will take care of:

- Selecting the correct detector
- Switching file mode
- Switching the instrument mode depending on the presence of the eulerian cradle
- Discovering the correct wavelength from the position of the monochromator lift
- Driving the analyser theta and two theta to the correct value when the analyser is present
- Make sure that the correct motor for nu is configured  

When zebraconf runs, it prints some ouput giving an indication what has been detected and what 
is being done. 

    instrmode [bi|nb|tas]

query or set the diffractometer mode. Which can be bi for conventional
four circle geometry, nb for normal beam geometry or tas for TAS
geometry where the goniometer tables are used to position the crystal. Please
note that the UB matrix, reflection positions etc are very different
from tas to either bi or nb mode due to the very different geometry.

    detmode [single|area]

query or set the detector mode. ZEBRA has two detector modes: **single**
for using the single detector, **area** for using the area detector.

ZEBRA is equipped with a number of **slit** systems. When such a system has been 
mounted on the instrument it is necessary to enable the slit and run a reference run 
for each blade. This has been automatised with special slit initialisation commands. 
These are:

    initnose

for the beam extraction nose slit system

    inits1

for slit system 1

    inits2

for slit system 2

    initana

for the analyser slits. 

There are also a number ov virtual motors to drive the width and height of the slits. These are:

    s1h, s1v

for slit system 1

    s2h, s2v

for slit system 2

    s2anh, s2anv

for the analyser slits. The motor ending with h is for the height, the one ending with v for 
vertical is the width. 

ZEBRA also has a PG filter. This filter can be controlled through the **pgfilter** command.
Without parameters, it prints the state of the filter, other accepted parameters are in or out 
and those will cause the desired action.      


* * * * *

#### Slit Refinement 

This is a set of commands for automatically refining the position of the ZEBRA slits. 
The algorithm is a such in pseudocode:

    foreach reflection:
       foreach motor:
           open all slits
	   scan the slit blade until closed

After all scans have been collected they will be visually inspected bz Oksana who will then deduce the 
required slit openings.  
	   
    slitref 

prints help on the slitref usage. 


    slitref clear

Clear the reflection list for slit refinement.

    slitref add h k l

Adds a reflection to the list for slit refinement.

    slitref print

Prints the reflection list and refinement results.

    slitref motors left right top bottom

Sets the names of the slit motors to use.

    slitref openpos left right top bottom

Sets the all open position for each slit motor.

    slitref scan mode preset nopoints

Sets the scan parameters: scan mode, scan preset and the number of points for the scan.

    slitref ref step 

Does the actual slit scanning. 




* * * *
#### Driving and Scanning

SICS has the notion of **virtual motors**. These are software motors
which in fact drive multiple physical motors when they are driven. Such
virtual motors can be used like physical ones in drive or scan commands.
This scheme is implemented for the reciprocal space coordinates h,k,l
and for the cone angle.

The syntax then is:

    drive motor value

drives a physical or virtual motor to value.

    drive hkl h k l

special syntax for easier driving in reciprocal space. H, k,l are the
miller indices for the new position in reciprocal space.

    sscan mot1 start1 end1 mot2 start2 end2 ... np preset

make a simple scan from start to end. With np points and the preset
preset at each scan point.

    cscan mot center step np preset

make a center scan on mot around center with the stepwidth step and np
points to each side of center. Use the preset preset for counting.

    psiscan h k l step-psi step-om om-np preset [mode]

Performs a psi scan around h,k,l. The psi scan uses a step width of step
for psi and a preset as given as a parameter. There is a omega scan at
each psi position. The width of this scan can be determined by the
parameters step-om and om-np.The scan count mode is set with the
optional parameter mode. If mode is not given the mode of the last scan
will be used. The psi scan starts a 0 and goes till 360. PSI scans can
only be performed in bisecting mode with a single detector. At each psi
point a small omega scan is performed. The data is saved to a ccl file.

The data file format written is dependent on the detector mode. For the
area detector it is binary NeXus, for single detector mode it is the
ASCI scan file format.

The following motors are known:

    h

reciprocal space h coordinate

    k

reciprocal space k coordinate

    l

reciprocal space l coordinate

    cone

cone angle for cone scans

    stt

two theta

    om

omega

    chi

chi

    phi

phi

    nu

tilt motor in normal beam mode.


##### Fastscan

There is a fastscan facility. This scan starts a
motor and the counter. As often as possible or configured, the scan
module will calculate the difference to the previous count, normalize it
to the monitor difference and print it. This is usually faster then
doing a step scan. There are limitations, though:

-   Fastscans are restricted to one motor. There is no way that a
    coordinated movement of several motors can be enforced by fastscan
    without mechanical coupling.
-   Due to the volatile nature of the beam at SINQ, this type of scans
    come into problems when the beam is off. A warning is printed,
    but...
-   If the motor is driving to fast, there might not be enough neutrons
    around even in the peak to be registered.

Basically, **if you see a feauture in fastscan, it is there, if not,
that does not mean anything!**. Fastscan comes into its own for
alignment scans or when searching peaks. The syntax is:

    fastscan motor start stop speed

Runs a fastscan on motor between start and stop. Speed is the speed with
which the motor is run. The motors speed is reset to the original value
when fastscan finishes.

    diffscan skip val

If fastscan produces to much output on to short intervalls, increasing
the skip parameter allows to control that. Skip is the number of SICS
cyles to skip between measurements. Thus this value is highly dependent
on the overall performance of SICS.

* * * * *

#### Reflection List

For UB matrix calculations and the refinement of the UB, a set of
centered reflections is required. As a means of storage for such
reflections, SICS has a reflection list. This reflection list can be
maintained with the commands described below.

    refclear

clear the reflection list

    reflist

print the reflection list

    refadd ang [stt om chi phi]

add a reflection with angles to the list. If no angles are given, the
current motor positions are read.

    refadd idx h k l

Add a reflection with indices h,kl, to the list.

    refadd idxang h k l [stt om chi phi]

add a reflection with indices and angles to the list. If no angles are
given, the current motor positions are read.

    refdel id

delete the reflection with id from list

    refhkl id h k l

set the indices of the reflection with the id id to h,k, l

    refang id [stt om chi phi]

set the angles for the reflection with the id id to the values given. If
the values are omitted, the current motor positions are used.

    refindex

Index the current reflection list with the current UB. Indices are
rounded to the next integer.

    refsave filename

saves the reflection list to filename.

    refload filename

loads a stored reflection list from filename.

* * * * *

#### Centering

    centerref preset [mode] [step-stt] [step-om] [step-chi] [step-phi]

find the reflection maximum starting at the current position. The
instrument must be positioned somewhat on the peak. Use the preset
preset for counting. If the mode is not given it defaults to monitor.
The other parameters allows to specify the stepwidth to use for
centering in stt, om, chi and phi.

    centerlist preset [mode] [skip] [omscan]

Center all the reflections in the current reflection list. If no angles
are given for reflections, then the angles will be calculated. Use the
preset preset for counting. If the mode is not given it defaults to
monitor. The usual reflection list commands like reflist etc. described
above apply in order to view the results of the centering. The optional
parameter skip determines how many reflections in the list to skip. This
helps when there are already centered reflection in the list. The full
command to skip reads: centerlist 20000 monitor 10. To skip 10
reflections and measure the rest. Omscan determines if an omega scan is done after 
centering or not. If omscan is 1 (the default), the omega scan is done, with 0 
it is supressed.

    centerload filename

load an external list of reflections in filename to replace the list of
reflections to center.

* * * * *

#### Reflection Search and Indexing

There is a standard four circle peak search algorithm implemented in
SICS. In pseudo code this is:

    foreach two theta from min2t to max2t in steps of step2t
      foreach chi from 0 to 90 in steps of  chipstep
           do a phi scan from 0 to 180 with phistep
           look for peaks in the scan
           center each peak found in the scan
           add each centered peak into the reflection list
      end foreach
    end foreach

In normal beam mode, the algorithm is slightly different:

    foreach two theta from min2t to max2t in steps of step2t
      foreach nu from nu.lowerlim to nu.upperlim in steps of  nustep
           calculate gamma from stt and nu
           drive gamma and nu
           do a om scan from om.lowerlim to om.upperlim with omstep
           look for peaks in the scan
           center each peak found in the scan
           add each centered peak into the reflection list
      end foreach
    end foreach

Thus scanning in nu and omega happens within the software limits of the
respective motors.

    confsearch [min2t step2t max2t stepchi stepphi chimin chimax phimin phimax]

lists or configures the peak search parameters. The parameters are:
min2t = minimum two theta for peak search, step2t = step in two theta
during peak search, max2t = maximum two theta to search, stepchi = the
step to make in chi, stephi = the step in phi to make. Then the limits:
chimin, chimax determine the limits to search in chi, phim in, phimax
the limits in phi to search. This is for reflection search for the
four circle geometry.

    confsearch par [value]

alternative syntax to above which allows to set or retrieve a single
peak search configuration parameter.

    confsearchnb [min2t step2t max2t stepom stepnu]

configure searching in the normal beam mode. Omstep is the step to make in
omega, nustep is the step to make in nu.

    search preset maxpeak [mode]

perform a peak search with counting with the given preset and mode. The
search terminates when maxpeak reflections have been found. Mode is the
countmode and is optional; if omitted it defaults to monitor.

    indexhkl [stt]

suggest indices for the the two theta value specified based on the cell
constants. If no value for two theta is given, the current motor
position is read. This requires the cell constants to be set properly
with the cell command. This command is dependent on the configuration command 
indexconf, see below.

    coneconf [id-center th tk tl [qscale]]

prints or configures the cone motor configuration. Id-center is the ID
of the center reflection in the reflection list, This must be one with
both indices and angles. th, tk, tl are the indices of the target
reflection. Qscale is an optional parameter which allows to shorten or
lengthe the q vector. The default is 1. Once this configuration has been
done, one may use cone as a normal motor in scan commands.

    findpeaksinscan

is a support command which locates the positions of candidate peaks in
the data from the last scan.

#### Simple Indexing

There is a very simple indexing module for neutron scattering. If a user
arrives at a neutron scattering single crystal diffractometer she
usually knows the lattice constants and other crystallographic
parameters quite well. Thus a full blown indexing routine which tries to
find the cell constants and the spacegroup symmetry as used at normal
four circle diffractometers is not needed. Especially as such routines
usually require 10-20 reflections in order to work properly. In neutron
scattering one rather wishes to find a minimum of two reflections and
have a UB quickly. This is the purpose of this module.

The algorithm is simple.

1.  From the list of reflections three non coplanar reflections at low
    two theta are selected. Low two theta because then the number of
    possible indices is low.
2.  For each reflection possible indices are generated for which the two
    theta calculated from the indices and the cell constants matches the
    measured two theta within a user defined limit.
3.  Each combination of indices is then checked for two conditions:
    -   The angles between the scattering vectors must match the angles
        calculated from the lattice constants and the indices.
    -   The indices must build a right handed coordinate system.

4.  Each matching set of indices constitutes a possible solution to the
    indexing problem.

The algorithm works with two reflections as well. But the test for right
handeness is not possible then.

#### Prerequisites

1.  The cell constants were given through *singlex cell*
2.  The spacegroup was defined through *singles spacegroup*
3.  The lambda value is correct
4.  Two, better three, reflections had been centered and entered into
    the reflection list.

#### Commands for Simple Indexing

    indexconf [sttlim anglim]

configures or queries the simple indexing routine. Sttlim is the maximum stt considered 
for indexing, anglim is the angle difference allowed for a reflection to be considered 
a solution. 

    index

runs the simple indexer against the reflections in the reflection list.
At the end a list of possible solutions is printed. This list contains a
goodness of fit, GOF. This number is the sum of all differences in two
theta and angle multiplied by 100. Smaller is beautiful in this case.

    indexub no

set a UB from the indexing result number of the last index run.

    indexhkl [stt]

suggest indices for the the two theta value specified based on the cell
constants. If no value for two theta is given, the current motor
position is read. This requires the cell constants to be set properly
with the cell command.


#### Dirax Indexing

For more complex indexing requirements the program dirax is recommended.
Please note that dirax needs at least 10, better 20 reflections in order
to work properly. There is one command:

    indexdirax

which writes a file: sics.dx with all your current reflections into your
projectdir.

You have to change directory to the project directory and run dirax
interactively with *dirax sics.idx*. You also have to copy the final UB
matrix into SICS manually. Typing help at the dirax prompt opens a
WWW-browser with usage instructions for dirax.

* * * * *

#### UB Matrix and Calculations

UB matrix refinement in SICS happens through the external programs rafin
and rafnb, rusty trusty workhorse programs from ILL. The way it works is
that SICS writes an input file for the rafin program into your project
directory. It also runs the program. SICS allows to view the refinement
results and to load the refined UB into SICS. SICS writes an input file
for a reasonable default refinement. Cell constants may be refined and
constraints according to the lattice type will automatically be
generated when the spacegroup had been set properly. Please note that
cell constant refinement only makes sense if there are enough well
centered reflections at low and high to theta angles in your reflection
list. If you have more advanced refinement requirements, change
directory to your project directory and manually edit the rafin input
file and run rafin under your control. If you wish to load the resulting
UB matrix into SICS, make sure that the results end up in the file
rafin.lis. For example by running:

    rafin > rafin.lis

in the project directory.

UB refinement by SICS is controlled through the commands:

    ubrefine [cell]

writes an input file for the refinement program and runs it. If the
optional key cell is given, the cell constants are refined too.

    ubshow

shows the results of the last refinement again.

    ubload

loads the UB and cell constants from the refinement results.

    ubcalc id1 id2 [id3]

calculate an initial UB matrix from indexed reflections identified by
their ids. If only two reflections are given the UB is calculated from
these reflections and the cell; if three are given, the UB is calculated
from the three reflections only. For each reflections there must be
indices and angles.

    ubreco

recover the previous UB matrix. This helps if a UB calculation results
are not useful.

    ub [ub11 ub12 ub13 ub21 ub22 ub23 ub31 ub32 ub33]

set or query the current UB matrix.

    hkltoang h k l [psi]

calculate and print angles for the reflection h,k,l using the current UB
matrix. This takes an optional psi argument to calculate the angles for a specific psi.

    angtohkl [stt om chi phi]

calculate the current indices from the angles with the current UB. If no
angles are given, the current motor positions are used.

    bitonb stt om chi phi

calculates normal beam angle gamma, om, nu from bisecting reflection
angles stt, om, chi and phi. The resulting angles are valid for chi = 0,
phi = 0. If there are any offsets to this on the instrument, they need
to be applied before this calculation.

    calcstt h k l

calculates two theta for h,k,l from the cell constants alone. Useful in
the early stages of setup when trying to locate the first reflection.

* * * * *

### Data Collection

Data collection is controlled by a table which determines the preset,
scan mode and number of points depending on two theta. A suitable table
should have been provided by your instrument responsible for you. This
table can be edited with the following commands:

    tablist

list the current measurement parameter table.

    tabclear

clear the table

    tabdel no

delete entry number no from the table

    tabadd sttend scanvar step np preset

adds a new entry to the measurement table. Sttend value is the end value
of two theta this entry is valid for. Scanvar is the scan variable to
use; this can be om for omega scans or o2t for omega two theta scans.
Step is the stepwidth to use, always given as omega step widths. NP is
the number of points to scan for each reflection. Preset is the count
preset to count for each scan point.

    tabsave filename

save the current data collection table to filename in project directory.

    tabload filename

loads a saved data collection table back into SICS. The file is searched
for in the project directory

Before data collection can start a list reflections to measure has to be
generated. This happens with the following commands. Please note that
both cell and spgrp have to be set properly for index generation to work
properly.

    loadx filename

loads reflection indices to measure from the file specified by filename.
The file is expected to be in the project directory.

    testx [del] [sym]

tests each reflection from the list if it is possible to measure it. If
the optional keyword del is given, reflections which cannot be measured
due to instrument constraints are removed from the list. If a reflection
is unreachable and the optional keyword sym is set test tries to find a
symetrically equivalent reflection which is reachable. If such a
reflection is found, it replaces the unreachable reflection with its
symetrically equivalent.

    hkllimit [hmin kmin lmin hmax kmax lmax sttmin sttmax]

Query or set the imits for index generation.

    hklgen [sup|opp]

Generate reflection indices with the current settings. Please note that
the expected results are only achieved when both the spacegroup and the
cell have been properly configured. The optional keyword sup suppresses
by its existence symmetrically equivalent reflections. The optional
keyword opp generates all those reflections which are forbidden by the
spacegroup. This may be useful when looking at super structures.


    hklsort

sort the data collection list by two theta. This is an important
optimisation for the instrument.

    travelsort

This command sorts the reflection list in a way as to minimise the
driving between reflections. To this purpose the reflection sorting
problem is treated as travelling salesman problem (TSP) which is solved
using an implementation of the Lin-Kernighan heuristics. This sorting
algorithm takes a little while to run. The result will be a list which
starts at a certain two theta nd then moves through the reflection list
and returns back to the same two theta. It makes a tour through the
reflection list. This is the most efficinet way of processing a reflection 
list as the algorithm minimses the path tp travel between reflections.

    hklsave filename

saves the current data collection list to filename in the project
directory. Can be loaded again with loadx.

    hkllist

print the data collection list on screen.

Actual data collection happens through the following comands:

    collconf [countmode weak weakthreshold]

shows or sets data collection configuration parameters. These are the
countmode which can be either monitor or timer, a flag, weak, which if 1
causes weak reflections to be remeasured with 5 times the normal preset,
and weakthreshold which is a threshold used to determine if a reflection
is considered weak.

    collect [skip|append]

Starts data collection. The optional parameter skip allows to skip the
first skip reflections. This feature supports the continuation of
interrupted data collections. Data files will be written in ccl mode for
single detector data or a nexus file for each reflection is created when
the area detector is used. If skip has the keyword append, then the new
data is appended to the last data file.

* * * * *
#### Incommensurate Commands

In addition to the commands mentioned above there is a set of commands for 
dealing with incommensurate reflections. SICS keeps them in a separate list because 
their peaks are usaullay and ugly and need different parameters during data 
collection. 

    satref clear

This command clears the satellite reflection list. 

    icmgen hw kw lw

add incommensurate reflections for each main reflection to the 
sattelite reflection list, using hw,kw,lw as the reciprocal displacement vector. 
This has to be called directly after hklgen. It can be called multiple times.


    satsave filename

Saves the sattelite reflection list to filename

    satload filename

Loads the reflections from file filename into the sattelite reflection list.

    satlist

Lists the sattelite reflection list

    testx sat  [del] [sym]

tests each reflection from the sattelite reflection list if it is possible 
to measure it. If the optional keyword del is given, reflections which 
cannot be measured due to instrument constraints are removed from the list. 
If a reflection is unreachable and the optional keyword sym is set test 
tries to find a symetrically equivalent reflection which is reachable. 
If such a reflection is found, it replaces the unreachable reflection with its
symetrically equivalent.

    hklsort

Described above also sorts the sattelite reflection list. 

    satcollect [skip|append]

Starts data collection for the sattelite reflection list . The optional parameter 
skip allows to skip the first skip reflections. This feature 
supports the continuation of interrupted data collections. Data files will be 
written in ccl mode for single detector data or a nexus file for 
each reflection is created when the area detector is used. 
If skip has the keyword append, then the new data is appended to the last data file.

The commands *collconf* and the data collection table commands described above aplly too. 
The *travelsort* command is currently not implemented for incommensurate reflections. 
