### System Commands

**sics\_exitus**. A single word commands which shuts the server down.
Only managers may use this command.

**wait *time*** waits time seconds before the next command is executed.
This does not stop other clients from issuing commands.

**resetserver** resets the server after an interrupt.

**dir** a command which lists objects available in the SICS system. Dir
without any options prints a list of all objects. The list can be
restricted with:

   dir var

prints all SICS primitive variables

    dir mot

prints a list of all motors

    dir inter driv

prints a list of all drivable objects. This is more then motors and
includes virtual motors such as environment devices and wavelength as
well.

    dir inter count

Shows everything which can be counted upon.

    dir inter env

Shows all currently configured environment devices.

    dir match wildcard

lists all objects which match the wildcard string given in wildcard.

**status** A single word command which makes SICS print its current
status. Possible return values can be: Eager to execute commands,
Scanning, Counting, Running, Halted. Note that if a command is executing
which takes some time to complete the server will return an ERROR: Busy
message when further commands are issued.

**status interest** initiates automatic printing of any status change in
the server. This command is primarily of interest for status display
client implementors.

**backup *[file]*** saves the current values of SICS variables and
selected motor and device parameters to the disk file specified as
parameter. If no file parameter is given the data is written to the
system default status backup file. The format of the file is a list of
SICS commands to set all these parameters again. The file is written on
the instrument computer relative to the path of the SICS server. This is
usually /home/INSTRUMENT/bin.

**backup motsave** toggles a flag which controls saving of motor
positions. If this flag is set, commands for driving motors to the
current positions are included in the backup file. This is useful for
instruments with slipping motors.

**restore *[file]*** reads a file produced by the backup command
described above and restores SICS to the state it was in when the status
was saved with backup. If no file argument is given the system default
file gets read.

**restore listerr** prints the list of lines which caused errors during
the last restore.

**killfile** decrements the data number used for SICS file writing and
thus consequently overwrites the last datafile. This is useful when
useless data files have been created during tests. As this is critical
command in normal user operations, this command requires managers
privilege.

**sicsidle** prints the number of seconds since the last invocation of a
counting or driving operation. Used in scripts.
