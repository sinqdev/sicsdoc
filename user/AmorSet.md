### AMOR Settings Module

This is the documentation for the new improved settings module for the
reflectometer AMOR. The problem is that two theta on AMOR is realized
partly through translational tables. Moreover the positions of various
slits have to be adjusted which can be in the beam or not. Contrary to
an older settings module this module assumes the straight beam as the
zero point for all heights rather then the optical bench.

The AMOR settings module has so many parameters that they have been
ordered according to components. Indivdual component parameters can be
accessed by prefixing them with **amorset componentname**. For example
*amorset slit2 active* will print the active parameter for slit2,
*amorset slit2 active 1* will set the active flag for slit2 to 1. All
AMOR settings module parameter commands follow the usual SICS syntax:
the parameter command alone prints the current value of the parameter,
the component command followed by a new value sets a new value for the
parameter. The following components are installed:

    mono

the monochromator

    ds

The exit slit of the monochromator bunker.

    slit2

The first pre sample slit

    slit3

The second pre sample slit

    sample

The sample position

    slit4

The first after sample slit

    detector

The detector

    ana

The analyzer

Each of these components has the following parameters:

**active**

1 if f the component is active, 0 else. This is for disabling components
which happen not be in the beam in a given setup

**offset**

a general offset for this component. This is fairly fixed and is
associated with the size of the component.

**scaleoffset**

offset between the component position and the scale used for reading its
position.

**read**

The value read for the component on the optical benchs scale.

**list**

lists the parameters associated with this component.

The AMOR settings module itself has a few additional parameters:

    amorset dspar

This read or sets a correction factor for the monochromator exit slit.

    amorset verbose

A flag which makes the AMOR settings module create more output about
what it is doing.

    amorset cd

apparently calculates the detector height.

In addition the AMOR settings module defines a couple of virtual motors
which can be driven using the normal drive command or which can be used
in scans. The following virtual motors are provided:

    m2t

monochromator two theta

    ath

analyzer theta

    s2t

sample two theta
