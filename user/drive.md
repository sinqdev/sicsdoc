### Drive commands

Many objects in SICS are **drivable**. This means they can run to a new
value. Obvious examples are motors. Less obvious examples include
composite adjustments such as setting a wavelength or an energy. Such
devices are alos called virtual motors. This class of objects can be
operated by the **drive, run, Success** family of commands. These
commands cater for blocking and non-blocking modes of operation.

    run var newval var newval ...

can be called with one to n pairs of
object new value pairs. This command will set the variables in motion
and return to the command prompt without waiting for the requested
operations to finish. This feature allows to operate other devices of
the instrument while perhaps a slow device is still running into
position.

    Success [RUNDRIVE]

waits and blocks the command connection until all
pending operations have finished (or an interrupt occured). The option
argument RUNDRIVE causes succes to only wait for motors and counts but
not for sample environment.

    drive var newval var newval ...

can be called with one to n pairs of
object new value pairs. This command will set the variables in motion
and wait until the driving has finished. A drive can be seen as a
sequence of a run command as stated above immediatly followed by a
Success command.
