Wavelength adjustment for a Velocity Selector
=============================================

The wavelength selected by a neutron velocity selector depends on the
tilt angle of the selector and its rotation speed. This is what the
velocity selector wavelength command is for. It is usually installed
into the system as lambda. This variable can be driven using the normal
[drive and run](drive.htm) commands. Additionally this command
understands a few sub commands. For the following discussion is assumed
that the name of the command is lambda.

lambda

The name of the variable alone prints the current wavelength in nm.

lambda rot val

calculates the rotation speed for the wavelength given by val.

lambda wl val

calculates the wavlength for the rotation speed given as parameter val.
