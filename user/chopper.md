### Chopper Control

Some instruments are equipped with a Dornier Chopper system running one
or more choppers: a disk chopper and possibly a fermi chopper. In most
situations the diskchopper is in slave mode. This means his speed is a
predefined ratio of the speed of the fermichopper. Furthermore, there is
a phase difference between the two choppers in order to allow for the
fligh time of neutrons between choppers.

The program handling RS-232 requests at the chopper control computer is
rather slow. This would slow the SICS server to unacceptable levels, if
any request would be handled through the RS-232 interface. In order to
cope with the problem, the SICS server buffers chopper information. This
information is updated any minute if not set otherwise.

The chopper system control is divided into several distinct objects:
There is the actual chopper controller which mainly serves for answering
status requests. Then there are a couple of virtual motors which
represent the four modifiable parameters of the chopper control system.
These can be driven through the normal [drive](drive.htm) command. The
commands understood by the chopper controller object are:

    choco list

prints a listing of all known chopper parameters.

    choco name

print only the value of parameter name. Possible values for name can be
extratcted from the list printed with choco list.

    chosta

This command procedure prints a status listing of the chopper system in
a nicely formatted stefan-happy form.

The following virtual motor variables exist for the chopper system.

    fermispeed

fermi chopper speed

    diskspeed or chopperspeed

disk chopper speed. Note, that driving this parameter while the chopper
system is in synchronous mode will throw an error condition.

    phase

The phase difference between the two choppers.

    ratio

The ratio of fermi to disk chopper speeds.

    updateintervall

The update intervall for the buffering of chopper data. Units are
seconds. Setting to low values here will compromise the responsiveness
of the SICS server.

Each of the variables kindly prints its current value when given as a
command. Modifying values happens through the normal drive command. For
instance the command:

    drive fermispeed 10000

will drive the fermi chopper to 10000 RPM eventually and if no problem
occurs. Please check your input carefully for all chopper commands.
Dornier has provided no way to stop an erraneous command in its
software. So, if you intend to run the chopper to 1000 RPM and mistyped
it as 10000 then you'll wait for 20 minutes until the chopper is at
speed!
