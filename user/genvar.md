### General SICS Variables

There are a number of SICS variables which can be set by the user. The syntax is easy:

    varname

will print the value of the variable

    varname new-value

Will replace the value of varname with new-value.

The following common SICS variables exist:

**user**

Supposed to contain the name of the current user

**sample**

The sample name

**title**

The title of the experiment

**comment**

A comment for the experiment. Will be written to the data file.

**sicsdatanumber**

The current file number. This is read only as it is automatically managed by SICS. 
