### Table Execution in SICS

SICS has a new feature which enables it to execute tables. Tables can replace batch files. In order to use it two things need to be done:

1.  Create a table and store it in comma separated value (csv) format
2.  Execute the table in SICS with the command tableexe

#### Creating the Table


Let us look at two examples first:


|title|sample|dt|gphi|monitor|command|
|-----|------|--|----|-------|-------|
|Test1|Testonit1|pos10|5.5|10000|cscan som -1 .1 10 1000|
|Test2|Testonit2|pos20|7.5|5000|cscan som -1 .1 10 2000|
|Test3|Testonit3|pos10|5.5|10000|cscan som -1 .1 10 1000|
|Test4|Testonit4|pos5|2.5|2000|cscan som -2 .2 30 10000|

This sets for each row title and sample first, drives phi, counts to the preset given and executes a scan. Another example:


|title|sample|dt|gph|compar|scanvar|start|step|np|preset|
|-----|------|--|---|------|-------|-----|----|--|------|
|Test1|Testonit1|pos10|5.5|cscan|som|-1|.1|10|1000|
|Test2|Testonit2|pos20|7.5|cscan|som|-1|.1|10|2000|
|Test3|Testonit3|pos10|5.5|cscan|som|-1|.1|10|1000|
|Test4|Testonit4|pos5|2.5|cscan|som|-2|.2|30|10000|

This sets for each row the title and the sample. drives gphi and the runs the scan with the parameters given.

The first line of the table is important: SICS uses it to determine what to do the with the data in the table columns. For table column names, you may use SICS object names and a couple of special words.

The SICS table processor knows what to do with most SICS objects. This include SICS variables, SICS motors like real motors, software motors and environment variables like temperature, SANS multi motor objects, macros etc. In the example above, sample and title are SICS variables and SICS will set their values when processing the table row, gphi is a motor and SICS will drive it to the value given in the row, dt is a SANS multi motor object and SICS will drive it to the named position given in the row. If there is a SICS object which is not understood by the table processor, please contact Mark Könnecke to have it fixed.

Besides SICS objects the table columns can have special names:

**monitor**

This is automatically converted by SICS into: count monitor value-in-row. For example count monitor 10000 for the second row in the example.

**timer**

Correspondingly is translated by the SICS table processor into count timer value-in-row.

**command**

Interpret the value in the table row as an arbitrary SICS command. In the example above, scan commands are given.

**compar**

Concatenate this column plus all following ones to yield a command to execute. See second example above.

**batch**

interpret the value as the name of a batch file to execute.

Tables can be edited with any application capable of dealing with the CSV format. Recommended, tested and readily available is open office calc. Once you are done with table editing, the table **MUST be stored in CSV format** somewhere where SICS can find it. In OOCalc use: File/Save As and select CSV as format in the drop down list.

#### Executing Tables in SICS

This is the easy bit:

    tableexe tablename

executes your table. SICS finds the table in the current batchpath.

#### How It all Works


This section is only for people who are not easily confused. It describes the inner workings of tableexe. The first thing tableexe does is to store the header. Then it processes each row:

    foreach row:
      foreach column:
           Look at the header and the data:
                 If it is something like a variable: just set it
                 If it is a motor: start the motor with run
                 If it is a count class command: monitor, timer, command or batch: 
                      store it in a list for later execution
      end foreach column
      Do a success comand in order to wait for all motors to arrive.
      Execute the count command in succession.
    end foreach row

The general idea is that motors and variables are set and driven first before anything to do with counting is done. It also saves the user from having to input the column names in the right order. Though this will not generally be used it is possible to have several count operations, for instance a count and a scan, for each rwo. It is recommended however that if you change something, like the polarisation of a magnet, to use a new row.
