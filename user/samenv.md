### Sample Environment Devices


#### SICS Concepts for Sample Environment Devices

SICS can support any type of sample environment control device if there
is a driver for it. This includes temperature controllers, magnetic
field controllers etc. The SICS server is meant to be left running
continously. Therefore there exists a facility for dynamically
configuring and deconfiguring environment devices into the system. This
is done via the **EVFactory** command. It is expected that instrument
scientists will provide command procedures or specialised Rünbuffers for
configuring environment devices and setting reasonable default
parameters.

In the SICS model a sample environment device has in principle two modes
of operation. The first is the drive mode. The device is monitored in
this mode when a new value for it has been requested. The second mode is
the monitor mode. This mode is entered when the device has reached its
target value. After that, the device must be continously monitored
throughout any measurement. This is done through the environment monitor
or **emon**. The emon understands a few commands of its own.

Within SICS all sample environement devices share some common behaviour
concerning parameters and abilities. Thus any given environment device
accepts all of a set of general commands plus some additional commands
special to the device.

In the next section the EVFactory, emon and the general commands
understood by any sample environment device will be discussed. This
reading is mandatory for understanding SICS environment device handling.
Then there will be another section discussing the special devices known
to the system.

#### Sample Environment Error Handling

A sample environment device may fail to stay at its preset value during
a measurement. This condition will usually be detected by the emon. The
question is how to deal with this problem. The requirements for this
kind of error handling are quite different. The SICS model therefore
implements several strategies for handling sample environment device
failure handling. The strategy to use is selected via a variable which
can be set by the user for any sample environment device separately.
Additional error handling strategies can be added with a modest amount
of programming. The error handling strategies currently implemented are:

**Lazy**

Just print a warning and continue.

**Pause**

Pauses the measurement until the problem has been resolved.

**Interrupt**

Issues a SICS interrupt to the system.

**Safe**

Tries to run the environment device to a value considered safe by the
user.

**Script**

Run a user defined script to do any magic things you may want.

#### General Sample Environment Commands

##### EVFactory

EVFactory is responsible for configuring and deconfiguring sample
environment devices into SICS. The syntax is simple:

**EVFactory new name type par par ...**

Creates a new sample environment device. It will be known to SICS by the
name specified as second parameter. The type parameter decides which
driver to use for this device. The type will be followed by additional
parameters which will be evaluated by the driver requested.

**EVFactory del name**

Deletes the environment device name from the system.

##### emon

The environment monitor emon takes for the monitoring of an environment
device during measurements. It also initiates error handling when
appropriate. The emon understands a couple of commands.

**emon list**

This command lists all environment devices currently registered in the
system.

**emon register name**

This is a specialist command which registers the environment device name
with the environment monitor. Usually this will automatically be taken
care of by EVFactory.

**emon unregister name**

This is a specialist command which unregisters the environment device
name with the environment monitor. Usually this will automatically be
taken care of by EVFactory Following this call the device will no longer
be monitored and out of tolerance errors on that device no longer be
handled.

#### General Commands UnderStood by All Sample Environment Devices

Once the evfactory has been run successfully the controller is installed
as an object in SICS. It is accessible as an object then under the name
specified in the evfactory command. All environemnt object understand
the common commands given below. Please note that each command discussed
below MUST be prepended with the name of the environment device as
configured in EVFactory!

The general commands understood by any environment controller can be
subdivided further into parameter commands and real commands. The
parameter commands just print the name of the parameter if given without
an extra parameter or set if a parameter is specified. For example:

> Temperature Tolerance

prints the value of the variable Tolerance for the environment
controller Temperature. This is in the same units as the controller
operates, i. e. for a temperature controller Kelvin.

> Temperature Tolerance 2.0

sets the parameter Tolerance for Temperature to 2.0. Parameters known to
ANY envrironment controller are:

**Tolerance**

Is the deviation from the preset value which can be tolerated before an
error is issued.

**Access**

Determines who may change parameters for this controller. Possible
values are:

-   0 only internal
-   1 only Managers
-   2 Managers and Users.
-   3 Everybody, including Spy.

**LowerLimit**

The lower limit for the controller.

**UpperLimit**

The upper limit for the controller.

**ErrHandler.**

The error handler to use for this controller. Possible values:

-   0 is Lazy.
-   1 for Pause.
-   2 for Interrupt
-   3 for Safe.
-   4 for Script.

For an explanantion of these values see the section about
[error](#error) handling above.

**errorscript**

The user specified script to execute when the controlled value goes out
of tolerance. Will be used whne the ErrHandler 4, script, is used.

**Interrupt**

The interrupt to issue when an error is detected and Interrupt error
handling is set. Valid values are:

-   0 for Continue.
-   1 for abort operation.
-   2 for abort scan.
-   3 for abort batch processing.
-   4 halt system.
-   5 exit server.

**SafeValue**

The value to drive the controller to when an error has been detected and
Safe error handling is set.

**MaxWait**

Maximal time in minutes to wait in a drive temperature command. If
maxwait is set to 0: If the temperature is not reached within tolerance,
it waits indefinitely.

**Settle**

Wait time [minutes] after reaching temperature. Indicates how long to
wait after reaching temperature. If the temperatures goes again out of
tolerance during the settling time, the time outside tolerance is not
taken into account.

Additionally the following commands are understood:

**send par par ...**

Sends everything after send directly to the controller and return its
response. This is a general purpose command for manipulating controllers
and controller parameters directly. The protocoll for these commands is
documented in the documentation for each controller. Ordinary users
should not tamper with this. This facility is meant for setting up the
device with calibration tables etc.

**list**

lists all the parameters for this controller.

no command, only name.

When only the name of the device is typed it will return its current
value.

name val

will drive the device to the new value val. Please note that the same
can be achieved by using the drive command. and **log frequency** (both
below)

##### Logging

The values of any sample environement device can be logged. There are
three features:

-   Logging to a file wih a configurable time intervall between log file
    entries.
-   Sums are kept internally which allow the calculation of the mean
    value and the standard deviation at all times.
-   A circular buffer holding 1000 timestamps plus values is
    automatically updated.

The last two systems are automatically switched on after the first drive
or run command on the environment device completed. This system is run
through the following commands.

**name log clear**

Resets all sums for the calculation of the mean value and the standard
deviation.

**name log getmean**

Calculates the mean value and the standard deviation for all logged
values and prints them.

**name log frequency val**

With a parameter sets, without a parameter requests the logging
intervall for the log file and the circular buffer. This parameter
specifies the time intervall in seconds between log records. The default
is 300 seconds.

**name log file filename**

Starts logging of value data to the file filename. Logging will happen
any 5 minutes initially. The logging frequency can be changed with the
name log frequency command. Each entry in the file is of the form date
time value. The name of the file must be specified relative to the SICS
server.

**name log flush**

Unix buffers output heavily. With this command an update of the file can
be enforced.

**name log status**

Queries if logging to file is currently happening or not.

**name log close**

Stops logging data to the file.

**name log tosicsdata dataname**

copies the content of the circular buffer to a sicsdata buffer. This is
used by graphical clients to display the content of the circular buffer.

**name log dump**

Prints the content of the circular log buffer to screen.

**name log dumptofile filename**

Prints the content of the circular log buffer into the file specified as
filename. Note, this file is on the computer where the SICS server
resides.

#### Special Environment Control Devices


This section lists the parameters needed for configuring a special
environment device into the system and special parameters and commands
only understood by that special device. All of the general commands
listed above work as well!

##### LakeShore Model 340 Temperature Controller

This is *the* temperature controller for cryogenic applications and
should replace at least the Oxford & Neocera controllers at SINQ.

The control is handled by a seperate server process TECS (TEmperature
Control Server) and is initialized by default on most instruments. If
there is already an other device selected, it must be deleted with:

> EVFactory del temperature

and TECS must be reinstalled with:

> tecs on

(This is just an abbreavation for EVFactory new temperature tecs)

More details can be found on the [Sample Environment Home
Page](http://sinq.web.psi.ch/sinq/sample_env/tecs.html)

##### ITC-4 and ITC-503 Temperature Controllers

These temperature controller were fairly popular at SINQ. They are
manufactured by Oxford Instruments. At the back of this controller is a
RS-232 socket which must be connected to a terminal server via a serial
cable.

####### ITC-4 Initialisation

An ITC-4 can be configured into the system by:

> EVFactory new Temp ITC4 computer port channel

This creates an ITC-4 controller object named Temp within the system.
The ITC-4 is expected to be connected to the serial port channel of the
serial port server porgramm at localhost listening at the specified
port. For example:

> EVFactory new Temp ITC4 localhost 4000 7

connects Temp to the serial port 7, listening at port 4000.

###### ITC-4 Additional Parameters

The ITC-4 has a few more parameter commands:

**timeout**

Is the timeout for the SerPortServer waiting for responses from the
ITC-4. Increase this parameter if error messages contaning ?TMO appear.

**sensor**

Sets the sensor number to be used for reading temperature.

**control**

Sets the control sensor for the ITC-4. This sensor will be used
internally for regulating the ITC-4.

**divisor**

The ITC4 does not understand floating point numbers, the ITC-503 does.
In order to make ITC4's read and write temperatures correctly floating
point values must be multiplied or divided with a magnitude of 10. This
parameter determines the appropriate value for the sensor. It is usually
10 for a sensor with one value behind the comma or 100 for a sensor with
two values after the comma.

**multiplicator**

The same meaning as the divisor above, but for the control sensor.

###### Installing an ITC4 step by step

1.  Connect the ITC temperature controller to port 7 on the terminal
    server box. Port 7 is specially configured for dealing with the
    ideosyncracies of that device. No null modem is needed.
2.  Install the ITC4 into SICS with the command: \
     evfactory new temperature localhost 4000 7\
     You may choose an other name than "temperature", but then it is in
    general not stored in the data file. Please note, that SICS won't
    let you use that name if it already exists. For instance if you
    already had a controller in there. Then the command:\
     evfactory del name \
     will help.
3.  Configure the upper and lowerlimits for your controller
    appropriatetly.
4.  Figure out which sensor you are going to use for reading
    temperatures. Configure the sensor and the divisor parameter
    accordingly.
5.  Figure out, which sensor will be used for controlling the ITC4. Set
    the parameters control and multiplicator accordingly. Can be the
    same as the sensor.
6.  Think up an agreeable temperature tolerance for your measurement.
    This tolerance value will be used 1) to figure out when the ITC4 has
    reached its target position. 2) when the ITC4 will throw an error if
    the ITC4 fails to keep within that tolerance. Set the tolerance
    parameter according to the results of your thinking.
7.  Select one of the numerous error handling strategies the control
    software is able to perform. Configure the device accordingly.
8.  Test your setting by trying to read the current temperature.
9.  If this goes well try to drive to a temperature not to far from the
    current one.

###### ITC-4 Trouble Shooting

If the ITC-4 **does not respond at all**, make sure the serial
connection to is working. Use standard RS-232 debugging procedures for
doing this. The not responding message may also come up as a failure to
connect to the ITC-4 during startup.

If error messages containing the string **?TMO** keep appearing up
followed by signs that the command has not been understood, then
increase the timeout. The standard timeout of 10 microseconds can be to
short sometimes.

You keep on reading **wrong values** from the ITC4. Mostly off by a
factor 10. Then set the divisor correctly. Or you may need to choose a
decent sensor for that readout.

Error messages when **trying to drive the ITC4**. These are usually the
result of a badly set multiplicator parameter for the control sensor.

The ITC4 **never stops driving**. There are at least four possible
causes for this problem:

1.  The multiplicator for the control sensor was wrong and the ITC4 has
    now a set value which is different from your wishes. You should have
    got error messages then as you tried to start the ITC4.
2.  The software is reading back incorrect temperature values because
    the sensor and divisor parameters are badly configured. Try to read
    the temperature and if it does have nothing to do with reality, set
    the parameters accordingly.
3.  The tolerance parameter is configured so low, that the ITC4 never
    manages to stay in that range. Can also be caused by inappropriate
    PID parameters in the ITC4.
4.  You are reading on one sensor (may be 3) and controlling on another
    one (may be 2). Then it may happen that the ITC 4 happily thinks
    that he has reached the temperature because its control sensor shows
    the value you entered as set value. But read sensor 3 still thinks
    he is far off. The solution is to drive to a set value which is low
    enough to make the read sensor think it is within the tolerance.
    That is the temperature value you wanted after all.

##### Haake Waterbath Thermostat

This is sort of a bucket full of water equipped with a temperature
control system. The RS-232 interface of this device can only be operated
at 4800 baud max. This is why it has to be connected to a specially
configured port. The driver for this device has been realised in the Tcl
extension language of the SICS server. A prerequisite for the usage of
this device is that the file hakle.tcl is sourced in the SICS
initialisation file and the command inihaakearray has been published.
Installing the Haake into SICS requires two steps: first create an array
with initialisation parameters, second install the device with
evfactory. A command procedure is supplied for the first step. Thus the
initialisation sequence becomes:

> inihaakearray name-of-array localhost name port channel\
>  evfactory new temperature tcl name-of-array

An example for the SANS:

> inihaakearray eimer localhost 4000 1 \
>  evfactory new temperature tcl eimer

Following this, the thermostat can be controlled with the other
environment control commands.

The Haake Thermostat understands a single special subcommand:
**sensor**. The thermostat may be equipped with an external sensor for
controlling and reading. The subcommand sensor allows to switch between
the two. The exact syntax is:

> temperature sensor val

val can be either intern or extern.

##### Bruker Magnet Controller B-EC-1

This is the Controller for the large magnet at SANS. The controller is a
box the size of a chest of drawers. This controller can be operated in
one out of two modes: in **field** mode the current for the magnet is
controlled via an external hall sensor at the magnet. In **current**
mode, the output current of the device is controlled. This magnet can be
configured into SICS with a command syntax like this:

> evfactory new name bruker localhost port channel

name is a placeholder for the name of the device within SICS. A good
suggestion (which will be used throughout the rest of the text) is
magnet. bruker is the keyword for selecting the bruker driver. port is
the port number at which the serial port server listens. channel is the
RS-232 channel to which the controller has been connected. For example
(at SANS):

    evfactory new magnet bruker localhost 4000 9

creates a new command magnet for a Bruker magnet Controller connected to
serial port 9.

In addition to the standard environment controller commands this magnet
controller understands the following special commands:

**magnet polarity**

Prints the current polarity setting of the controller. Possible answers
are plus, minus and busy. The latter indicates that the controller is in
the process of switching polarity after a command had been given to
switch it.

**magnet polarity val**

sets a new polarity for the controller. Possible values for val are
**minus** or **plus**. The meaning is self explaining.

**magnet mode**

Prints the current control mode of the controller. Possible answers are
**field** for control via hall sensor or **current** for current
control.

**magnet mode val**

sets a new control mode for the controller. Possible values for val are
**field** or **current**. The meaning is explained above.

**magnet field**

reads the magnets hall sensor independent of the control mode.

**magnet current**

reads the magnets output current independent of the control mode.

Warning: There is a gotcha with this. If you type only magnet a value
will be returned. The meaning of this value is dependent on the selected
control mode. In current mode it is a current, in field mode it is a
magnetic field. This is so in order to support SICS control logic. You
can read values at all times explicitly using magnet current or magnet
field.

##### The Eurotherm Temperature Controller

At SANS there is a Eurotherm temperature controller for the sample
heater. This and probably other Eurotherm controllers can be configured
into SICS with the following command. The eurotherm needs to be
connected with a nullmodem adapter.

> evfactory new name euro computer port channel


name is a placeholder for the name of the device within SICS. A good
suggestion is temperature. euro is the keyword for selecting the
Eurotherm driver. port is the port number at which the serial port
server listens. channel is the RS-232 channel to which the controller
has been connected. **WARNING:** The eurotherm needs a RS-232 port with
an unusual configuration: 7bits, even parity, 1 stop bit. Currently only
the SANS port 13 is configured like this! Thus, an example for SANS and
the name temperature looks like:

    evfactory new temperature euro localhost 4000 13

There are two further gotchas with this thing:

-   The eurotherm needs to operate in the EI-bisynch protocoll mode.
    This has to be configured manually. For details see the manual
    coming with the machine.
-   The weird protocoll spoken by the Eurotherm requires very special
    control characters. Therefore the send functionality usually
    supported by a SICS environment controller could not be implemented.


##### The PSI-EL755 Magnet Controller

This is magnet controller developed by the electronics group at PSI. It
consists of a controller which interfaces to a couple of power supplies.
The magnets are then connected to the power supplies. The magnetic field
is not controlled directly but just the power output of the power
supply. Also the actual output of the power supply is NOT read back but
just the set value after ramping. This is a serious limitation because
the computer cannot recognize a faulty power supply or magnet. The EL755
is connected to SICS with the command:

> evfactory new name el755 localhost port channel index

with port and channel being the usual data items for describing the
location of the EL755-controller at the serial port server. index is
special and is the number of the power supply to which the magnet is
connected. An example:

    evfactory new maggi el755 localhost 4000 5 3

connects to power supply 3 at the EL755-controller connected to lnsa09
at channel 5. The magnet is then available in the system as maggi. No
special commands are supported for the EL755.

