### The Batch Buffer Manager

The batch buffer manager handles the execution of batch files. It can
execute batch files directly. Additionally, batch files can be added
into a queue for later processing. The batch buffer manager supports the
following command described below. Please note, that the examples assume
that the batch manager has been configured into SICS under the name of
exe.

    exe buffername

directly load the buffer stored in the file buffername and execute it.
The file is searched in the batch buffer search path.

    exe batchpath [newpath]

Without an argument, this command lists the directories which are
searched for batch files. With an argument, a new search path is set. It
is possible to specify multiple directories by separating them with
colons.

    exe syspath [newpath]

Without an argument, this command lists the system directories which are
searched for batch files. With an argument, a new system search path is
set. It is possible to specify multiple directories by separating them
with colons.

    exe info

prints the name of the currently executing batch buffer

    exe info stack

prints the stack of nested batch files (i.e. batch files calling each
other).

    exe info range [name]

Without an argument prints the range of code currently being executed.
With a parameter, prints the range of code executing in named buffer
within the stack of nested buffers. The reply looks like: number of
start character = number of end character = line number.

    exe info text [name]

Without an argument prints the code text currently being executed. With
a parameter, prints the range of code text executing in the named buffer
within the stack of nested buffers.

    exe enqueue buffername

Appends buffername to the queue of batch buffers to execute.

    exe clear

Clears the queue of batch buffers

    exe queue

Prints the content of the batch buffer queue.

    exe fullpath filename

Prints the full path name for filename if the file can be located
somewhere in exe paths. Else an error is printed. The purpose is to use
exe file management facilties in scripts.

    exe makepath filename

Prints the full path name for filename in the first direcory of batch
path. This is a tool to have scripts open files in the proper user
directory.

    exe run

Starts executing the batch buffers in the queue.

    exe print buffername

Prints the content of the batch buffer buffername to the screen.

    exe interest

Switches on automatic notification about starting batch files, executing
a new bit of code or for finishing a batch file. This is most useful for
SICS clients watching the progress of the experiment.

    exe upload

Prepares the batch manager for uploading a buffer to SICS

    exe append some text

Appends a line with everything after append to the upload buffer

    exe save filename

saves the recently uploaded buffer under filename on the SICS server.
Does not overwrite existing files.

    exe forcesave filename

saves the recently uploaded buffer under filename on the SICS server.
Overwrites existing file.

    exe clearupload

clears any pending upload operations.
