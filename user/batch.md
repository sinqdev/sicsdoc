Batch Processing in SICS
========================

Users rarely wish to stay close to the instrument all the time but
appreciate if the control computer runs the experiment for them while
they sleep. SICS supports two different ways of doing this:

-   SICS has a built in [macro programming](macro.htm) facility based on
    the popular scripting language Tcl. The most primitive usage of this
    facility is processing batch files.
-   Second there is the [LNS Rünbuffer](buffer.htm) system.
-   Third there is the new [Batch File](exeman.htm) execution system.

