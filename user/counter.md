SICS counter handling
=====================

* * * * *

A counter in SICS is a controller which operates single neutron counting
tubes and monitors. A counter can operate in one out of two modes:
counting until a timer has passed, for example: count for 20 seconds.
Counting in this context means that the noutrons coming in during these
20 seconds are summed together. This mode is called timer mode. In the
other mode, counting is continued until a specified neutron monitor has
reached a certain preset value. This mode is called Monitor mode. The
preset values in Monitor mode are usually very large. Therefore the
counter has an exponent data variable. Values given as preset are
effectively 10 to the power of this exponent. For instance if the preset
is 25 and the exponent is 6, then counting will be continued until the
monitor has reached 25 million. Note, that this scheme with the exponent
is only in operation in Monitor mode. Again, in SICS the counter is an
object which understands a set of commands:

-   **countername setpreset *val*** sets the counting preset to val.
-   **countername getpreset** prints the current preset value.
-   **countername preset *val*** With a parameter sets the preset,
    without inquires the preset value. This is a duplicate of getpreset
    and setpreset which has been provided for consistency with other
    commands.
-   **countername setexponent *val*** sets the exponent for the counting
    preset in monitor mode to val.
-   **countername getexponent** prints the current exponent used in
    monitor mode.
-   **countername setmode *val*** sets the counting mode to val.
    Possible values are Timer for timer mode operation and Monitor for
    waiting for a monitor to reach a certain value.
-   **countername getmode** prints the current mode.
-   **countername *mode val*** With a parameter sets the mode, without
    inquires the mode value. This is a duplicate of getmode and setmode
    which has been provided for consistency with other commands.
    Possible values for val are either monitor or timer.
-   **countername setexponent *val*** sets the exponent for the counting
    preset in monitor mode to val.
-   **countername getcounts** prints the counts gathered in the last
    run.
-   **countername getmonitor *n*** prints the counts gathered in the
    monitor number n in the last run.
-   **countername count *preset*** starts counting in the current mode
    and the given preset.
-   **countername status** prints a message containing the preset and
    the current monitor or time value. Can be used to monitor the
    progress of the counting operation.
-   **countername gettime** Retrieves the actual time the counter
    counted for. This excludes time where there was no beam or counting
    was paused.
-   **countername getthreshold *m*** retrieves the value of the
    threshold set for the monitor number m.
-   **countername setthreshold *m val*** sets the threshold for monitor
    m to val. WARNING: this also makes monitor m the active monitor for
    evaluating the threshold. Though the EL7373 counterbox does not
    allow to select the monitor to use as control monitor in monitor
    mode, it allows to choose the monitor used for pausing the count
    when the count rate is below the threshold (Who on earth designed
    this?)
-   **countername send *arg1 arg2 arg3 ...*** sends everything behind
    send to the counter controller and returns the reply of the counter
    box. The command set to use after send is the command set documented
    for the counter box elsewhere. Through this feature it is possible
    to diretclly configure certain variables of the counter controller
    from within SICS.

