# SICS User Documentation

## SICS Device Control

* [[Single Counters|user/Counter]]
* [[Histogram Memory|user/histogram]]
* [[Motors|user/motor]]
* [[Sample environment|user/samenv]]
* [[Velocity Selectors|user/velocity]]
* [[Choppers|user/chopper]]



## General SICS Commands

* [[General SICS Variables|user/genvar]]
* [[Driving|user/drive]]
* [[Counting|user/count]]
* [[Scanning|user/topscan]]
* [[Executing batch files|user/exeman]]
* [[Experiment Table processing|user/SicsTable]]
* [[Listing Things in SICS|user/sicslist]]
* [[SICS System Commands|user/system]]
* [[SINQ Online Status|user/sinq]]

## Instrument Specific SICS Commands

### AMOR

* [[AMOR Settings Module|user/AmorSet]]

### Four Circle Diffaction

This command set applies to TRICS, ORION and MORPHEUS when in four circle mode.

* [[Four Circle Command Set|user/TRICSCommandSet]]
* [[Four Circle Example|user/TRICSExample]]
* [[Four Circle Cheat Sheet|user/TRICSCheat]]

### SANS

* [[Common SANS Commands|user/saco]]
* [[Special SANS Commands|user/sanscom]]

### Triple Axis Spectrometers

This syntax is valid on CAMEA/RITA2 and EIGER. TASP is different.

* [[PSI TAS Syntax|user/NewTasSyntax]]
