### The Scan Command

* * * * *

An important concept in neutron scattering instrument control is a
"scan". For a simple scan a range of instrument positions is divided
into equidistant steps. The instrument then proceeds to drive to each of
these points and collects data at each of them.

There are two general commands which are of interest for all scans:

    scan mode [val]

prints or sets the scan counting mode. Val can be either monitor or timer. 

   scan getcounts

returns the counts collected in the scan. May be useful in scripts. 


#### Center Scan

Center scan is a convenience command which starts a scan around a
specified center value. This mostly used for centering purposes. The
syntax is like this:

    cscan *var center delta np preset*

All parameters must be specified. The parameters and their meanings:

-   **var** is the variable which is to be center scanned. Only one can
    be specified.
-   **center** is the value to use as center of the scan.
-   **delta** is the step width to use for the scan.
-   **np** is the number of points to scan in each direction.
-   **preset** is the preset to use for the counter. As the counter
    mode, the mode currently configured active in the scan object is
    used.

    example: cscan *d2hr 0.5 0.01 10 10000*

#### Simple Scan

Simple scan is a convenience command which starts a scan for one to
several variables with a simplified syntax. The syntax is like this:

    sscan *var1 start end var2 start end ... np preset*

All parameters must be specified. The parameters and their meanings:

-   **var1 start end** This is how the variables to scan are specified.
    For each variable scanned the name of the variable, the start value
    and the end value of the scan must be given. More then one triplet
    can be given in order to allow for several scan variables.
-   **np** is the number of points to scan.
-   **preset** is the preset to use for the counter. As the counter
    mode, the mode currently configured active in the scan object is
    used.

#### Fastscan

At some instruments there is a fastscan facility. This scan starts a
motor and the counter. As often as possible or configured, the scan
module will calculate the difference to the previous count, normalize it
to the monitor difference and print it. This is usually faster then
doing a step scan. There are limitations, though:

-   Fastscans are restricted to one motor. There is no way that a
    coordinated movement of several motors can be enforced by fastscan
    without mechanical coupling.
-   Due to the volatile nature of the beam at SINQ, this type of scans
    come into problems when the beam is off. A warning is printed,
    but...
-   If the motor is driving to fast, there might not be enough neutrons
    around even in the peak to be registered.

Basically, **if you see a feauture in fastscan, it is there, if not,
that does not mean anything!**. Fastscan comes into its own for
alignment scans or when searching peaks. The syntax is:

    fastscan motor start stop speed

Runs a fastscan on motor between start and stop. Speed is the speed with
which the motor is run. The motors speed is reset to the original value
when fastscan finishes.

    diffscan skip val

If fastscan produces to much output on to short intervalls, increasing
the skip parameter allows to control that. Skip is the number of SICS
cyles to skip between measurements. Thus this value is highly dependent
on the overall performance of SICS.

#### Peak And Center

These two commands are related to the scan command insofar as they act
upon the results of the last scan still in memory. The command **peak**
prints the position, FWHM and maximum value of the peak in the last
scan. The command **center** drives the first scan variable to the peak
center of the last scan. Both peak and center use a rather simple but
effective method for locating peaks. The prerequisite is that the peak
is approximatly gaussian shaped. The algorithm first locates the peak
maximum. Then it goes to the left and right of the maximum and tries to
find the points of half maximum peak height. The two points are
interpolated from the data and the peak position calculated as the
middle point between the two halfheight points.

