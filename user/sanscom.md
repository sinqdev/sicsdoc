### Special SANS commandos


This section describes some commands special to SANS. One feature of
SANS is that components are used as a whole rather than referring to
single motors. For SANS the beamstop, the detector and the sample table
are managed like this. Within a component each axis can be addressed
specificaly by using an axis = value pair. Axis defined for each
component will be listed below. Additionally these components support a
common commands as well. These mainly deal with named positions. A named
position is a set of values which can be driven to by just specifying
its name. For instance: **BeamStop PositionA** drives the BeamStop to
the position defined as PositionA. All component drive commands do not
block, i.e. you can type further commands. If it is needed to wait for a
component to arrive, use the [Success](drive.htm) command right after
your command.

#### Commands supported by all components


In the following the name of the component will be represented by COP.

   COP

The name of the component alone will yield a listing of the current
position of the component. This is also a way how to find out which axis
the component supports.

    COP back

drives the component back to the position before the last command.
Please note, that back does not operate on istself, i.e. two times COP
back will not do a trick.

    COP pos name

This command promotes the current position of the component to a named
position name. Afterwards this position can be reached by typing: COP
name.

    COP defpos name mot val mot val ....

this command defines a named position named name. This must be followed
by a list of motor alias names to run and their values.

    COP drop name

deletes the named position specified as second parameter.

    COP axis = val axis = val ........

drives the component to the new position specified by 1-n sets of axis
name = value pairs. The axis refer to the internal axis of the component
which are listed below or can be seen in a listing as created by typing
COP.

#### Specific components

This section lists the components which follow the component syntax
described above. Furthermore the axis available for each component are
listed. Each component has two names: a long one and a short one given
in parantheses.

**BeamStop (bs)**

The beam stop in front of the detector. Supports tow axis:

-   X, the horizontal movement.
-   Y, the vertical movement.

The beam stop has additional command assoicated with it:

-   bsout, drives the beamstop out of the beam.
-   bsin, drives the beam into position.
-   bschange, inquires the current beam stop.
-   bschange par, changes the beam stop to the specified number. Valid
    numbers are:
    -   1, no beam stop.
    -   2, small beam stop.
    -   3, medium beam stop.
    -   4, large beam stop.
-   bsfree, when changing the beam stop goes wrong the beam stop may be
    in a locked state. This command releases this lock. Drive with great
    care then!

**detector (dt)**

moves the detector around. The following axis are available:

-   X, the movement along the tank.
-   Y, a movement sideways.
-   phi, the rotation of the detector.

**sample (sa)**

the sample table. Support the axis:

-   Omega, sample table rotation.
-   X, translation X
-   Y, translation Y. \>LI\>Z, translation Z.
-   phi, cradle 1.
-   chi, cradle 2.

#### The Shutter

Even the shutter can be opnend from within SICS. Please note, that this
will only work if the LBC says it is OK, i.e. the enclosure is locked.

-   shutter, prints the shutter status.
-   shutter open, opens the shutter.
-   shutter close, closes the shutter.

#### Usage examples

    bs X = 12. Y 200

moves the beam stop to 12 horizontal position, Y vertical position.

    bsout

drives the beam stop to the previously defined position out.

    bsback

drives the beam stop to the position before the preceeding command.
