## Debugging SICS Server Startup

This page describes the procedure to debug SICS server startup problems. This is a fundamental 
skill. I use the placeholder inst again for the instrument name. It works as follows:

1. Log in to the instrument account
2. cd into the inst_sics directory, for example hrpt_sics
3. Stop SICS: monit stop sicsserver
4. Run SICS manually, thereby watching the messages: ./SICServer inst.tcl | more
5. Look on and watch

What to look for?

Error messages of course. When there is one, look into inst.tcl and find its cause and fix it.

Watch for places where startup seems to hang. By searching for the last seen startup messages 
in inst.tcl you should be able to figure out what SICS is tring to do when it hangs. Then check 
that piece of hardware. It also helps to be more patient then the typical scientist: may be then 
a timeout error message or a connection cannot be established pops up, giving you a clue what may be 
wrong. Sometimes, this takes minutes before it gives up on a connection. 

AscErr messages appearing saying that no connection can be established. Check what this is 
and verify that the hardware is on, connected and generally happy. At BOA, this can also 
mean that one of the RS-485 buses needs to be restarted. 

Proceed according to the instructions on page [Some Hardware does not work|SICSDevice]. 

When you are done, do not forget to restart the SICS server with monit!




 
