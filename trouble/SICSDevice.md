## Some Hardware does not Work

Obviously there are limits what can be done with software about this. Rather sooner then later help from 
the electronics group will be needed. But we can try to diagnose the problem and solve the stupid things.

### Symptom No connection to a Device

You find, during debugging startup or else that there is no communication with a device. There are 
some things which can be done about this.

### Solution

The first thing to ask is: is it actually configured? Many instrument have optional hardware. Such options are enabled  
by setting flags in the inst.tcl file. Thus, if the device is optional, check for the state of the applicable flag 
in inst.tcl.
 
If this is not the case you need to figure out which controller the device actually is associated with. I assume that 
you know the name of the non working device in SICS.

1. Login to the instrument account and cd into the inst_sics directory if you are not already there.
2. Open the inst.tcl file and search for the initialisation of the device by searching for the device name. 
  You will often find two: one for simulation and a real one. The simulation ones are those with the string Sim 
  in the ceation command. 
3. For counters and stuff alread the creation command  will contain a string of the form hostname:port. For example, 
  dmc-ts:3002. Take note of that string.
4. For motors the creation command often states a controller name and a number on the controller. Then try to locate the 
  creation command of the controller. Again take note of the host:port string.

Now, how to proceed with this information?

1. Return to the unix prompt on the instrument computer
2. Try pinging the host: ping hostname, for example ping dmc-ts
3. After a little while this should start printing lines of the form: 64 bytes from dmc-ts (129.129.92.62): icmp_seq=1 ttl=255 time=0.449 ms. This means that the host is OK.
4. If this does not come and after exiting ping with CTRL-C a message with 100% packet lost comes, then the host is not 
 switched on or not plugged into the network. Sort that one out, then. 

A common occurrence with histogram memories is that the VME crate holding the HM computer is down. Locate the 
VME crate and look at it: the symptom will be that the red large digit does not show 5 Volt as it should and or 
a LED indication a fan error or something like this is lit on the VME crate. Also all lights on the HM computer 
are off. Then ask Roman Buerge to fix the crate. 

With the old hardware, recognisable by the strings el737 or el734 occurring in the creation commands, there is 
another thing to be tried. 

1. From the unix prompt do: telnet hostname portnumber, for example telnet dmc-ts 3002
2. Then type ID 
3. If all is well, there will be a message containing EL734 motor controller, EL737 counter box or so
4. If the box is in local mode, the answer will be ?1. This can be sorted by giving the command RMT 1 multiple 
  times
5. If there is no response at all, the controller is dead or not connected to the terminal server. Sort this one out then.
6. If the controller is a  motor controller and the reponse is *ES, then the emergency stop is engaged. See below what to 
  do.



### Motor not working

The motor is connected to SICS (has sensible hardware limits) but malfunctions.

### Solutions

When a message containing the string emergency stop appears and the motor controller does not show positions 
any more but the string *ES, then the emergency stop is engaged. In the experimental area and sometimes in the 
cabins, there are emergency stop buttons for motors. These need to be released if they accidentally or purposefully have 
been activated. Releasing them means turning them a little. If you are sure that you have released ALL emergency 
stops and the condition persists, then something is wrong with buttons themselves and an electronics person is 
needed to fix it.

When the motor is a second generation motor and brings such message as: command not found and the controller 
shows a red display, then the motor had an error. If the electronics group is around, tell them. They __want__  
to be informed. If not, power cycling the motor controller might help. 

When there are a number of Restarting messages before the motor gives up then two  possibilities exist:

1. The difference reported in the Restarting message is small: then the precision parameter of the motor may 
  need adjustment. In SICS: motorname precision value. The value should be a little larger then the 
  difference reported in the Restarting message
2. When the differences are big then the motor cannot start for some mechanical of electrical reason. Check if there 
  is something in the path of the motor, and if so, remove it. Else call electronics. 

When there is a message saying: !!! MOTOR ALARM !!!, then the motor has mispositioned for 5 consecutive times. 
It then assumes that something is seriously wrong and stops the batch file. This could be a consequence of 
the Restarting message described above. If so proceed as described above. Otherwise this indicates a mechanical 
or electrical problem: call electronics. If you are really, really, really, really sure that the condition is harmless 
then you can issue the command in SICS: motorname ingnorefault 1. This will suppress this error. Do this only when 
you are 100% sure that your are not breaking things. 


Else there is a driver problem or softare problem and you need to debug. 

