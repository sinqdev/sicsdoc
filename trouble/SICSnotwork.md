## SICS does not work at all

### Symptom
SICS does not seem to work at all. Users cannot connect to a SICS server or the command line 
client six fails. 


### Check if the SICS Server is Running

Often this is caused by people not realizing that the client server nauture of SICS means that two 
programs need to run: the SICS server and a SICS client. The first thing to do then is to check 
if the SICS server is running at all. See the page [Running SICS](SICSRun.md).


### Check for Startup Problems

IF the SICS server is running and even restarting SICS does not solve the problem, then there may 
be a problem in the SICS server starting up. Proceed as described on the page 
[Debugging SICS Server Startup](SICSStartup.md). 


### Check Network

If all this is OK, then the client may be in the wrong network. Access to SICS servers is restricted to 
the PSI network. It will not work from the guest network. In which you may accidentally be connected too if 
you leave the WLAN of your notebook switched on. Or you accidentally connected your computer to the instrument network. 
This does not have DHCP server, so you will not get an IP-address. And without IP-address, no connection.

 
