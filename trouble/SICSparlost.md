## SICS Parameters Lost?

### Symptom

All the software limits, zero points and values of SICS parameters are lost in connection with 
a restart of SICS. 

### Solution

Apparently the SICS status backup file got corrupted. This can happen if you start SICS before 
all the hardware has been switched on. Or due to some other error. Keep calm, recovery is easy!

1. Consider the time when all the parameters were still good. 
2. Login to the instrument account
3. CD into the directory data/year/statusHistory. Repalce year by the current year, for example 2014.
4. In this directory there are daily status backup files with names like backupd-MM-DD.tcl Where MM is the 
 number of the months and DD the day. For example backupd-07-09.tcl is the file for July, 9. For the 
 last 24 hours there are 10 minute wise backup files with names like: backup-02h50.tcl for a file written 
 at 2:50. Choose a suitable file according to the date when all was fine.
5. Connect to SICS with a SICS client
6. In the SICS client type the command: exe /home/inst/data/2014/statusHistory/backupfile to load the 
 choosen backup file. For example, at dmc: exe /home/dmc/data/2014/statusHistory/backupd-07-09.tcl
7. From now on everything should be fine again. 

If this happens right at the startup of SINQ you may need to go back to the previous year 
to locate a good status backup file.


### Symptom

The hardware limits of some or all motors are 0. 

### Solution

This is another problem and is caused by the SICS server not being able to communicate with the 
motor controller. Proceed according to the instructions on the page [Debugging Startup problems](SICSStartup.md) and 
[Some Hardware does not work](SICSDevice.md). 



