# Trouble Shooting SICS

Here there is some information about how to solve problems with the SINQ data acquisition system SICS. 

## Basic Information

This section contains information on things which need to be understood before even trying to start troubleshooting SICS. Also fundamental techniques used for debugging SICS problems are described. 

* [Overview of how SICS is organized](SICSOrg.md) 
* [Neutron Data Acquisition](NeutronDAQ.md)
* [Interacting with the histogram memory WWW Interface](NeutronWWW.md)
* [Running SICS](SICSRun.md)
* [Log files and tracing](SICSlog.md)
* [Finding source code and manuals](SICSsource.md)
* [Debugging at the Instrument](SICSdebug.md)
* [Debugging SICS server startup](SICSStartup.md)
* [General trouble shooting hints](GeneralTrouble.md)
* [Avoiding Trouble with SICS](SICSAvoid.md)


## Recipes for SICS Trouble Shooting

* [SICS does not work at all](SICSnotwork.md)
* [SICS parameters lost](SICSparlost.md)
* [Counting does not work](SICScount.md)
* [Some hardware does not work!](SICSDevice.md)
* [Synchronisation to AFS fails](SyncFail.md)
* [No communication with sample environment](SampleFail.md)
* [SICS hanging or in funny state](SicsFunny.md)
