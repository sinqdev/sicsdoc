## SICS is Hanging

### Symptom

The SICS server does not seem to come back to the command prompt and hangs indefinitly.

### Solution

This is often caused by by a hardware device not giving a termination response to SICS. Or a infinite 
loop in your batch file. Or a real programming problem. Often, the  best way to clear the situation 
is to restart the SICS server. See the page on [running SICS](SICSRun.md) for instructions on how to do this. 
But it is worth to collect some more information to help with debugging.

To this purpose open and connect  a new SICS client.
One thing to check is the SICS task list:   In a SICS client type: task ps. A list of SICS tasks will be printed. 
There is a number of system tasks.  But when devices are running there will be a task per device and a devexec task. 
This can tell you the name of the device which has got stuck. Take note of the stuck devices name. Already 
this helps with debugging. Switch on [tracing](SICSlog.md) for a little while  in order to capture some of the 
communication with the offending device. Forward the trace and the name of the device to LDM computing stuff for 
further inspection.

You may stop tasks with: task kill taskname in a SICS client. Your progress may vary: if the hardware is 
in disorder this will only solve the problem until SICS tries to access that hardware again. Task kill 
is dangerous; you  may accidentally kill SICS system tasks and render SICS totally dysfunctional. If this hapens:
stay calm, a restart of the SICS server will resolve this problem.

