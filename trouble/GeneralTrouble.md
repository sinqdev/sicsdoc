## General Hints for Trouble Shooting SICS

One of the first things to do is to verify the report you get. Try to locate the problem 
in the automatic log files. Inspect the error messages. 
Can the problem be reproduced when you type the same commands as the user? Do not believe in 
any theories offered by the user, try to look at the symptoms yourself.

 
