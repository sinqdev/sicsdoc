## Counting does not Work

###  Symptom

No data is collected due to error messagfe while counting. Or no or empty data appears in the data 
file. 

### Solutions

Follow the instructions on page [Hardware does not work](SICSDevice.md) in order to figure out hardware problems 
with the counter box or histogram memory.

If this is OK and a histogram memory  is involved follow the instructions on page [Interacting with the 
histogram memory WWW interface](NeutronWWW.md). Only if there are events collected in the histogram memory and data 
does not arrive in data files a software problem is to be considered. 

 
