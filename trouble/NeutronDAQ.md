## Neutron Data Acquisition

### Simple Counters and Monitors

Single tube counters and monitors are handled through the EL737 counter box.  This seems simple 
but the EL737 counter box has some quircks. 

* Quite some confusion comes from the fact that meaningful cables from monitors and counters are 
 plugged into the counter box and thus mapped to an integer id in the counter box. These interger IDs are  
 directly mapped into SICS. Replugging at the counter box is easy. With the effect that it is never 
 clear which monitor is which in SICS.
* Counting on preset monitor works only on the monitor which is plugged into channel 1 of the counter box.    
* The EL737 counter box also detects the NoBeam condition through a monitor threshold. 

The EL737 counter box has in interesting output: this is the TTL trigger signal if data acquisition 
is active or not. 

### Area Detectors

In order to understand neutron DAQ with area detectors let us follow the path of a detected neutron through 
the system. 

1. The neutron causes a charge on one of the detector wires
2. After preamplification and some signal processing the neutron causes a pulse on some wire
3. An electronic box called the Multi Detector Interface (MDI) detects the pulse and encodes it into a 
   message to be sent through a fibre optic link. The neutron event message sent consists of:
   1. A message header identifying the message type
   2. The status of various sync bits. These are external inputs to the MDI. An important sync bit is the 
      input from the EL737 counter box if data acquisition is active or not.
   3. The position information of the detected neutron event. 
4. The fibre optic link transports the neutron event message to the histogram memory.
5. The histogram memory (HM) is indeed a computer. In the case of SINQ a MEN A12 VME PowerPC board running 
  realtime linux. This HM computer runs two main tasks: a server task is responsible for configuring the 
  HM and for communicating with then outside world which consists mostly of SICS. This server task is 
  actually a WWW-server. Then there is the histogramming task: this task continuously reads the fibre optic 
  link and analyizes the neutron event messages. It checks if the events are OK and accumulates them in 
  the appropriate bins according to the position information in an in memory representation of the detector.
6. SICS communicates with the HM, configures it, starts and stops DAQ and download and saves the data to file. 

Even with an area detector an EL737 single counter box is necessary. This box handles the monitors and 
through its TTL output triggers neutron data acquisition. 

Just another time, to make the triggering process crystal clear. When data acquisition becomes active, 
the EL737 counter box sets its TTL output. This is fed into the MDI and is represented is one of the sync 
bits of the neutron event message. The histogramming task tests both the event type and the presence 
of the sync bit against a mask. Only matching neutron events with appropriate sync bits are histogrammed. 
   
The question may arise why this is so important. Now, SINQ is an unreliable source compared to a reactor. 
The beam may go down for milliseconds to days. But when the beam is down, the detector and electronics 
continue to produce noise. You do not want this noise on your data. Thus DAQ needs to be interrupted by 
way of the threshold in the counter box and the sync bit. 


### Time-Of-Flight Neutron Data Acquisition

In time-of-flight (TOF) neutron data acquisition not only the positional information of a neutron event is 
measured but also the time it needed to cover a certain distance before it arrived at the detector. This is 
a measure for the energy (speed) of the neutron. In order to do this, a starting point in time is needed 
when all neutrons started to travel towards the detector. At pulsed neutron sources this starting point in 
time is provided by the source. At SINQ it is provided by the chopper. This is a fast rotating disk which 
cuts the neutron beam into pulses by way of slits in the disk. 

TOF neutron DAQ is very similar to area detector data acquisition:

1. After some suitable electronics preprocessing the neutron event arrives at the MDI in a position encoded 
  input channel.
2. The MDI maintains a counter which counts time. The time is the time since the last reset from the pulse 
  generator. At SINQ, this reset signal is delivered by the chopper.
3. The MDI encodes the header, sync bits, time stamp and position information in a neutron event message.
4. The neutron event message is forwarded via the fibre optic link to the HM
5. The HM histograms the neutron event according to position and time stamp. 

Besides the normal sync inputs there are additional ones. Most notably an input from the chopper which 
indicates if the chopper is running at the desired speed and phase. If any of this is not the case, then 
the neutron event is useless. 

Another complication comes from the fact that most choppers have two slits (for symmetry) but deliver only 
one signal per chopper revolution. The signal needs to be duplicated. This is the purpose of the Emmenegger 
electronics. This is a rather obscure bit of hardware. Mr Emmenegger has left PSI a long time ago and 
no one knows any details about it anymore. 

Yet another complication comes from the fact that the data width for the time stamp is only 20 bits. In order 
to increase the available time resolution a delay time is subtracted in the MDI. This delay time is the 
time which the fastest interesting neutrons need to travel from the chopper to the detector. The delay 
time is set in the MDI by way of a RS232 connection from SICS. Unfortunately the implementation of the 
RS232 interface is dubious. RS232 is connected via  a fibre optic link. Thus there is a fibre optic 
to RS232 converter. Some of those converters work, some not. One has to try out several ones when this 
link needs to be replaced until a converter is found which works. 

Summing it up, the MDI in the TOF case needs the following inputs:

1. The position encoded inputs from the detector
2. A time reset signal from the chopper via the Emmenegger electronics
3. A sync input from the EL737 counter box which indicates wether DAQ is active or not
4. A sync input from the chopper which tells if the chopper is OK
5. A RS232 input to set the delay time from SICS 
   
Time-of-flight brings with it yet another complication: the spectrum of the incoming neutron beam is not 
flat. I.e. for different neutron energies there are different source intensities. In order to arrive at 
absolute (or relative) intensities in reduced data the scientist has to normalize against the source 
spectrum. To this purpose the monitors are passed into the MDI too and histogrammed as well. How this is 
done differs from TOF instrument to TOF instrument: At AMOR this data is handled as a different detector 
bank, on others the monitors are appended to the real data as extra detectors.   



### CCD Cameras

CCD cameras are like digital cameras in that incoming photons increment a charge in a detector pixel. 
After exposure, the charge map of the detector is read out. CCD detect photons: neutrons are detected by 
looking at scintillator screens which essentially converts neutrons into light. This explanation serves 
to give the background for the fact that the electronic noise suppression which works for normal 
detectors at the  neutron event level does not work for CCDs. Thus one needs to rerun the 
exposure if SINQ goes down to long during an initial exposure. SICS checks for this and does the 
reexposure when necesary.

Now, commercial CCDs come from companies living in the dark ages IT wise. Which means that they 
usually get deliverd with some windows system to run them. The better ones come with a Linux API 
in a library. This is what we use. On top of the library a litte WWW-server, ccdwww, is run which 
allows SICS to expose images and download data, configure the camera and such. This WWW-server is run 
at a  separate computer. For a good reason, these CCD cameras require a special hardware connection. 
Which often fails. With a different computer the CCD camera computer can be restarted without 
having to take down the rest of the data acquisition system.      


### Specialities at different Instruments

#### AMOR

At AMOR, the Emmenegger electronics died. Fortunately, the data for a full revolution of the chopper 
could be fitted into the 20 bit time stamps. Thus there are actually two pulses in the neutron event data. 
This is corrected for by a special histogramming process on the HM computer for AMOR which merges 
the two pulses. This makes the HM configuration for AMOR a little complicated. 

#### POLDI

POLDI works with frame overlap. This means that the data from different neutron pulses overlap in the 
data. POLDI has a very complex chopper: the disk is split into four quadrants with each 8 slits. 
And there is only one reset signal from the chopper per revolution. Fortunately all data fits into the 
20 bit time stamp. The raw data from the POLDI HM contains data from all four quadrants after each other 
in time. SICS sums the four quadrants together again. 
 
  
#### HRPT

HRPT is the first instrument having the second generation data acquisition electronics. Namely the 
prototype. It replaces the MDI and front level electronics. This electronics has an own TCP/IP address 
and its own console. No documentation available, naturally. 


