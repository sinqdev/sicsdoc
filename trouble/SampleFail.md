## No communication with Sample Environment

### Symptom

You do not get connection to the sample environment. No connection to sea. 

### Solutions

Type connect_sea into a connected SICS client window. Most of the time that is all that is needed.

If this does not help, check if the SeaServer is running. See the page [Running SICS](SICSRun.md) for 
more information.

If all this fails, consult Markus Zolliker or Marek Bartkowiak for help.

 
