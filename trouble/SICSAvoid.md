## Avoid Trouble with SICS

The underlying assumption is that SICS is usally quite mature. Serious bugs are rare. 
There are some conditions when problems with SICS tend to occur:

1. When the software and hardware has changed after the startup of SINQ. Normally staff is available at this 
 time to sort things out.
2. When you do something new: new hardware, new routines etc. 
3. When the hardware breaks wile SINQ is running

There are some things you can do to minimze issues and have them resolved quickly:

1. Know your instrument well enough to be able to switch it on or off. A surpring number of problems have to do 
 with this. 
2. Be aware of the flags in the inst.tcl file which control optional hardware. 
3. If you plan larger changes to the hardware which might require new drivers consult with LDM computing early. 
4. At SINQ startup, check your instrument thoroughly: try as many options as feasible, run a known sample to 
 verify that everything is OK. Perhaps you write a batch file for this which you share with LDM computing?
5. Schedule additions to your instrument or trying out new features into times when computing support is available. 
6.  When there is trouble with SICS, do not only work around it: report it by email. Only then SICS can be 
 improved for everyone. 


Be aware of the fact that testing DAQ software is a problem: we have automatic tests for many features of 
SICS which can be tested offline. But some issues only occurr when running with real hardware and samples. 
And LDM computing does not even have a hardware setup for tests anymore. If we can have two weeks of beam 
time at SINQ startup to test with samples and everything we might be able to deliver a perfectly working SICS. 
As no one is willing to waist so much beamtime for software, the only alternative is what we do today: wait 
until the problem occurrs and try to fix it quickly. 

  



