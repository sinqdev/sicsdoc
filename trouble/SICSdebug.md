## Debugging SICS at the Instrument

When you are here, you wish to debug the SICS C-code at the instrument. Look at core files, 
run gdb and such. This works like this:

1. Login to the instrument account
2. cd into inst_sics
3. monit stop sicsserver, to stop the sicsserver
4. Get an AFS token with klog yourusername 
5. To look at core files: ../sicscommon/debsics core.xxxxx where xxx is the number of the core file
6. To debug SICS: ../sicscommon/debsics
7. Set your breakpoints or whatever
8. Within gdb run: run inst.tcl

The background is that debsics sets directory paths such that gdb can find source files. This currently 
points to /afs/psi.ch/project/sinqdev/mksics. This assumes that with your AFS user name you at least 
have read rights there.

If this does not suit you, make your own version of debsics using the supplied version as a template.


