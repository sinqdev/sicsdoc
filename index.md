# SICS Documentation Wiki

## Categories

* [User Documentation](User.md)
* [Documentation for the GTSE Graphical User Interface](gtse/GTSEOverview.md)
* [SICS Manager Documentation](manager/Manager.md)
* [Trouble Shooting SICS](trouble/SICSTrouble.md)

## Technical Notes

1. Loading this for the first time takes a little while; this is normal. It is fast ever afterwards.
2. This documentation is in markdown wiki syntax. Files live at  /afs/psi.ch/project/sinqdev/mksics/sicsdoc and can be edited by the knowledgable  few. But everyone can read the markdown files and can provide additions or updated versions to the maintainers. 
3. The mysterious maintainers of this documentation are Mark Koennecke, Uwe Filges, Emmanouela Rantsiou and Markus Zolliker.

