# SICS User Documentation

## General SICS Commands and Features

* [Connecting to SICS](user/connect.md)
* [General SICS Variables](user/genvar.md)
* [Driving](user/drive.md)
* [Counting](user/count.md)
* [Scanning](user/topscan.md)
* [Executing batch files](user/exeman.md)
* [Experiment Table processing](user/SicsTable.md)
* [Listing Things in SICS](user/sicslist.md)
* [SICS System Commands](user/system.md)
* [SINQ Online Status](user/sinq.md)
* [SICS Second Generation Objects](user/secgen.md)

## SICS Device Control

* [Single Counters](user/Counter.md)
* [Histogram Memory](user/histogram.md)
* [Motors](user/motor.md)
* [Sample environment](user/samenv.md)
* [Velocity Selectors](user/velocity.md)
* [Choppers](user/chopper.md)
* [Shutter](user/shutter.md)


# Instrument Specific SICS Commands

## AMOR

* [AMOR Settings Module](user/AmorSet.md)

## Four Circle Diffaction

This command set applies to TRICS, ORION and MORPHEUS when in four circle mode.

* [Four Circle Command Set](user/TRICSCommandSet.md)
* [Four Circle Example](user/TRICSExample.md)
* [Four Circle Cheat Sheet](user/TRICSCheat.md)

## SANS

* [Common SANS Commands](user/saco.md)
* [Special SANS Commands](user/sanscom.md)

### Triple Axis Spectrometers


* [PSI TAS Syntax](user/NewTasSyntax.md) for CAMELEA and EIGER
* [TASMAD Syntax](user/tasmad.md) for TASP


