The GumTree SE Tree Perspective
---------------------------------

This perspective is designed for direct interaction with the instrument.
Again the layout of this perspective:

![](gtsemain.png)

To the left hand side is a tree display of all the devices and commands
the instrument has to offer. Use normal mouse operations to view the
instrument at different levels of detail. The values in the tree are
automatically updated through the instrument control software. Double
clicking on nodes in the tree will open an editor in the right top side
of the perspective. There you may configure new values for parameters,
configure command parameters, scans etc. Once this is done to your
satisfaction, hit the button labelled "Make it so". Commands will be
built and sent to the instrument control software. Commands and feedback
from the instrument control system will be displayed in the lower right
console window. At any time, if you feel it necessary to stop the
instrument, hit the red interrupt button.

A caveat: GTSE automatically stops you from modifying parameters for
which you have no privilege. Such parameters are marked with a little
lock in the tree display and cannot be edited in editors. Changing
privilege can be accomplished through the dialog which opens from the
SICS/Authorize menu entry.
