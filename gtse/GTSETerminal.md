The Gumtree SE Terminal
----------------------------

More experienced users are happy to type commands to be executed in the
instrument control server themselves. To this purpose, there is a
terminal perspective.

![](gtseterm.png)

Terminals are a simple thing, so this interface is simple too. There is
a command entry line at the botoom and a log window for the
communication on top of it. Please note that hitting Ctrl-space in the
input line opens a command history from which you may select old
commands for editing.

The red large interrupt button at the bottom is for stopping the
instrument.
