Gumtree SE Graphics Perspective
-----------------------------------

The graphics perspective allows to view the progress of the instrument
during an experiment in a graphical way. This perspectives layout:

![](gtsegraph.png)

To the left is a list which allows to select the graphics to be shown.
The selected graphics is then shown in the right hand area, together
with controls which allow to modify the plot. Most graphics views
support zooming by dragging a rectangle with the mouse.

Please note that this perspective may take a little while to initialize
when first activated.

A special view is the quickview which shows selected instrument
parameters as text.

Graphics can be printed or saved as png files through menu entries in
the Mountaingum menu.
