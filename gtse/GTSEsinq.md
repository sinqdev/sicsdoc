Gumtree SE SINQ Status
-------------------------

** This perspective is defunct as of 2014. This is due to changes in the way how the 
accelartor data is distributed by another group within PSI**


This perspective allows to view the SINQ status. The perspective
corresponds to equivalent displays elsewhere. Please note that this
perspective needs some time to fully initialize and show both plots and
information.
