Gumtree SE Batch Editor
=======================

At the end of the day, users wish to create a batch file which holds
experiments to be performed during the night. To this purpose there is
the graphical batch file editor.

![](gtsebatch.png)

The underlying model of this batch editor is a playlist, a sequence, of
operations to perform.

To the left is a copy of the instrument parameter and command tree.
Clicking nodes in this tree cause a editor to be opened in the center
upper editor view and the command to be added to the batch queue at the
right. The command can be edited in the center. The changed value are
saved into the batch queue when the button commit has been activated. So
there is left to right work flow: select at the left, edit in the
middle, save to the right.

Already existing entries in the batch queue can be edited by clicking
them. The editor will be reopened and edited values are again saved to
the queue with the commit button.

Batch queue entries can be copied, cut and pasted using the buttons
provided. Moving batch queue entries can also be performed through drag
and drop. Please note that you need to hold down the CTRL key in order
to copy a batch queue entry by drag and drop.

At the bottom there are some buttons which allow to save bacth files to
the control system and to run them. These buttons are:

Load

load an existing bacth file from the control system

Save

save a batch queue to the instrument control system

Start

save and execute the current batch queue. While the queue is executing
the currently active command is highlighted in the batch queue. The
feedback from the control system is shown in the log window in the
middle, below the editor.

Interrupt

aborts the batch queue.
