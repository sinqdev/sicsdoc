Gumtree Swiss Edition Overview
==============================

Gumtree Swiss Edition (GTSE) is a flexible graphical user interface to
all SINQ instrumentation. It is very much based on ideas pioneered by
Gumtree from ANSTO, Australia.

The first thing you will see after a startup of GTSE is the main window:

![](gtsemain.png)

Before you can do anything with GTSE you will have to connect it to an
instrument. This is achieved through the SICS/Connect menu entry. Choose
you instrument, your user privilege and enter your password. A short
while later, GTSE will have initialized for your instrument and look as
in the image above.

GTSE is an eclipse rich client application (eclipse-RCP). One of the
nicer features of this platform is that you may grap the divider between
components, the sash, and drag it in order to modify the relative size
of the components against each other. Configure gtse to your liking with
this feature.

The eclipse-RCP framework supports the concept of perspectives. This
means that the application changes its layout when the user tries to
perform different tasks. Switching between the various tasks is
accomplished through the main tool bar below the menu row.

![](gtsetool.png)

GTSE has the following perspectives to offer:

**Tree**

a [tree view](GTSETree.md) of the instrument for direct interaction
with the instrument

**Graph**

a perspective which allows to view the online [graphics](GTSEGraph.md)
displays the instrument has to offer.

**Terminal**

a [terminal](GTSETerminal.md) for direct interaction with the
instrument.

**Batch**

a graphical [batch file](GTSEBatch.md) editor

**SINQ**

the status of the accelerator and of [SINQ](GTSEsinq.md).

Then there are various other buttons:

**New Window**

when checked each perspective is opened in a new window.

**The red cross** 

is the online help; you already found that.

**STOP**

a button to stop the instrument.
