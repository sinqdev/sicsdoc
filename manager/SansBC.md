### SANS Beamcenter Calculations

This is a small mocule for calculating the beam center on a SANS. This
consists of an internal module, \_sansbc, and the actual sansbc command.
The internal command is configured into SICS with:

    MakeSansBC name SansBC

This module understands:

    _sansbc stat hm-name

prints statistics on the data in the histogram memory hm-name. This is
the sum, the maximum value and the average.

    _sansbc cog hm-name contour

calculates the beam center as the center of gravity. hm-name is again
the name of the histogram mmeory. Only data points with an intensity
above the value contour will be considered.

    _sansbc coc hm-name contour

calculates the beam center as the center of the contour. hm-name is
again the name of the histogram mmeory. Only data points with an
intensity above the value contour will be considered. The difference to
COG is that with COG each data point is weighted according to their
intensity, with COC they are weighted equally.

On top of this module, there is a script **sansbc** which runs the
statistics and then returns the center of contour for half the maximum
intensity.
