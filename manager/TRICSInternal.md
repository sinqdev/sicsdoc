### SICS Internal Commands for Four Circle

This is set of commands which are implemented in C and make up SICS four
circle support together with the scripts described as user commands. A
normal user may not need to deal with these commands. But if you wish to
adapt or write own scripts, then this helps. All these modules are
initialized centrally by the MakeSingleX command in the initialization
file. The modules have the following names and purposes:

    singlex

the master module holding central flags, the cell and UB matrix

    hkl

responsible for calculating settings.

virtual motors h, k, l

to drive in reciprocal space. These use hkl for calculations.

    ubcalc

a UB matrix calculation module.

    reflections lists ref and messref

two reflection lists, one for UB matrix calculations, the other for
measurements.

    fmess

a module to support the measurement of a list of reflections and to
store data in the TRICS format.

    cone

which provides the virtual motor for cone scans.

The more detailed reference:

-   [The single crystal master module](SingleX.md)
-   [Reflection Lists](FRefList.md)
-   [Index and Angles Calculation](TRICSHkl.md)
-   [UB Calculation](SingleXUBCalc.md)
-   [Measurement module](SingleXMess.md)
-   [Cone module](ConeIntern.md)
-   [Simple Indexing module](SimpleIndex.md)

