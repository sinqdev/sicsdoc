### Overview of SICS Initialization

The command to start the SICServer is: **SICServer inifile**. So, what
happens at the initialization of the SICS server? First, internally, a
set of standard SICS commands is initialized, than a set of special
initialization commands. These are special commands which help to
configure the SICS server to match the actual hardware present on the
hall floor and to define the commands available later on. Following
this, the SICS server will read the initialization file specified on the
command line or servo.tcl as a default. Using the data supplied in this
file, the server will be configured. At this stage all special
initialization commands, all Tcl-mechanisms and all SICS commands
already initialized are available. After this is done, the server will
delete the initialisation commands from its internal command list (No
need to carry them around all the time). Now a status backup file will
be read. This file contains normal SICS statements which initialise
parameter values to the values they had before the last shutdown of the
server. Such a file is automatically written whenever a normal shutdown
of the server happens or variables change.

The SICS server configuration file is essentially a SICS macro language
file. This implies that all general SICS commands and Tcl mechanisms are
available. Additionally the configuration file (and only the
configuration file) may use special commands for the installation of:

-   SICS server options.
-   SICS variables.
-   Special commands.
-   Hardware

Actually the SICS servers configuration is rarely stored in one file but
in several files which are included by the main configuration file. In
general the following files are present:

    inst.tcl

Replace inst with the name of the instrument in lowercase. This is the
main initialization file. It should contain all the hardware
initialization.

    instcom.tcl

Again replace inst with name of the instrument in lowercase. This file
holds instrument specific commands defined in the Tcl macro language.
This file is automatically included by inst.tcl.

