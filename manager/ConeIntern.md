#### The Cone Module

If one has found a single reflection on a four circle diffractometer
then others can be found by looking on a cone around the first, center,
reflection if the lattice constants are known. Because then it is
possible to calculate both the opening angle of the cone and the
scattering vector length of a second reflection. SICS supports cone
scans by providing for a virtual motor, cone, to drive on such a cone.
There is a separate document available from the author which describes
the cone calculation in more detail.

The cone module does the actual cone calculation. Cone can be configured
with commands:

    cone center

prints the number of the reflection in ubcalc to use as a center.

    cone center num

sets the reflection in ubcalc to use a the center for driving on the
cone. Set to 0 to use the first reflection in ubcalc.

    cone target

prints the target reflection indices for the cone.

    cone target h k l

sets the target reflection indices.

    cone qscale

prints the current value of the scattering vector scale.

    cone qscale val

sets a value for the scaling factor for the scattering vector. Values
should be close to 1.0;
