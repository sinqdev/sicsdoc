#### Reflectionlist

The SICS reflection list module provides basic facilities for
maintaining a reflection list. It is supposed to be augmented with
further, instrument specific scripts. The reflection list is flexible in
such a way that both the values expected for indices and angles can be
changed. The idea is to use one reflection list module for all the
cases: four circle bisecting, four circle normal beam, triple axis etc.

##### Configuration

A reflection list is created through the command

    MakeRefList name

in the instrument initialisation file. The parameter specified is the
name of the reflection list. Which, in typical SICS manner, is also the
name of the reflection list command. For the purpose of the next
sections this name will be assumed to be **ref**.

As noted in the introduction, the reflection list can be configured for
different puposes. This happens by specifying the meaning of bothe
indices and angles through special variables. Variables follow the usual
SICS variable syntax. Both take a comma separated list with the header
infromation as a parameter. Thus:

    ref indexheader [i1,i2,i3,..]

configures the indices

    ref anglesheader [a1,a2,a3..]

configure the angles to expect.

The default configuration is for a four circle instrument in bisecting
mode.

##### Reflectionlist Commands

Commands below are given assuming four circle bisecting mode. If the
reflection list is configured in a different way then the indices and
the angles may be different.

    ref clear

clears all reflections

    ref addx h k l

Add a reflection with indices h,k,l to the list. Angles are left at
zero.

    ref setx id h k l

Set indices h,k,l for entry id.

    ref adda stt om chi phi

adds a reflection to the list with the angles specified. Indices will be
0.

    ref seta id stt om chi phi

Sets the angles for the reflection with the id id.

    ref addax h k l stt om chi phi

adds a reflection to the list all indices and angles.

    ref del id

delete the reflection with the ID id.

    ref print

print all reflections.

    ref names

prints the id's of all known reflections. This is useful for scripts.

    ref flag id [val]

queries or sets the flag for the reflection ID.

    ref show id

print all values for the reflection id. The list contains: id flag h k l
stt om chi phi
