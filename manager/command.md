### Command Initialisation

Besides the general SICS commands which are available for all
instruments, there exist instrument specific commands. Such instrument
specific objects are configured into the system through special commands
in the initialisation file. Such special commands are described here.

    MakeRuenBuffer

MakeRuenBuffer makes the RünBuffer system available.

    MakeDrive

MakeDrive craetes the drive command.

    MakeScanCommand name countername headfile recoverfil

MakeScanCommand initialises the SICS internal scan command. It will be
accessible as name in the system. The next parameter is the name of a
valid counter object to use for counting. The next parameter is the full
pathname of a header description file. This file describes the contents
of the header of the data file. The format of this file is described
below. The parameter recoverfil is the full pathname of a file to store
recover data. The internal scan command writes the state of the scan to
a binary file after each scan point. This allows for restarting of
aborted scans.

    MakePeakCenter scancommand

MakePeakCenter initialises the peak analysis commands peak and center.
The only parameter is the name of the internal scan command.

    Publish name access

The SICS server uses Tcl as its internal macro language. However, it was
felt that the whole Tcl command set should not be available to all users
from the command line without any protection. There are reasons for
this: careless use of Tcl may clog up memory, thereby grinding the
system to a halt. Invalid Tcl statements may cause the server to hang.
Last not least, Tcl contains commands to manipulate files and access the
operating system. This is a potential security problem when the server
is hacked. However, in order to make macro procedures available the
Publish command exists. It makes a Tcl command name available to SICS
users with the access code access. Valid values for access are:
Internal, Mugger, User and Spy.

    MakeMulti name

SANS uses a special syntax feature where several motors are grouped into
a component group. For example beamstop or detector. MakeMulti creates
such a group with the name name. Once such a group has been created, it
has to be configured. For this a few configuration commands are
available:

    name alias motname compname

This command makes motor motname available as component motor compname.
For example: **bs alias bsx x** makes motor bsx available as x in the
beamstop group. Then the bsx motor can be driven by the command **bx x =
12.**.

    name pos posname motname value motname value ....

The group command supports the notion of named positions. This means
that a special combination of angles can be accessed through a name.
This commands defines such a named position with the name posname.
posname is followed by pairs of motorname value which define the
position.

    name endconfig

Once a group has been completely defined the configuration process must
be ended with endconfig.

    MakeMono name M1 M2 M3 M4

This command creates a crystal monochromator object. Such a
monochromator object is necessary for the usage of the wavelength or
energy variables. The parameter name defines the name of the
monochromator object in the system. M1 and M2 are the names of the Theta
and two Theta motors respectively. M3 is an optional parameter defining
a motor for driving the horizontal curvature. M4 is an optional
parameter defining a motor for driving the vertical curvature of the
monochromator.

    TokenInit tokenpassword

This command initialises the token control management system with the
token command. The single parameter tokenpassword specifies the password
for the token force command.


    MakeOptimise name countername

This command installs the Peak Optimiser into the SICS server. The Peak
Optimiser is an object which can locate the maximum of a peak with
respect to several variables. The arguments are: name, the name under
which the Peak Optimiser can be accessed within SICS and countername,
which is the name of an already configured SICS counter box.


   MakeAmorStatus name scan hm

This creates a helper object for the reflectometer status display with
name name. This object performs some operations on behalf of the status
display for the reflectometer AMOR. The parameter scan denotes the name
of the scan object. The parameter hm the name of the histogram memory
object.


    MakeSANSWave name velo\_name

Installs a velocity selector wavelength variable into SICS. The
variable will have the name given as first parameter. Usually lambda is
a good idea. The second parameter, velo\_name, is the name of the
velocity selector which is controlled by this wavelength variable.

    MakeHklscan scan hkl

Installs a command named hklscan which allows scans in reciprocal space
expressed as Miller Indizes on a four circle diffractometer. scan must
be the name of a scan object as created by MakeScanCommand. hkl is the
name of a hkl calculation object as created by makeHKL.

    MakeXYTable myname

Creates a XYTable object with the name myname. This object can store a
list of x-y values.

    MakeNXScript [name]

Installs the NeXus dictionary scripting module. If no name is given, the
name will be nxscript.

    MakeSinq

Install the listener module for the accelerator divisions broadcast
messages. This creates a command sinq.


    MakeO2T nam OM 2TM

creates an omega 2Theta virtual motor with name nam for omega 2Theta
scans. OM defines an omega motor, 2TM a two theta motor.

    MakeDataNumber SicsDataNumber filename

This command makes a variable SicsDataNumber available which holds the
current sequential data file number. filename is the complete path to
the file were this data number is stored. This file should never, ever
be edited without good reason, i.e. resetting the sequential number to 0
at the beginning of a year.

    MakeMaximize counter

Installs a command max into SICS which implements a more efficient
algorithm for locating the maximum of a peak then scanning and peak or
center.

    MakeMaxDetector name

Installs name into SICS which implements a command for locating maxima
on a two dimensional histogram memory image.

    MakeLin2Ang name motor

Creates a virtual motor name which translates an input angle into a
translation along a tangent to the rotation axis. The distance of the
translation table can be configured as variable: name length once this
is established.

    MakeSWHPMotor realmotor switchscript mot1 mot2 mot3

Creates switched motors mot1, mot2 and mot3 for real motor realmotor.
For switching the script switchscript is used. This can be used when
several motors are operated through the same motor driver. This
implementation is not completely general now.


    MakeOscillator name motor

Installs a module name which oscillates motor between the software
limits of the motor. This is useful for suppressing preferred
orientation effects on powder diffractometers.name then supports the
commands: start, stop, status which are self explanatory.

#### The AntiCollision Module

In some cases motors have to be drive in a coordinated way. For
instance, at TRICS, the chi motor may need to move first before omega
can be driven in order to avoid collisions. Or at the ECB instruments,
only one of eight motors in a rack can be driven at any given time. The
anti collision module now allows to implement this. Anticollisions
pattern of operation: Each collaborating motor is registered with the
anti collision module. When trying to start such a motor, the anti
collider just takes a note where it shoud go. On the first status check,
a program is called which has to arrange the running of the motors into
a sequence. This sequence then is executed by the anti collision module.
The program which arranges the motors into a sequence is a configurable
parameter and usually implemented as a script in SICS own marco
language. In the SICS initialization file this requires the commands:

    AntiCollisionInstall

Creates the anitcollision module.

    anticollision register motorname

Registers motorname with the anti collision module.

    anticollision script scriptname

This command configures the script which is responsible for arranging
the sequence of operations.

The script configured into anticollision is called with pairs or motor
names and targets as parameters, Example:

    sans2rack mot1 target1 mot2 target2 .....

Within the anticollision script, the following command may be used in
order to define the sequence.

    anticollision clear

Clears the sequence list

    anticollision add level motor target

Add motor with target to level in the sequence.


#### Monochromators

A monochromator is represented in SICS through a monochromator object
which holds all the parameters associated with the monochromator and
virtual motors which drive wavelength or energy. The commands to
configure such a monochromator are:

    MakeMono name M1 M2 M3 M4

This command creates a crystal monochromator object. Such a
monochromator object is necessary for the usage of the wavelength or
energy variables. The parameter name defines the name of the
monochromator object in the system. M1 and M2 are the names of the Theta
and two Theta motors respectively. M3 is an optional parameter defining
a motor for driving the horizontal curvature. M4 is an optional
parameter defining a motor for driving the vertical curvature of the
monochromator.

    MakeWaveLength nam mono

creates a wavelength variable nam. The monochromator mono is used for
adjustment.

    MakeEnergy nam mono

creates a energy variable nam. The monochromator mono is used for
adjustment.



#### The Scan Command Header Description File

The SICS internal scan command allows to configure the contents of the
header of the ASCII scan data file through a template header file. This
section describes the contents of this file. This header description
file consists of normal text mixed with a few special keywords. The
normal test will be copied to output verbatim. The keywords indicate
that their place will be replaced by values at run time. Currently only
one keyword per line is supported. Keywords recognized are:

** !!DATE!! **

Will be replaced with the file creation date.

**!!VAR(name)!!**

Will be replaced with the value of the SICS variable name.

**!!DRIV(name)!!**

Will be replaced with the value drivable variable name. Drivable
variables are all motors and all variables which may be used in a drive
or run command.

**!!ZERO(name)!!**

Will be replaced with the value of the softzero point for motor name.

**!!FILE!!**

Will be replaced by the creation name of the file.

Please note that text behind such a keyword in the line will not be
copied to the output.
