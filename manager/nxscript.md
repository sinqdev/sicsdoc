### Scripting NeXus Files

This section describes the scripting interface to NeXus files, called
nxscript. Scripting the generation of NeXus files has the advantage that
it can be customised very quickly to special needs. Moreover it might
help to reduce the amount of instrument specific code required for an
instrument. This scripting interface uses the NeXus dictionary API for
the actual writing process. This has the following consequences:

-   The interface needs two filenames: the NeXus filename and the
    dictionary filename when opening files.
-   Writing commands have the general form: alias object. This means
    that object is written to the NeXus file using the specified alias.
-   Another property is that some writing commands write several bits of
    information in one go. In such cases the aliases for the additional
    bits are derived from the base alias by appending specific strings.
    Thus dictionary files need to have a special form for this scripting
    interface to work.
-   Nxscript also tries to figure out the dimensions of multidimensional
    datasets such as histogram memories by itself. In such cases the
    dimension attribute in the dictionary file must be omitted.
-   Nxscript assumes the following policy for data writing: irrespective
    of errors write everything you can. Thus this interface will
    complain bitterly and verbosely if something does not work, but
    never return an error.

#### Usage

Before this facility can be used nxscript has to be installed into the
SICServer from the instrument configuration file. This is done through
the command:

    MakeNXScript ?name?

This creates a NeXus scripting object. If the name is omitted, an object
named nxscript is created. If a name is given, this name is used for the
scripting object. Having scripting objects with different names is also
the only possibility to have more then one NeXus file writing operation
going at a given time.

In the following sections it is assumed that an object **nxscript** had
been configured into SICS.

#### File Opening and Closing

    nxscript create5 nexusFile dictFile

Creates a new NeXus file based on HDF-5 with the name nexusFile. The
dictionary file dictFile is used.

    nxscript create4 nexusFile dictFile

Creates a new NeXus file based on HDF-4 with the name nexusFile. The
dictionary file dictFile is used. Using HDF-4 files is deprecated. 

    nxscript createxml nexusFile dictFile

Creates a new NeXus file based on XML with the name nexusFile. The
dictionary file dictFile is used.

    nxscript reopen nexusFile dictFile

Reopens an existing NeXus with the name nexusFile for modification or
appending. The dictionary file dictFile is used.

    nxscript close

Closes the current NeXus file. This command MUST be given at the end of
each script in order to make sure that all data is written properly to
disk.

#### Writing Things

    nxscript puttext alias bla bla bla ....

Writes everything after alias as text data to the alias. The definition
string for the alias should not contain a dimension description, this is
automatically appended.

    nxscript putfloat alias value

Writes a single floating point value to alias alias.

    nxscript putint alias value

Writes a single integer value to alias alias.

    nxscript updatedictvar alias value

Updates the dictionary value alis to value.

    nscript putmot aliasName motorName

Writes the position of the motor motorName into the NeXus file as
described by aliasName. Theposition is a zero point corrected position.
If another alias aliasname\_null exists in the dictionary, the zero
point of the motor is also written to file.

    nxscript putcounter aliasBase counterName

Writes counter data to a NeXus file. Data is taken from the single
counter counterName. What is written depends on the aliases present in
the dictionary file:

aliasBase\_preset

The preset value.

aliasBase\_mode

The counter mode

aliasBase\_time

The actual time counted, without breaks due to insufficient beam.

aliasbase\_00 ... aliasBase\_09

The monitor readings for monitors 0 to 9. Please note that 00 would
denote the normal counting tube at a scanning type of experiment.

    nxscript puthdb path ?alias

Put a hipadaba path. The alias to put the data too is found in one of
two places: as the nxalias property on the node or as given on the
command line. The alias is expected to match the size and type of the
data. Please note that hipadaba stores all floats as double which is
NX\_FLOAT64 as number type.

    nxscript puthdboff path offset ?alias

Put a hipadaba path. The alias to put the data too is found in one of
two places: as the nxalias property on the node or as given on the
command line. The alias is expected to match the size and type of the
data. Please note that hipadaba stores all floats as double which is
NX\_FLOAT64 as number type. Writing data is started with the offset
specified from the start of the data. This is useful to split a
histogram memory area into separate detectors or whatever. The length of
the amount of data to copy is implied by the dimension of the SDS alias
points to.

    nxscript puthdbslab path start size

Put a hipdaba node as a slab. The node must have a property nxalias to
determine where to write to. Start and size are Tcl lists which give the
start point where to write and the size of the data to write. See
nxscript putslab for more information.

    nxscript puthm hmAlias hmName ?start? ?length? ?bank?

Writes data from the histogram memory hmName to a NeXus file using the
alias hmAlias. Nxscript automatically updates the dim0, dim1, ...,
timedim dictionary variables. Thus these can be used to define the
dimensions in the dictionary file. If the optional parameters start and
end are given, a subset of the data is written. It is the users
responsability that the values requested make sense to the histogram
memory. In the case of subset writing, the dimensions have to be
specified in the definition string belonging to the alias. Nxscript sets
a variable timedim in the dictionary though which contains the length of
a time binning if appropriate. This is a special feauture for writing
extra detectors at SANS and AMOR. Optionally, together with start and
length, a bank number can be given. This is a feauture to support the
rare case of having multiple banks in one histogram memory. If not give
bank defaults to 0.

    nxscript puttimebinning aliasName hmName

Writes the time binning at histogram memory hmName to file using the
alias aliasName. The length of the time binning data is automatically
appended to the definition string for the alias.

    nxscript putarray aliasName arrayName length

Writes the Tcl array arrayName to file using the aliasName. The
definiton string belonging to aliasName does not need to contain a -dim
argument as this is set by this routine. The parameter length is the
length of the array. Only rank 1 arrays are supported. The array is
considered a float array.

    nxscript putintarray aliasName arrayName length

The same as above, but the data is considered integer.

    nxsript putglobal attName bla bla bla

This writes an global attribute attName. Everything after attName is
concatenated to a string which then respresents the value of the
attribute.

    nxscript putsicsdata alias dataname

Writes the sicsdata array dataname to alias.

    nxscript putattribute alias name value

Add another text attribute to alias. The name of the attribute is name,
the value value.

    nxscript makelink targetAlias victimAlias

This creates a symbolic link for victimAlias in the group designated by
targetAlias.

    nxscript putslab alias startlist sizelist obj

Writes a slab of data. alias is the alis to write too. startslist is a
Tcl list containing the start indexes, sizelist is the size of the slab
to write as a Tcl list and obj is the SICS object to write. Obj can be a
histogram memory; then the histogram memory data is written. Or it can
be a sicsdata object, the value of which will then be written.

    nxscript putslab16 alias startlist sizelist obj

Writes a slab of data as above. Parameters are as setailed above. The
difference is that this converts the data to a6 bit int before writing.
And SICSData objects are not supported.

    nxscript puts alias sgetstring [dim,dim]

Writes any SICS data which can be fetched with sget. The input to sget
is the sgetstring. If sgetstring contains spaces, it must be quoted. The
code tries to guess types and dimensions automatically, thus alias must
point to a definition string without -type, -dim and -rank. The
dimensions can be overriden with the optional dim argument which is a
komma separated list of dimensions.

    nxscript sappend alias sgetstring point [dim,dim]

Appends any SICS data which can be fetched with sget to alias. The input
to sget is the sgetstring. If sgetstring contains spaces, it must be
quoted. The code tries to guess types and dimensions automatically, thus
alias must point to a definition string without -type, -dim and -rank.
It is assumed that the first dimension is NX\_UNLIMITED. The dimensions
can be overriden with the optional dim argument which is a komma
separated list of dimensions. This override only allows to specify the
correct dimensions of the detector, NX\_UNLIMITED is prepended
automatically. Point is the count in the dataset where to append to.
Usually this is the scan point number.

### Automatic Updating of NeXus Files

Some instruments perform measurements for quite long counting times. In
such cases it is advisable to save the data measured so far to file in
order to protect against hardware or software failures. To this purpose
an automatic file upgrade manager is provided. On installation the
automatic update object is connected wth a counting device through the
the callback interface. This makes sure that the update manager is
automatically notified when counting starts or finishes.

#### Prerequisites for Using the Automatic Update Manager

In order to use automatic updating, three programs must be provided.
Each of these programs can be a script which uses the nxscript facility.
It can also be a SICS command.

    startScript

This program is supposed to write the static part of the file. It is
called once when the file is created.

    updateScript

This program is supposed to create and update the variable data elements
in the NeXus file. This is called frequently.

    linkScript

This program is supposed to create the links within the NeXus file. This
is called once after startscript and updateScript have been run.

#### Installing Automatic Update

An automatic update object is installed into SICS with:

    updatefactory name countername

name is a placeholder for the name under which SICS knows the automatic
update object. name is available as a SICS command later on. countername
is a placeholder for a counter object (counter or HM) which triggers
automatic updating of NeXus files. This object has to support both the
countable and callback interfaces of SICS. Suitable SICS objects include
counter and histogram memory objects.

#### Configuring Automatic Update

The SICS command created with updatefactory (see above) supports a few
parameters which allow for the configuration of the whole process.
Parameters follow the normal SICS syntax. Futhermore there is a
subcommand list, which lists all configuration parameters. Supported
parameters are:

    startScript

The program supposed to write the static part of the file.

    updateScript

The program supposed to create and update the variable data elements in
the NeXus file.

    linkScript

This program supposed to create the links within the NeXus file.

    updateintervall

The time intervall in seconds between updates. The defualt is 1200, eg.
20 minutes.
