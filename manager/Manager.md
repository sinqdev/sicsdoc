# SICS Manager Documentation

## SICS Initialisation Commands

This is the documentation for the commands used to install hardware or  commands into the SICS server. Additional initialisation commands may live in the description of instrument specific commands and internal SICS commands.

* [SICS Initialisation overview](ini.md)
* [SICS server options and user configuration](option.md)
* [SICS global variables](var.md)
* [Old generation hardware initialisation](hwini.md)
* [Second generation hardware initialisation](secgenini.md)
* [Command initialisation](command.md)
* [Command aliasing](alias.md)
* [SICS help system](helpman.md)
* [The SICS McStas interface](mcstas.md)
* [SICS Logging](NewSICSLog.md)

## Instrument Specific Initialisations and Commands

* [FOCUS special initialisation and commands](focus.md)
* [Pfeiffer vacuum readout at FOCUS](Pfeiffer.md)
* [Old POLDI test rig](PZReiss.md)
* [SANS beam center calculation](SansBC.md)
* [Four circle internal commands](TRICSInternal.md)
* [Triple axis software](SicsTriple.md)
* [Triple axis initialisation commands](tas.md)
* [Internal Tasub module commands](tasub.md)
* [Triple Axis UB Matrix related commands](TasUBMatrix.md)

## Hipadaba and ScriptContext Documentation

* [Hipadaba Introduction, Initialisation and Commands](Hipadaba.md)
* [Hipadaba Property Use](HipadabaProp.md)
* [Background Information on how ScriptContext works](NewSciptContext.md)
* [ScriptContext Controller Documentation](ScriptConDoc.md)
* [ScriptContext Protocol Driver Documentation](ScriptConProt.md)
* [Scriptcontext driver general documentation](ScriptContextDriver.md)
* [SICS Objects and ScriptContext Driver Reference](ScriptDriverReference.md)

## Internal SICS Commands

This is the documentation for SICS internal command and features. These are items which the user is not normally supposed to see. But it is important for people implementing scripts, configure data file writing and such. 

* [SICS Cron or Reocurring Tasks](cron.md)
* [SICS data module (array processing)](SicsDataDoc.md)
* [Internal SICS scan module](iscan.md)
* [Writing SICS Macros and defining interfaces in Tcl](macroman.md)
* [Scripting NeXus file writing](nxscript.md)
* [The old peak optimiser](optimise.md)
* [XY Tables](xytable.md)
* [SICS Polling](SicsPoll.md)

