### SICS Object and ScriptContext Driver Reference  

SICS is based on objects. Objects can hold parameters and methods. First generation SICS objects were hard coded in ANSII-C. This led to 
a lot of repetetive code. Thus second generation SICS objects were invented. Once a second generation SICS object has been created, parameter and 
function nodes can be added to it. Object parameters and functions are essentially hipadaba nodes. A standard interpretation function then takes care of handling such objects: accessing parameters, running object functions etc. Most such SICS objects are implemented in ANSII-C. But it also possible to create and populate such objects from the server side scripting language Tcl. This is described in this section. 

By adding suitable node properties and registering SICS object nodes with a scriptcontext controller it is possible to access hardware from second generation SICS object and thus implement drivers. 

There is a general purpose second generation object and more specialised ones for different kind of hardware or functionality.  

#### General Purpose Second Generation Objects

Nodes can be added through  commands using the /sics/object syntax. Those nodes can then be configured with the appropriate properties and wired to a scriptcontext controller. This general purpose SICS object is used for secondary hardware such as monitor equipment for various parameters like vacuum and such. All those, where the more specialised second generation objects described below do not match. 

A general purpose SICS object is created through the command:

    MakeSICSObj objname objtype

objname is the name of the object in SICS, objtype is the SICS type name of objname in SICS. The type name does not matter much. 


##### Features common to all Second Generation Objects

A second generation object can be addressed through the special path /sics/objname. This then allows to add further nodes to the object through [hipadaba](Hipadaba.md) commands. 

All second generation objects support the list method:

    sgo list

will always return a list of parameters and methods supported by the object.

More methods can be added to second generation objects through the makescriptfunc function:

    sgo makescriptfunc name script priv 

will create a entry name to sgo which calls the script when invoked. All children of name are considered 
parameters to script and are used as a documentation to make sure that enough parameters of the right 
type are passed to script. 

The special parameter child named **args** concatenates all additional parameters passed and passes them as a text parameter to the script 
function. Then the script function can parse this string itself.  


How to connect obect nodes with hardware parameters can be learned in the section Node Processing of the [ScriptContext Controller Documentation](ScriptConDoc).  

#### General Purpose Drivable Object

Another general purpose second generation object is the drivable second generation object. This can be used anywhere where a motor is required. It should be used everywhere where the more specialised motor object does not apply but setting the hardware parameter takes some time. Such an object is configured into SICS with the command:

    makesctdriveobj objname numbertype accesscode sicstype sctcontroller

The parameters:

* **objname** is the name of the object in SICS
* **numbertype** is one of float, int or any of the general Hipadaba types.
* **accesscode** defines the privilege required to drive this device. Can be spy, user or manager.
* **sicstype** is the SICS object type. Is not very important.
* **sctcontroller** is the scriptcontext controller this drivabel object is to be configured too. 

In order for such an object to work properly some more scripts and properties have to be provided. 

* First of all read and write script chains have to be provided for objname. 
* A property named halt on the objname node has to be configured with the name of a script which can send a halt command for the device.
* A property named checkstatus on the objname node has to be configured with the name of a script which checks if the drivable has reached its position.  This script has to return one out of: idle, busy, posfault or error. 
* A property named checklimits on the objname node has to be configured with the name of a script which checks if the desired value to drive to is within the devices limits. 

An example of this can be found in the Tcl module stddrive.tcl. This Tcl module provides for a simple drivable object.  

Far more elaborate examples of the use of the general purpose second generation SICS objects can be found in the chopper drivers. 


#### A Second Generation Motor Driver

A second generation motor object is created with the command:

    MakeSecMotor motorname

which creates a SICS motor called motorname.     

A scriptcontext driver for a second generation motor has at minimum to
configure the following scripts:

1.  A read script on the hardposition
2.  A write script for the hardposition
3.  A read script for the status
4.  There is also a halt script

The calculation of physical values is performed by the second generation motor object. The driver scripts have only to concern themselves with the values to write or read from the hardware.

##### Responsabilities of a Motor Value Write Script

When the motor starts, the status value of the motor is set to run by
SICS. Then SetHipadabPar and thus the write script is run.

1.  It must send an appropriate command to the hardware.
2.  It must check the reply
3.  If sending the hardware command fails there are options:
    1.  If the failure is permanent, the the motor status code has to be
        set to error.
    2.  If the problem can be solved and a restart is required, then the
        motor status code has to be set to restart.

4.  When running the motor continues in one form or the other, then the
    motor status field has to be queued with progress priority at the
    appropriate scriptcontext controller. This is essential to start
    polling.

##### Responsabilities of a Motor Value Read Script

1.  It must send a suitable request to the hardware
2.  It must analyse the reply:
    1.  In the event of a succesfull read, the motors hardposition must
        be updated and the geterror property of hardposition must be
        cleared by either deleting it or setting it to none.
    2.  In the case of a problem, the geterror field of the motors
        hardposition must be set to a meaningful description of the
        problem.

3.  Normal, scheduled polling of the motor position will take care of
    redoing the command.

##### Responsabilities of a Motor Status Read Script

Once SICS has set a new motor value it will keep inspecting the motors
status field in order to detect the status of the motor. Depending on
the value of the status field it will act:

**run**

keep on polling

**idle**

check the position, reposition when necessary or terminate

**restart**

Restart the motor by setting the new position again. This is for fixing
of hardware problems where a restart can solve the problem.

**error**

There is an unsolvable harwdare problem. The motor terminates.

**poserror**

The motor failed to position. SICS will check the position and possibly
restart. Or terminate. Especially when the maximum number of restarts
has been reached.

There are two ways that the status gets updated: Either directly through
the motor status node or by updating the status value through
scriptcontext. Thus a scriptcontext status reader script chain has the
following responsabilities:

1.  It must clear its geterror property before each request
2.  It must send requests to the motor about what it is doing
3.  It must analyse the replies from the motor and update the motor
    status field appropriatly.
4.  When further polling is necessary, it must queue the motor status
    and the motor hardposition with progress priority at the controller.
    The latter in order to achieve position updates while the motor is
    running.
5.  When the motor is about to go idle, it has to ask for the motors
    position. It also has to analyse the reply to this request and
    update the motors hardposition. Otherwise the motor is not finished
    yet!
6.  When errors occurr there are a number of options:
    1.  If it is a fixable communication error, fix it and queue another
        request
    2.  If the motor reports a mispositioning set the status to
        poserror. Update the geterror property and the motor error with
        an appropriate description.
    3.  If the motor has big problems, set the status to error and
        update the geterror property and the motor error field with an
        appropriate description.

##### Responsabilities of the Halt Script

The halt script must queue an appropriate command to stop the motor with
halt priority at the controller. Nothing else, the status reading
scripts must pick up the actual stopping of the motor. It may take a
little while until the motor really stops.


* * * * *

#### Second Generation Counter Driver

A second generation counter object is installed into SICS with the command:

    MakeSecCounter name numCounters

where name is the name of the counter in SICS and numCounters is the number of counters and monitors attached to the thing.     

A second generation counter driver needs to supply the following morsels of code:

1.  Code for writing the control variable. This actually controls
    counting.
2.  Code for reading the status
3.  Code for reading the counter data into values

##### Writing the Control Variable

The counter is controlled by writing special values to the control
variable. The special values are:

**1000**

Start counting

**1001**

Stop counting

**1002**

Pause counting

**1003**

Continue paused counting

The main tasks of the code to write the control variable thus are:

1.  Depending on the target value passed, format a command to enact the
    operation for the counter and send it it to the counter
2.  For a scriptcontext driver: queue the counter status read action
    with progress priority

Before counting is started, the counters status field has been set to
run. If errors occur while sending commands to the counter then the
counters status value has to be set to error and the geterror property
of control should be set to a description of the error. If the driver
code can fix the problem, then it also can set the status code to
restart. Restart will cause the counter to be restarted.

##### Reading the Status

Reading the status comes down to polling the counter for completion of
counting. Thus the main tasks of the status reading code are:

1.  Clear the geterror property of counter status.
2.  Send a command to read the counter status to the counter
3.  Analyse the response and update the status accordingly.
4.  For the case of a scriptcontext driver: queue the status read action
    again with progress priority when further pollling is necessary.
5.  For the case of a scriptcontext driver: queue the value read action
    again with progress priority when further pollling is necessary.
    This causes updates of the counter values while counting is in
    progress.

For updating the status codes the following rules apply:

**run**

is the status code to set when counting continues

**idle**

When the counter is finished the counter status is to be set to idle.
However, when the counter reports idle, the status code first has to
update the counter values by sending appropriate commands and analysing
their responses before the status field can be set to idle.

**error**

When an unfixable error occurs while counting, a communication error or
a hardware error, then the status code has to be set to error. In this
case the geterror property of counter status has to be set to a
description of the error. When an error can be fixed by the driver there
are two options: a) the counter needs to be restarted. Then set the
status to restart. b)The counter kept running. Then just queue a new
status read request,

**paused**

When the counter is in paused state.

**nobeam**

When the counter reports an insufficient count rate.

This is another **important** note: when the counter reports it has finished, the data has to be transferred from the hardware into SICS. Before this is not done, counting shall not finish. Otherwise unexpected results may occur when writing data files. With script chains this means on end of counting the chain has to send a data reading command and finish only when this has completed. 

##### Reading Values

At preset intervalls or while counting the values of the counters will
be requested. This code thus has to:

1.  Clear the geterror property of values
2.  Send a command to the counter which requests the counter values
3.  Analyse the response of the counter and update the counter values.

When an error occurs while reading the counter values, two cases have to
be distinguished:

-   the driver code can fix the problem. Then just wait for the next
    poll or requeue the values field for repolling
-   The problem is not fixable. In this case the geterror property of
    values should be set to a description of the problem. The problem
    should also be reporte to the user through a call to clientlog.

* * * * *
#### Second Generation Histogram Memory

This is in many ways like a counter object as described above. The same rules for writing to the control variable and for reading status apply. A second generation HM is configured into SICS with the command:

    MakeSecHM name rank tof

Name is the name of the HM in SICS, rank is the number of dimensions and tof is a flag which, when set to 1, enables time-of-flight operation. 

