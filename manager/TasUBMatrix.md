### UB Matrix for Triple Axis Instruments 

`> CELL _a b c alpha beta gamma_`

set unit cell constants. If b or c are omitted, the same value as for a is used, if angles are omitted, they are set to 90 degress. Without arguments, the cell constants are listed. 

`> addauxref _qh qk ql_`

Define the vectors for the scattering plane. 

`> REF _qh qk ql a3 a4 sgl sgu Ei Ef_`

Make an entry in the list of peaks. For angles and k-values not given, the current motor positions are used. The peak number and the peak data are returned. 

`> REF AUX _qh qk ql_` Add an auxiliary reflection to the list of reflections. A4 is calculated from the cell constants, the current ei and ef values are used. A3 is left alone or is calculated to have the right angular difference to the previous reflection. Using auxiliary reflections is a help during setup of the instrument or in powder mode. 

The sequence: 
    
    
    ref clear all
    cell aa bb cc alpha beta gamma
    addauxref ax ay az
    addauxref bx by bz
    makeub 1 2
    

with ax, ay, az being the first plane vector and bx, by, bz being the second plane vector creates a UB matrix which allows to drive Q positions and qm. But be aware that a3, sgu and sgl values are still invalid. 

`> REF CLEAR _peak_`

Remove a peak. 

`> REF CLEAR ALL`

Remove all peaks. 

`> REF`

List all peaks. 

`> MAKEUB _peak1 peak2_`

Calculate, activate and print out the UB matrix, as well as the peak list with qhkl values calculated back. _peak1_ is used as the main peak, i.e. driving to the angles given for this peak will correspond to a QHKL which may only differ by a scalar factor of about one, if the cell constants are not correct. _peak2_ is used only for determining the scattering plane. 


`>MAKEUBFROMCELL`

Calculate and activate a UB which has been calculated from the cell constants alone. This is useful to get a4 when no reflection has yet been found to calculate a proper UB. 

`>MAKEAUXUB qh qk ql`

Calculate a UB matrix from the first reflection and an auxiliary second reflection specified through the miller indices qh , qk, ql. The auxiliary reflection will have the same angles as the first reflection, except a3 and a4 will be adjusted to match the requirements from the crystalls geometry. 


`> LISTUB`

List the UB matrix, the cell constants and all stored reflections. 

### Out Of Plane Operation

This formalism allows to drive out of the scattering plane using the tilt motors of the sample stage. Some cryostats cannot stand this. Therefore driving out of plane can be switched off and on. 

`> TASUB OUTOFPLANE 1`

This command switches out of plane operation **on**. 

`> TASUB OUTOFPLANE 0`

This command switches out of plane operation **off**. 

`> TASUB OUTOFPLANE`

lists the current value of the outofplane variable 

**Example session:**   
(Some numbers might not be correct, as this module does not yet work in this form). 
    
    
    We enter cell values
    
       > CELL 2.88 2.88 2.88 90 90 90
    
       > MAKEUBFROMCELL
    
    MAKEUBFROMCELL builds a UB matrix from the cell only. With this you can drive to 2 0 0
    
       > drive qh 2 0 0
    
    This will get you the correct two-theta == a4. You have to search the peak in a3, sgu and sgl and optimize it.
    
    Once we are at the maximum:
    
       > REF 2 0 0
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       1    2.00   0.00   0.00   65.31  -87.21    1.25   -5.22
    
       > makeauxub 0 4 4
       > dr qh 0 4 4
    Make an axiliary UB and drive to the theoretical position for 0 4 4
    
    Find the peak 0 4 4 and optimize it.
    
       ...
    
    Once we are at its maximum:
    
       > REF 0 4 4
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       1    0.00   4.00   4.00   65.31  -87.21    1.25   -5.22
    
    Show again the peak list:
    
       > REF
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       1    2.00   0.00   0.00   65.31  -87.21    1.25   -5.22
       2    0.00   4.00   4.00   99.45   20.39    4.25   -1.22
    
    Calculate the UB matrix, with 2 0 0 as primary peak and 0 4 4 as
    secondary peak.
    
       > MAKEUB 1 2
       UB   2.456   0.530  -0.200
           -0.530   2.456  -0.090
            0.200   0.090   2.456
       Peak    QH     QK     QL      A4      A3     SGL     SGU     QH     QK     QL
       1 A   2.00   0.00   0.00   65.31  -87.21    1.25   -5.22   2.03   0.00   0.00
       2 B   0.00   4.00   4.00   99.45   20.39    4.25   -1.22   0.02   3.95   4.03
    
    the last 3 columns above show the Q values calculated back from the angles.
    We drive now to the 2 0 0 peak
    
       > DR QH 2 0 0
       > PR A4 A3 SGL SGU
       A4 =  64.218
       A3 = -87.515
       SGL =  2.258
       SGU = -3.217
    
    The value of A4 has changed, because the cell constants do not match the values
    given for the first peak. The value of A3 has changed by the same reason, and
    in addition, because the the plane given by 2 0 0 and 0 4 4 is now tilted back
    into in the scattering plane. For the latter reason SGU and SGL have changed also.
    
       > DR QH 3 3 3
       > PR A4 A3 SGL SGU
       A4 =  82.516
       A3 = -31.215
       SGL =  2.258
       SGU = -3.217
    
    note that the values for SGU and SGU have not changed. The will not change
    as long as we stay in the scattering plane (no drives to Q3) and as long
    as the UB matrix has not changhed.
    
       > REF 3 3 3
       Peak   QH     QK     QL      A4      A3     SGL     SGU
       3    3.00   3.00   3.00   82.32  -31.25    2.26   -3.22
       > MAKEUB C B
       UB   2.456   0.530  -0.200
           -0.530   2.456  -0.090
            0.200   0.090   2.456
       Peak    QH     QK     QL      A4      A3     SGL     SGU     QH     QK     QL
       1     2.00   0.00   0.00   65.31  -87.21    1.25   -5.22   2.03   0.00   0.00
       2 B   0.00   4.00   4.00   99.45   20.39    4.25   -1.22   0.02   3.95   4.03
       3 A   3.00   3.00   3.00   82.32  -31.25    2.26   -3.22   3.00   3.00   3.00
    
    We have calculated a new UB matrix, based on the peak 3 3 3 as primary and the
    0 4 4 as secondary peak. The UB matrix has not changed, as we were already exactly
    at 3 3 3.
    

  

* * *
