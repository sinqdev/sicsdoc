### The Internal Scan Commands

#### Scan Concepts

Scans in SICS involve an internal scan module and a lot of scripts which
wrap the internal scan module into a syntax liked by the users.

The internal scan module in SICS evolved a little over time. It turned
out that scans are a demanding job for a programmer because of the
plethora of special scans people wish to perform and the many data file
formats which have to be supported. This requires a very high degree of
configurability. Under several refactorings the internal scan command
has grown to become:

-   A controller for the scan process.
-   A container to store scanned variables and counts during the process
    of a scan. This includes commands to store and retrieve such values.
-   A container for the configuration of the scan. A scan is configured
    by specifying functions to be called at the various steps during the
    scan. These are preconfigured to the standard scan functions. An API
    is provided to replace some or all of the scan functions by user
    defined ones.

The internal scan object is augmented by a library of standard scan
functions. The transition to the new model is not yet clean in order not
to break to much old code.

The standard scan command can be configured into SICS using the command:

    MakeScanCommand name countername headfile recoverfil

MakeScanCommand initialises the SICS internal scan command. It will be
accessible as name in the system. The next parameter is the name of a
valid counter object to use for counting. The next parameter is the full
pathname of a header description file. This file describes the contents
of the header of the data file. The format of this file is described
below. The parameter recoverfil is the full pathname of a file to store
recover data. The internal scan command writes the state of the scan to
a file after each scan point. This allows for restarting of aborted
scans.

The scan object (named here xxscan, but may have another name)
understands the following commands:

    xxscan clear

clears the list of scan variables. Must be called before each scan with
different parameters.

    xxscan add name start step

This command adds the variable specified by the argument name to the
list of variables scanned in the next scan. The arguments start and step
define the starting point and the sptep width for the scan on this
variable.

    xxxscan log var

This command adds a variable to be logged during the scan. Can be slave
motors such as stt, om, chi, phi during four circle work. These
variables are not driven, just logged. var is the SICS variable to log.
Only drivable parameters may be logged in such a way.

    xxscan run NP mode preset

Executes a scan. The arguments are: NP the number of scan points, mode
the counter mode to use (this can be either timer or monitor) and preset
which is the preset value for the counter. Scan data is written to an
output file.

    xxscan continue NP mode preset

Continues an interrupted scan. Used by the recovery feauture.

    xxscan silent NP mode preset

Executes a scan. The arguments are: NP the number of scan points, mode
the counter mode to use (this can be either timer or monitor) and preset
which is the preset value for the counter. The difference to run is,
that this scan does not produce an output file.

    xxscan recover

Recovers an aborted scan. The scan object writes a file with all data
necessary to continue the scan after each scan point. If for some reason
a scan has been aborted due to user intervention or a system failure,
this scheme allows to continue the scan when everything is alright
again. This works only if the scan has been started with run, not with
silent.

    xxscan getfile

This command returns the name of the current data file.

    xxscan setchannel n

Sometimes it is required to scan not the counter but a monitor. This
command sets the channel to collect data from. The argument n is an
integer ID for the channel to use.

    xxscan getcounts

Retrieves the counts collected during the scan.

    xxscan getmonitor i

Prints the monitor values collected during the scan for the monitor
number i

    xxscan gettime

Prints the counting times for the scan points in the current scan.

    xxscan np

Prints the number of points in the current scan.

    xxscan getvardata n

This command retrieves the values of a scan variable during the scan
(the x axis). The argument n is the ID of the scan variable to retrieve
data for. ID is 0 for the first scan variable added, 1 for the second
etc.

    xxscan noscanvar

Prints the number of scan variables

    xxscan getvarpar i

Prints the name , start and step of the scan variable number i

    xxscan interest

A SICS client can be automatically notified about scan progress. This is
switched on with this command. Three types of messages are sent: A
string NewScan on start of the scan, a string ScanEnd after the scan has
finished and a string scan.Counts = {109292 8377 ...} with the scan
values after each finished scan point.

    xxscan uuinterest

As above but the array of counts is transferred in UU encoded format.

    xxscan dyninterest

As above but scan points are printed one by one as a list containing:
point number first\_scan\_var\_pos counts.

    xxscan uninterest

Uninterest switches automatic notification about scan progress off.

    xxscan integrate

Calculates the integrated intensity of the peak and the variance of the
intensity for the last scan. Returns either an error, when insufficient
scan data is available or a pair of numbers. Peak integration is
performed along the method described by Grant and Gabe in J. Appl.
Cryst. (1978), 11, 114-120.

    xxscan window [newval]

Peak Integration uses a window in order to determine if it is still in
the peak or in background. This command allows to request the size of
this window (without argument) or set it (with argument giving the new
window size).

    xxscan simscan pos FWHM height

This is a debugging command. It simulates scan data with a hundred
points between an x axis ranging from 10 to 20. A gauss peak is produced
from the arguments given: pos denotes the position of the peak maximum,
FWHM is the full width at half maximum for the peak and height is its
height.

    xxscan command tclcommand

Sets the tcl command procedure to invoke at each scan point. See below
for the description of user defined scans (Old Style). Invoked without
argument command returns the name of the current command procedure.

    xxscan configure mode

Configures the several possible scan modes for the scan object.
Currently there are two:

-   **standard**, the default mode writing ASCII files.
-   **amor**, a special mode the reflectometer AMOR which writes NeXus
    files.
-   **script** Scan functions are overriden by the user.
-   **soft** The scan stores and saves software zero point corrected
    motor positions. The standard is to save the hardware positions as
    read from the motor controller.
-   **user** configures the old style user overridable scans.

    xxscan storecounts counts time mon1 mon2 ...

This stores an entry of count values into the scan data structure. To be
used from user defined scan functions. The scan pointer is incremented
by one.

    xxscan storecounter

Store the counts and monitors in the counter object configured for the
scan into the scan data structure. Increments the scan pointer by one.

    xxscan appendvarpos i pos

Append pos to the array of positions for scan variable i. To be used
from user defined scan functions.

    xxscan callback scanstart | scanpoint | scanend

Triggers callbacks configured on the scan object. May be used by user
functions implementing own scan loops.

    xxscan function list

Lists the available configurable function names. The calling style of
these functions is described in the next section about stdscan.

    xxscan function functionname

Returns the currently configured function for functionname.

    xxscan function functionname newfunctionname

Sets a new function to be called for the function functionname in the
scan.

#### User Definable Scan Functions

The last commands in the last section allowed to overload the functions
implementing various operations during the scan with user defined
methods. This section is the reference for these functions. The
following operations during a scan be configured:

    writeheader

Is supposed to write the header of the data file

    prepare

Prepare does all the necessary operations necessary before a scan
starts.

    drive

Is called to drive to the next scan point

    count

Is called at each scan point to perform the counting operation

    collect

Is called for each scan point. This function is supposed to store the
scan data into the scan data structure.

    writepoint

Is called for each scan point and is meant to print information about
the scan point to the data ile and to the user.

    finish

Is called after the scan finishes and may be used to dump a data file or
perform other clean up operations after a scan.

    userdata

This is the name of a user defined object which may be used to store
user data for the scan.

The exact invocations of the functions:

-   writeheader scanobjectname userobjectname
-   prepare scanobjectname userobjectname
-   drive scanobjectname userobjectname point
-   count scanobjectname userobjectname point mode preset
-   collect scanobjectname userobjectname point
-   writepoint scanobjectname userobjectname point
-   finish scanobjectname userobjname

scanobjectname is the name of the scan object invoking the function.
This can be used for querying the scan object. userobjectname is the
name of a entity as specified as userdata in the configuration. point is
the number of the current scan point.


#### The Scan Command Header Description File

As if all this configurability is not enough, there is another level of
configurability. The SICS internal scan command allows to configure the
contents of the header of the ASCII scan data file through a template
header file. This is only possible when the scan functions are left in
their default configuration. If scan functions are overloaded it is the
users repsonsability to take care of data file writing. This section
describes the contents of the template file. This header description
file consists of normal text mixed with a few special keywords. The
normal test will be copied to output verbatim. The keywords indicate
that their place will be replaced by values at run time. Currently only
one keyword per line is supported. Keywords recognized are:

!!DATE!!

Will be replaced with the file creation date.

!!VAR(name)!!

Will be replaced with the value of the SICS variable name.

!!DRIV(name)!!

Will be replaced with the value drivable variable name. Drivable
variables are all motors and all variables which may be used in a drive
or run command.

!!ZERO(name)!!

Will be replaced with the value of the softzero point for motor name.

!!FILE!!

Will be replaced by the creation name of the file.

Please note that text behind such a keyword in the line will not be
copied to the output.

#### Differential Scans

When aligning or when searching peaks a very fast scan is required. This
is the differential scan. It starts a motor and collects counts while
the motor is running. The counts collected are the monitor normalized
difference to the previous reading. This functionality can be configured
into SICS with the command:

    MakeDiffScan 

in the configuration file. An optional parameter defines another name
then diffscan (the default) for this object. Differential scans can only
be run against one motor as it cannot be guaranteed that motors involved
in a coordinated movement operate at the same speed without mechanical
coupling. The procedure to use diffscan is:

-   Configure a scan variable into a SICS scan object: xxscan add var
    start step
-   Run diffscan as: diffscan scanobjectname end\_position\_of\_scan
    This runs the differential scan. Scanobjectname is the name of a
    SICS internal scan object. It will be used to store the results of
    the scan. While the scan is running some basic information is
    printed. The scan will range from the start given in the xxscan add
    command to the end position given in this call.

The diffscan object has two configurable parameters:

monitor

The monitor number to normalize against. For maximum precision this
should be a monitor with a lot of intensity on it.

skip

The number of SICS main loop cycles to skip between readings. This can
be used to control the amount of data generated during a differential
scan. This may become a problem if there is fast hardware.

A word of warning: positions found in differential scans may not be
totally correct. The differential scan might even miss peaks when the
relationship between motor speed and sampling rate is bad.

Diffscan is usally wrapped in a common script command:

    fastscan motor start stop speed

which does a fast scan for motor from start to stop. Before the scan the
motors speed is set to speed. The motor is set to its original speed
after termination of the scan.

This script can be copied from one of the older instrument command
files.

#### Peak Analysis

There are several other feautures which can be configured into SICS
which interact very closely with the scan module:

    MakePeakCenter scancommand

MakePeakCenter initialises the peak analysis commands peak and center.
The only parameter is the name of the internal scan command.

#### Common Scan Scripts

There exists a library of script functions around the scan module which
are commonly used. They provide an object oriented wrapper around the
internal scan command and the **cscan** and **sscan** commands. These
commands can be made available by including the scancommand.tcl file
into the instruments configuration file.
