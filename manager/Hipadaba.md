### The SICS Hierarchical Parameter Database

The new SICS hierarchical parameter database aims at addressing a couple of shortcomings of the old parameter handling code and implements:

-   Uniform access method for parameters. This includes not only setting and getting but also generalized automatic notifications on changes.
-   Concentrating all parameter setting/getting code in one place
-   The hierachical parameter database is also the base for a additional feautures such as fine grained logging of parameter changes. We need this for muSR.
-   Facilitate the sharing of parameters between modules.
-   help to lift the somewhat artificial distinction between object and driver parameters in SICS.
-   Hipadaba would alo facilitate the synchronisation of a Java tree in a client.

The Hipadaba is a hierarchy of nodes. Nodes can be parameters or even commands. For each node there are also properties for holding meta data associated with nodes.


Please note that second generation SICS objects also answer to Hipadaba commands. Because the use the 
same underlying node implementation. SICS oject nodes can be accessed by Hipadaba commands through the special path:

    /sics/obj/nodename

Where obj is a placeholder for the SICS object name and nodename of course the name of the object node to 
be accessed.

* * * * *

##### Hipadaba Configuration Commands

    hattach path obj name

This command can attach certain SICS objects as children to path under the name name. Currently this works for motors, HM data, drivable objects and SICS Variables.

    hattach path counter monitorno

attaches monitor monitorno from counter counter to path. Monitorno can be -1 for count time, 0 for the actual counts of a single counter and \> 0 for monitors. There is a limitation: this works only for one counter object not for the rare case that there are several.

    hattach rootpath sicsdata type name

Attaches a sicsdata object to path as name. The associated callback executes scripts stored in the node property readscript before retrieving the data and scripts stored in the property writescript after setting the SICSData. As SICSData objects are usaully used as a form of array during computations, this makes sense. Type must be either floatvar ar or intvarar, depending on the intended type of the SICSData. For an example see SANSLI detector data which is calculated from the raw data.

    hattach target drivablename nodename

Update the node specified under nodename whenever a new target is set for drivablename. The node must be have been created in advance, with internal privilege. This implementation was choosen in order to allow for possibly multiple aliases to be attached to one node.

    hdel path

delete a node starting at path recursively.

    hfactory path plain privilege type [length]

create a normal node with the specified type and access privilege as path. The privilege to access this node is privilege. Allowed values are: **spy**, **user**, **manager**, **internal**. The datatype is set to type. Allowed keywords for type are: **int**, **float**, **intar**, **floatar**, **intvarar**, **floatvarar**, **text** and **none**. If one of the array datatypes is selected an arraylength must be given.

    hfactory path script getscript setscript type [length]

creates a parameter node which calls getscript for getting values and setscript for setting values. The new node is path, the type type. Arrays types require the length argument. The setscript has the signature *setscript text*. With text being the new value. The getscript has no arguments but is required to return a text containg the value. SICS answers of the type something = anything will be treated correctly. To a node generated as script node, the **property priv** must be set to a desirable value in order to use it.

    hfactory path link sicsobj

links the object sicsobj onto path. Sicsobj must be a SICS object which uses hipadaba mechanisms for parameters and commands.

    hfactory path command commandscript

Installs a command node onto path. Children of this node are considered parameters. When this node is set the specified commandscript will be concatenated with the values of the parameters and passed to SICS for execution.

    hfactory path alias targetnode

Install an alias node pointing to another node specified as targetnode. This is similar to a link in a file system. All set/get operations on the aliasnode will be forwarded to the targetnode. All updates of the target node will update the aliasnode too. Properties of the targetnode have to be configured manually.

    hsubsamplehm hm node command

stores the result of subsampling hm with the command given in node. Command is histogram memory dependent. Hm is usually the histogram memory name. The form hm:bank is also supported in order to specify the histogram memory bank. Node is the path to the node to update. Node must be of type HIPINTVARAR.

    hchain targetpath sourcepath

Chain the updating of two path components. If sourcepath is updated, so is targetpath.

    haddcheck node values

Add a callback to node node which checks that only such data is accepted as given in the values property. Which is a comma separated list of valid entry strings.

* * * * *

##### Hipadaba Node Properties

    hsetprop path key value

sets a property on node path with key key and the value resulting from a concatenation of everything after key.

    hdelprop path key

deletes the property key at node path.

    hgetprop path key

retuns the current value of the property key on that node with prefix detailing node and attribute

    hgetpropval path key

retuns the current value of the property key on that node without any prefixes. Ideal for scripts.

    hlistprop path

lists all the properties available for this node.

    hmatchprop startnode propname data

searches a node whose property propname is includes in the string data. The search start at node defined by the path startnode. This is for searching for nodes with certain properties. Used for tree node association when parsing old bacth files.

* * * * *

#### SICS Commands To Deal with the Hipadaba

In order to address nodes in the hierachical parameter database, unix like path strings are used. For example: /sample/two\_theta/upperlimit would denote the a4 motors upperlimit in the sample group.

    hset path value

sets the value of the node at path to the data given as value.

    hinfo path

prints information about the node at path. Data comes as a comma separated list holding: datattype,number-of-children, length-of-data.

    hval path

prints the data for path without any additional text; ideal for use in scripts without extra conversions.

    hget path

prints the value of the node at path together with path.

    hzipget path

prints the value of the node at path in zipped format. This works only for arrays and is intendent for the efficient transfer or large data items. The message sent starts with: SICSBIN ZIP path length on a line. This is followed by length bytes of data compressed with gzip. Integers come in network byte order. Floats come as fixed point: multiplied by 65536 as integers in network byte order.

    hupdate path [value]

Updates the hipadaba node path with value. This is to be used to trigger automatic notifications from server side Tcl script after changing tree data. If value is missing, hupdate runs a get on the parameter and invokes update on it afterwards.

    hlist [option] path

lists the content of the node at path. Options specifies the print format. When option is not given, a normal list is printed. As of now two options are supported:

-   -val which prints a value for each list element
-   -cli which prints a komma sparated list containing: name, datatype,number-of-children,datalength, data, data ...


    hnotify path id [recurse]

adds an automatic change notification callback to the node specified by path and all its children. The callback will have the ID id which can later be used to delete the callback again. The optional parameter recurse defines if notifications should be applied to all children of the node. The default is 1 for yesy. Set to 0 for no. 

    hdelcb id

removes all callbacks with the ID specified as a parameter from the full Hipadaba tree. This does not find SICS second generation objects. 

    hdelcb id objname

removes all callbacks with the ID specified from the SICS object objname.  

    hcallnotify path

Does nothing to the node data but invokes all notification callbacks on the node. Use this when the content of the node has been internally modified and you wish to inform possible listeners of the change.

    harray path resize length

resize an array node to length

    harray path idx value

set the value of the array at path at index idx to value. This is for direct array manipulation at hipadaba nodes.

    harray path init val

initializes the whole array under path to val

    harray path sum xstart xend xdim ystart yend ydim

assuming that path is a 2D data set, sum the content of the window described by start, xend and ystart, yend.

    harray topath copynode frompath [start] [length]

Copies data from frompath to topath. Optional parameters start and length allow for copying a subset of data. No notification is sent to topath in this operation.

    dataname copytonode path start length

copy data from the sicsdata to the node described by path. Data in the range from start till start+length is copied. The original is in sicsdata.radi. This is just a reminder that this exists.

    hsubsamplehm hm path command

Store the result of subsampling the histogram memory hm with the command specified into the node described by path. The content of command is histogram memory specific.

    hscriptnotify node script

Calls script when node is being updated. This allows for flexibly updating dependent nodes when a node has chnaged its value. The callback installed by this makes sure that the node value has been copied before calling the script.

* * * * *

##### Miscellaneous Hipadaba related Commands

    status hdbinterest path

connects the SICS status with the hipadaba node path.

* * * * *

##### Obsolete Hipadaba Commands

    hmake path privilege datatype length

creates a new node with the given path path. Only the last path element is allowed to be new. The privilege to access this node is priv. Allowed values are: **spy**, **user**, **manager**, **internal**. The datatype is set to datatype. Allowed keywords for datatype are: **int**, **float**, **intar**, **floatar**, **intvarar**, **floatvarar**, **text** and **none**. If one of the array datatypes is selected an arraylength must be given.

    hmakescript path readscript writescript datatype arraylength

creates a hipadaba node which is connected to the rest of SICS through read and write scripts. The dataype parameters and length parameters behave as described above. This has to be separate because scriptable parameters have to be treated differently. The readscript is supposed to return the data read as ASCII. It can also return @@NOCOPY@@ if the data has been copied into the node by a C function called from the script. The writescript is called with the values to set as parameters.

    hlink path SICS-object-name [treename]

Links the SICS object SICS-object-name into the Hipadaba tree as a child of path. The SICS object to link must support the Hipadaba scheme for its native data storage needs. The optional parameter treename will be the name of the object in the Hipadaba tree.

    hcommand path script

creates a command node which calls script whenever it is set.
