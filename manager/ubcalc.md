The Online UB Calculation Routine
=================================

This module allows to calculate the UB matrix from either two
reflections and the cell constants or standalone from three reflections.
A little aid for indexing is implemented too. As usual when dealing with
calculations the common rule: shit in, shit out holds true. In this case
this means that the quality of the UB matrix obtained increases when:

-   Reflections have been centered as accuratly as possible, i.e. with
    slits closed and collimators in.
-   Two theta has been calibrated against a standard beforehand.
-   The crystal under investigation has been properly centered.
-   Cell constants and wavelength are accurate.

This module is for classic four circle diffraction only.

A range of commands allows for reflection and data input and
calculations. For the following discussion it is assumed that the UB
matrix calculation object has been configured into the system under the
name ubcalc.

ubcalc ref1, ref2, ref3 h k l two\_theta omega chi phi

Reflection input for up to three reflections. Miller indices must be
given. When angles are not given, the current position of motors stt,
om, chi, phi is read.

ubcalc cell a b c alpha beta gamma

Input of cell constants

ubcalc ub2ref

Calculate the UB matrix from ref1, ref2 and the cell constants.

ubcalc ub3ref

Calculate the UB matrix from the three reflections ref1, ref2 and ref3.

ubcalc listub

Print the calculated UB matrix

ubcalc cellub

Caclulate and print the cell constants as calculated from the UB matrix.

ubcalc activate

Copies the UB matrix to the hkl module. The positions of new reflections
can then be calculated with hkl.

ubcalc index two\_theta

Makes suggestions for possible miller indices matching two\_theta. If
two\_theta is omitted, the current value of two theta is read from the
motor stt. A brute force search through the space of possible indices is
undertaken using the cell constants given and the wavelength from the
hkl module. This routine is controlled by three parameters within
ubcalc:

-   difftheta, The maximum permissible difference in two\_theta
-   maxindex, The maximum value for miller indices in any direction.
-   maxlist, The maximum number of suggestions to list.

ubcalc difftheta, maxindex, maxlist value

Inquire or set the above parameters. For inquiry, give no value, for
setting the parameter give tha name and the value.

ubcalc list

Print all the data in ubcalc.

ubcalc listcell

Print the cell constants.

ubcalc listref1, listref2, listref2

Print reflections 1, 2 or 3
