### The McStas SICS Interface

It is useful to drive a simulation of an instrument with the same
interface as is used at the original instruments. One of the better
packages for performing simulations of neutron scattering instruments,
including samples, is McStas. This section describes the SICS interface
to McStas simulations. The interface consists of three parts:

-   A McStas controller module which controls the actual simulation.
-   A McStas reader which is responsible for reading simulated data into
    SICS counters and histogram memories.
-   Counter and histogram memory drivers which redirect their actions to
    the McStas controller module.

The general ideas is that all parameters are handled through the normal
SICS simulation drivers. The counting operations, however, are linked
with the McStas simulation. In order to be portable, many aspects are
controlled by scripts. These scripts are configured at the McStas
Controller. Several scripts must be defined:

    startmcstas

This script will be invoked when counting starts and has to collect the
necessary settings from SICS, construct a McStas command line and start
the simulation. As a return value this script has to return the PID of
the started mcstat process.

    mcstastest pid

Tests if the McStas simulation is still running.

    mcstasdump pid

Has to send a signal to McStas which causes it to dump its data without
terminating. Current versions of McStas do this on receiving the USR2
signal.

    mcstasstop pid

Stops the McStas simulation.

    mcstasread

Reads the McStas simulation output and transfers monitor and histogram
memory data into the appropriate SICS objects.

#### McStas Requirements and SICS Requirements

In order for the McStas SICS interface to work the McStas simulation has
to be configured in a certain way:

-   All parameters which have to pass between SICS and McStas have to be
    declared as simulation parameters in the DEFINE INSTRUMENT section
    of the instrument definition file. Alternatively SICS can write the
    data to be passed to McStas into a file. But then this file must be
    read in the INITIALIZE section of the instrument definition and
    values must be assigned to the appropriate McStas variables.
-   In order for the NeXus-XML based reading to work McStas must dump
    its data into a single file: use the **-f filename** option. The
    format must be **--format=XML**.
-   In order to count on monitor, a modified monitor component,
    MKMonitor MUST be used in the simulation. This component writes the
    collected total counts into a file. This file is the read by SICS in
    order to determine the control monitor. Evaluating the McStas dump
    file each time proved to be to inaccurate. The name of the file
    containing the monitor must be configured through: mccontrol
    configure mcmonfile name-of-file.
-   The mcstas simulation executable must be declared with allowexec in
    order to be able to start with the Tcl exec command.

#### The McStas Reader

In order to enable transfer from McStas result files into SICS objects a
reader object is needed. This module supports XML formatted McStas
files, with the output dumped into one file. The McStas options to
achieve this are: **-f filename --format="XML"** This module supports
the following commands:

    mcreader open filename

Opens a McStas simulation file for reading.

    mcreader close

Closes a McStas file after use.

    mcreader insertmon path object monitornumber scale

This transfers a monitor value from a previously opened McStas file into
a SICS monitor field. The McStas field read is the values tag belonging
to the component. The parameters:

**path**

The path to the correct values field in the simulation file. The format
is the same as the path format for NXopenpath in the NeXus-API (which
will internally be used). For groups, the name attribute is used a path
component.

**object**

The counter object into which the monitor is read. This is a limitation,
with the this McStas interface only counters can store monitors.

**monitornumber**

Monitor number is the monitor channel into which the value is to be
stored.

**scale**

Scale is an optional scale factor for the monitor. Real monitors have a
sensitivity of E-6, McStas monitors have an efficiency of 1.0. This
factor allows to correct for this.

    mcreader inserthm path hmobject scale

Inserts array data stored under path in the histogram memory array of
hmobject which must be a valid SICS histogram memory object. The path is
the same as given for insertmon, but of course the data part of the
detector must be addressed. Scale is again an optional scale factor
which allows to scale the McStas counts to real counts.

The mccreader module also supports reading data from any ASCII file into
SICS. Mcreader close and open are not required then. For reading
histogram memory data, the appropriate data has to be parsed into a
SICSdata object first. Then data can be trasnferred using the following
commands:

    mcreader insertmondirect counter num value

Assigns value to the monitor num at the counter object counter. Monitor
0 is the actual counts data.

    mcreader inserthmfromdata hm data

Inserts the data in the SICSData object data into the histogram memory
hm.

#### The McStas Controller

The actual control of the "counting" operation of the McStas simulation
is done by the McStas controller module in SICS. Internally this module
implements the routines for counting virtual neutrons. Towards the SICS
interpreter, an interface is exhibited which allows to configure and
test the McStas controller. The McStas Controller delegates many tasks
to script procedures written in SICS's internal Tcl scripting language.
This is done in order to achieve a degree of generality for various
types of instruments and in order to allow easier adaption to differing
demands. This is the SICS interface implemented:

    mccontrol configure mcstart startscriptname

Configures the script which starts the McStas simulation.
Startscriptname is the name of a Tcl procedure which collects the
necessary information from SICS, builds a command line and finally
starts the simulation. This script is expected to return either an error
or the PID of the started process. Startscriptname will be called with
the parameters mode and preset which represent the counting
characteristics.

    mccontrol configure mcisrunning runtestscriptname

Configures the name of a script which tests if the McStas process is
still running. Runtestscriptname is called with the PID as a parameter.
This returns 0 in the case the McStas simulation was stopped or 1 if it
is still running.

    mccontrol configure mcdump dumpscriptname

Configures the name of the script which causes McStas to dump
intermediate results. The script will be called with the PID of the
started McStas process as a parameter.

    mccontrol configure mckill killscript

Configure the name of a procedure which kills the current McStas
simulation process. KillScript will be called with the PID of the McStas
simulation as a parameter.

    mccontrol configure mccopydata copyscript

This configures the name of a script which has the task to read the
results of a McStas simulation and assign values to SICS monitors and
histogram memories.

    mccontrol configure update updateintervallinseconds

This configures the minimum time between McStas dumps in seconds. The
idea is that SICS buffers values during a simulation run and does not
interrupt the McStas process to often.

    mccontrol configure update monitorscale

Configures the scaling factor to use on the monitor in monfile. Normal
monitors have a efficiency of 1E-6, the McStas monitor counts every
neutron. This can be compensated for by this scaling factor. Note that
this scaling factor may be dependent on the wavelength used.

    mccontrol configure mcmonfile filename

This configures the file which mccontrol is going to read in order to
watch the simulation control monitor.

    mccontrol list

Prints a listing of the configuration parameters.

    mccontrol run scriptkey

Invokes one of the scripts configure for testing purposes. scripkey can
be one of: mcstart, mcisrunning, mcdump, mckill and mccopydata.

    mccontrol finish

This calls waitpid on the PID of the McStas process. This should be done
in the mckill script. Otherwise it may occur that the McStas simulation
turns into a Zombie process.

Standard scripts for many of the script routines required are provided
for the unix environment in the file mcsupport.tcl. Please note, that
all system executables called from scripts must be registrered with SICS
using the allowexec command.
