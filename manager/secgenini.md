### Initialising Second Generation Hardware Objects

#### Overview 

In order to initialise a second generation hardware object, two items are 
required:

1. A second generation device object. There are various commands to create such objects. 
2. A scriptcontext controller

The scriptcontext controller manages the communication with the hardware. It provides the low level line protocoll, serialisation of commands through a queue and polling. 

Nodes of the second generation device object are connected to the [scriptcontext controller](ScriptConDoc.md) by registering them with the scriptcontext controller. This registration installs special scriptcontext callback routines on the second generation object nodes. The scriptcontext callback routines are responsible for calling special command formatting Tcl scripts for the node, queuing commands and calling response evaluation Tcl scripts on the node. The names of the scripts to call are configured via node properties. 

Much of this hydraulics is hidden in device dependent Tcl driver modules. But it is worth knowing anyway. Anyway, much of the driver programming for second generation hardware objects happens in Tcl. Thus there is some overlap between configuration and programming.  

A typical initialisation for a scriptcontext controller looks like this example:

	makesctcontroller mota std $ts:3002 \r 5 \r

There is the keyword makesctcontroller followed by the name of the controller, in this case mota. This is followed by a keyword describing the protocol driver to use plus protocol dependent parameters. In this case it is the protocol driver std with parameters $ts:3002 \r 5 \r. The first protocol parameter follows a very common form: it is host:port and this encodes the network address of the device. 


#### Second Generation Motors

##### EL734 Motors

This is the old standard PSI EL734 motor controller. For the configuration see the example:

    source $home/sicscommon/el734.tcl
	makesctcontroller mota std narsiss-ts:3002 \r 5 \r
	mota transact "RMT 1"
	mota transact "ECHO 0"
	mota transact "RMT 1"
	mota transact "ECHO 0"

	el734::make mom 1 mota
	el734::make fom 2 mota

The first line loads the el734 Tcl module. This is required only once per initialisation file. 

The second line installs a scriptcontext controller named mota. One line per EL734 controller is required. The name of the controller and of course the address of the terminal server and the port to which the EL734 controller is connected too need to be changed. 

The following lines, containing the string transact, switch the motor controller into remote mode. Copy and just change the 
controller name.

Then there are lines of the form:

    el734::make motorname motno controllername

Motorname is the SICS name of the motor. Motno is the number of the motor in the EL734 motor controller. Motno is in the range 1-12. Controllername is the name of the scriptcontext controller managing this motor. 

##### Delta Tau Motors

This is the new PSI standard motor controller. Another example: 

	source $home/sicscommon/deltatau.tcl
	makesctcontroller pmac01 pmac orion-mcu1:1025 5
	MakeDeltaTau mom1 pmac01 1
	MakeDeltaTau mom2 pmac01 2


The first line  installs the deltatau tcl module. Required once per initialisation file.

The second line creates a scriptcontext controller with the name pmac01. The only parameters which need to be changed in this line are the name and the network name of the MCU. 

Then there are lines of the form:

    MakeDeltaTau motorname  controllername motNo 

Motorname is the SICS name of the motor. Motno is the number of the motor in the Delta Rau MCU motor controller.  Controllername is the name of the scriptcontext controller managing this motor. 

There is another variant:

   MakeDeltaTau motorname  controllername motNo 1

Use this when the motor has been inverted in the MCU. The symptom, the 1 cures, are that the lower and upper hardware limits are interchanged. 

Due to some limits of the Delta Tau MCU control unit and the implementors it is sometimes required to override the driver for special use cases. 


##### Phytron Motor Controllers

The Phytron motor controller is a 2 axes unit which is used at SINQ for less important or  portable devices. Another example:

    source $home/sicscommon/phytron.tcl
    makesctcontroller stickrot phytron boa-ts:3003 5
    phytron::make om X stickrot -180 360 0


The first line  installs the deltatau tcl module. Required once per initialisation file.

The second line creates a phytron scriptcontext controller. Change the name and the network address as required. 

Then there is the line:

    phytron::make motorname X|Y controllername lower upper encoderflag

This creates a phytron motor motorname. The other parameters are:

* **X|Y** Either X or Y to select the axis in the phytron motor controller
* **controllername** The name of the phytron motor controller created with the makesctcontroller line
** **lower, upper** The hardware lower and upper limits for this motor
** **encoderflag** Is 1 if the motor has an encoder, 0 else. 

The phytron has another special feature. A line like:

     phytron::configureM2 motorname X|Y out

configures motorname to set the output out after driving. This is used to set brakes on motors which would otherwise creep out of position. Some motors are just creepy :-) The exact value of out has to be asked from the electronics group. 


##### Nanotec Motors

The Nanotec motors are RS-485 bus motors as used at BOA. Another example:

    source $scripthome/nanotec.tcl
    makesctcontroller nano5 std boa-ts:3014 \r 1 \r
    nanotec::make sbl 38 nano5   -50 50 1600


The first line loads the nanotec Tcl driver.
The second line creates scriptcontext controller for the bus to which the motor is connected. RS-484 buses are accessed via terminal servers. Thus the address of the terminal server and port and the name of the scriptcontext controller may need to be changed.

The line:

    nanotec::make motorname busNo controllername lower upper scale

configures motorname into SICS. The parameters:

* **busNo** The ID of the motor on the RS-484 bus
* **controllername** the name of the controller for the bus as initialized with makesctcontroller
* **lower, upper** Lower and upper hardware limits for the motor
* **scale** A scale factor to apply to both reads and writes of the motor. 


##### Simulation 

For software testing purpose it is useful to have a simulation motor. This is how it is initialized. This sort of motors are purely software and do not move stuff around. 

    source $home/sicscommon/secsim.tcl
    MakeSecSim phi -20. 20. 1.

The first line loads the Tcl code for a simulated motor.

The second line has the form:

    MakeSecSim motorname lower upper speed

Ths creates a motor with the name motorname and hardware limits lower and upper. The parameter speed influences how fast the simulated motor drives. The simulated motor is also used for regression testing. Thus it has additional parameters which allow to simulate a number of common motor error conditions. 

#### Counting Stuff

##### EL737 Counter Box

The old counter box, inherited from the dawn of SINQ. Still a mainstay device as the second generation electronics has not yet materialized. 

	source $home/sicscommon/el737sec.tcl
	MakeSecEL737 el737 $ts:3008

The first line loads the Tcl counter driver.

The second line creates the counter object. The syntax is:

    MakeSecEL737 countername terminalserver:port

Terminalserver:port defines to which port at which terminal server the counter box is connected too. 

##### Simulation Counter

It is useful to have a software counter for testing. This is initialized:

    source $home/sicscommon/secsimcter.tcl
    simcounter::make el737

The first line loads the simulated counter Tcl driver. 

The second line just makes  the counter object. The only parameter is the name of the counter object in sics which is to be created. 

##### Histogram Memory

Throughout SINQ we the SinqHM histogram memory software is used for position sensitive detectors. The interface to SICS is a WWW-interface. For configuration, SinqHM evaluates a XML file. The XML is dynamically created. Either by loading a static XML data file or by generating the XML though a script.  The syntax of the XML file is documented, if at all, in the SinqHM documentation. The initialisation of a HM object in SICS looks like this:

	source $home/sicscommon/sinqhttp.tcl
	MakeHTTPHM banana 2 sans2hm "loadsinqhm $scripthome/sansii_a3.xml"
	banana dim 512 512
	banana mode timer
	banana preset 100
	banana stop
	banana init

The first line loads the Tcl driver code.

The second lines syntax is:

     MakeHTTPHM hmname rank SinqHMhost configscriptname [tof]

The parameters:

* **hmname** The name of the histogram memory in SICS
* **rank** The dimensions of the area detector
* **SinqHMHost** the host name of the histogram memory server 
* **configscriptname** The name of the script which generates the XML configuration data
* **tof** An optional flag which adds time-of-flight processing for the last dimension of the HM

The initialisation is not yet complete. Some parameters of the HM must be set and then **hmname init** must be called to configure the histogram memory both on the SinqHM computer and within SICS. The minimum is to configure the dimensions of the HM. 

Please note that this driver does not automatically install polling on the HM data. The HM data is loaded though when counting finishes. Anything in between has to be configured manually. 

TOF mode requires another configuration. The initial time delay has to be forwarded to the Multi Detector Interface (MDIF) when the time binning changes. This is usually done from the HM configuration script. The MDIF communicates via a serial port. Thus a suitable configuration looks like this:

	MakeRS232Controller mdif $ts 3009
	mdif replyterminator 0xd
    mdif sendterminator 0xd
	mdif timeout 2000
	mdif write "RMT 1"
	mdif write "ECHO 0"

The only thing to change is the name of the terminal server and the port number to which the mdif has been connected. Forwarding the delay happens with a command like:

    mdif send "DT 80"

replace 80 by your number. 

Extra detector banks or TOF monitors require modifications of the driver code. See examples in existing instrument initialisation files how this is done. 

##### CCD Cameras

The support for CCD cameras is available in general in SICS. As of now, we only have one: the ANDOR Ikon-M at BOA. SICS accesses CCD cameras with the help of the CCDWWW server. This is a little HTTP server running on a separate computer which manages the CCD camera and exposes its capabilities through a HTTP interface. The initialisation is as such:

    source $home/sicscommon/ccdwww.tcl
    source $home/sicscommon/andorhm.tcl
	exec /bin/cp $scripthome/andor.xml $scripthome/camera.xml
	MakeAndorHM hm ${ccdwww}:8080


The first line loads the genereall CCDWWW camera interface module.

The second line loads ANDOR special CCD camera module. It extends the basic ccdwww module with specifics for the ANDOR camera.

The third line copies a camera data file to camera.xml. This is required by the andor driver to configure itself.

The fourth line installs a ANDOR CCD camera as hm into SICS. The syntax:

    MakeAndorHM hmname ccdwwwcomputer:port


SICS tries hard to treat a CCD camera in a similar way as a normal histogram memory. But there are differences in running the hardware. With histogram memories, SICS stops the HM after the controlling counter reported it finished. This is not advisable with CCD cameras, you have to wait until the CCD camera finished exposing and transfering data. With  BOA, there is another complication: in the Ikon-M at BOA a special bulb mode was installed which has the exposure controlled by a TTL signal from the counter box. This, and the different end behaviour, requires a special configuration of the HM, counter and the multi counter control module. An example can be seen below. 

	hsetprop /sics/hm/status httpevalstatus boaevalstatus hm
	hsetprop /sics/counter/control write el737boacontrol
	MakeHMControl hmc counter hm
	hmc stopslaves 0
    hmc boamode

The necessary additional script programs, el737boacontrol and boaevalstatus, can be copied from BOA. 

##### Simulation HM

There is also a simulation histogram memory. 

    source $home/sicscommon/simhm.tcl
    simhm::MakeSimHM banana 2
	banana dim 512 512
	banana stop
	banana init

The first line loads the Tcl driver module

The second line has the syntax:

    simhm::MakeSimHM hmname rank [tof]

The parameters have a similar meaning as with the real HM. As with the real HM, the simulated HM needs additional configuration, at minimum dimensions,  and a hmname init to function properly. 


##### Multi Counter Control Module

When there is one or more histogram memories, the counting operation has to be synchronised. This is one task of the multi counter module. Then there is scan support. For scanning, SICS expects somtehing with the same interface as a single counter for counting. The multi counter module provides for this. In this context, it must be asked what constitutes the counts when scanning against a position sensitive detector? Often this is the sum over the detector or of a window on the detector. As this can vary, the multi counter module is configured with a script which collects counts and monitors whenever count data needs to be transferred. A multi counter control module is configured like this:

	MakeMultiSec counter 8
	counter master el737
	counter slaves banana
	counter transfer hrpttransfer

The fist line creates a multi counter with name counter and 8 monitors.

The second line configures the master counter, in this case el737. This is the normal EL737 counter box which triggers counting.

The third line configures the slave histogram memories. This is a komma separated list of slave HM to account for multi detector systems. In the example, there is only one HM, banana

The fourth line configures the name of the count and monitor transfer script, in this case hrpttransfer. Whta happens in this script is up to the configurator. An example transfer script is given below:

	proc hrpttransfer {} {
	    hupdate /sics/counter/time [sget "el737 time"]
	    set val [SplitReply [banana sum 0 1600]]
	    append res $val " "
	    for {set i 1} {$i < 8} {incr i} {
			append res [SICSValue "el737 getmonitor $i"] " "
	    } 
	    hupdate /sics/counter/values  $res
	}
	Publish hrpttransfer Spy



#### More Hardware

##### Agilent Frequency Generator

The polarised people use this at BOA, NARZISS and sometimes AMOR. It initializes like this:

    source $home/sicscommon/stddrive.tcl
    source $scripthome/agilent.tcl
    makesctcontroller agi std freq1:5025 \n 2 \n \n
    agilent::make agi

The first line install the stddrive module. This is a basic drivable object which relies on the comparison of current and target value for termination.

The second line installs the agilent module.

The third line creates a scriptcontext controller, named agi. Replace freq1 with the network anme of your agilent. Make sure that the Agilents TCP/IP address has been properly configured from the front panel. 

The fourt line creates the agilents objects. The only parameter is the name of the scriptcontext controller created in line three. After this, there will be three virtual motors: freq, pow and phphase which allow to drive the fequency, power and phase of the frequency generator. Phase makes only sense if there is a second one. That forth line can also look like:

    agilent::make sciptcontextcontrollername freqname powname phasename

This versions allows to configure which names are used for the virtual motors for frequency, power and phase.

##### Rhode Schwarz Frequency Generator

Yet another frequency generator with a RS-232 interface. Again used by the polarisers at AMOR, NARZISS and else.
It is initialized with a line:

	source $home/sicscommon/rhodeschwarz.tcl

The code assumes that the rhode schwarz is connected to port 8 at the terminal server. If this is not the case, edit the Tcl code.

##### Dornier Chopper Controllers

These are the old Dornier chopper controllers. They are operated via a DOS program which listens to the outside world on a  RS-232 port. This is initialized like this:

	set amor 1
	source $home/sicscommon/astrium.tcl

The first line selects the chopper. Either set poldi, amor or focus to 1. 

The second line installs the chopper. The terminal server port assignement is hard coded in that file. Edit when changes are required. This module provides for several things:

* A choco object which shows all the parameters of the chopper
* A chosta command which shows the chopper sttaus in a well formatted list
* Virtual motors for chopperspeed, phase, ration and such, as appropriate to the chopper system.

 

