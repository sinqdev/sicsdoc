#### The Single Crystal Master Module

This module holds the information which is global and valuable for a
four circle diffractometer. This is the cell, the UB, the wavelength and
the assignment of motors for the various functions in the
diffractometer. The Single crystal master module is also responsible for
initializing all the other four circle modules. TRICS supports various
modes of operation. It can operate as a normal four circle
diffractometer in bisecting mode. It can do normal beam mode, titling
the detector up. And it can also use the tilt motors of a standard
sample table to drive to reflections. The latter one has been inherited
from the triple axis and is not yet fully implemented.

##### Configuration


Four circle mode can be activated though the

    MakeSingleX

command in the instrument initialisation file. This also installs a
command **singlex** into SICS.

The next thing to do is to assign real motors for the necessary
functions of the four circle diffractometer. This is done through a
command:

    singlex configure function motorname

This assugns motorname to perform the function function in the
diffcratometer.

The following function names are supported:

* **stt** two theta, gamma
* **om** omega
* **chi**
* **phi**
* **nu** normal beam tilt motor
* **sgu** cradle tilt motor
* **sgl** second cradle tilt motor

Another thing to do is to define the source of the wavelength. This
happens through the

    singlex configure lambda var

command. Singlex as of now supports three cases:

1.  var can be a SICS driable variable, such as the lambda variable
    associated with a monochromator
2.  var can be a SICS variable
3.  if var cannot be found in SICS, a local field in singlex is created.

#### Commands

    singlex ub [ub11 ub12 ub13 ub21 ub22 ub23 ub31 ub32 ub33]

sets or queries the UB matrix

    singlex cell [a b c alpha beta gamma]

sets or queries the cell constants.

    singlex mode val

selects the diffractometer mode. Currently supported are: **bi** for
bisecting, **bio** for bisecting orion, **nb** for normal beam ,
 **binb** for normal beam plus eulerian cradle and **tas** for triple axis.
The difference between bi and bio is that bi is the TRICS setup; crystal
hanging down, whereas bio is the ORION setup with the crystal standing
up.

    singlex lambda [val]

optionally for setting and querying the wavelength.

    singlex motval function

returns the value of the motor used for one of the functions defined
above.

    singlex lattice

is a read only parameter which prints a code for the lattice type: 1 is
triclinic, 2 monoclinic, 3 orthorhombic, 4 tetragonal, 5 trigonal, 6
hexagonal, 7 cubic.

    singlex motnam function

returns the motor name for the function argument

    singlex symref h k l

checks if a symmetrically equivalent reflection to h,k,l can be reached
within the instrument limits. If so, the reflection is returned as
h,k,l. Else an error is thrown.

    singlex sttub h k l

calculate stt only for h, k, l using the UB.
