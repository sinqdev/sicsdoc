### Asynchronous I/O Implementation

Interfacing with hardware in the new word of the second generation SICS
object model involves two SICS objects:

1.  The actual SICS object holding the parameters. For instance a motor
    or a chopper.
2.  A scriptcontext controller which is responsible for handling the
    communication with the device asynchronously.

It is envisaged that most hardware will be accesed through the script
context and its use of script for command formatting and reply
processing. However it is possible to use the asynchronous I/O
infrastructure from C-code implementing for instance node callbacks too.

* * * * *

#### UML

![](asynctrans.jpg)

AsyncTransaction is an abstract base class. It encloses all details
about a current transaction. This is one executing script chain. In the
current implementation AsyncTransaction is implicit through the
parameter list of DevQueue etc. in devser.\*.

![](scriptcontext.jpg)

AsyncProtocoll is an abstract class which represents a line protocoll.
various implementation will appear over time. At the time of writing
three implementations exist: \<cr\>\<lf\> protocoll, sinqhm-http and
Jülich chopper protocoll.

The inner class called sct in ScriptContext can be used in scripts to
conveniently:

-   set and get nde proerties
-   store the time
-   print tect to the user

* * * * *

#### ScriptContex Use Cases

These use case are meant as the documentation for using the full
software stack, including the scripting mechanism.

###### Get

1.  The get callback on the node checks if the geterror property has
    been set.
2.  If yes:
    1.  return the error

3.  else
    1.  return the cached value.

##### Set

1.  Something asks a node to set.
2.  This causes the set callback of the node to be invoked.
3.  The set callback sets the target property of the node.
4.  The set callback checks if a script has been registered in the check
    property of the node.
5.  if yes
    1.  The check script is invoked.
    2.  If the check script returns an error
        1.  Abort

6.  The set callback queues a write action with the device serializer.
7.  See below for transaction processing.

##### Update

1.  The device serializer initiates the update of a node by invoking its
    read script.
2.  See transaction processing for further details

##### Transaction Processing

1.  Eventually the device serializer comes down to schedule the pending
    request.
2.  It calls the actionHandler of the ScriptContex action with a NULL
    reply.
3.  The actionHandler determines what kind of action it is. If it is a
    write action, it looks in the write property of the node for a
    script to execute. If it is a read action it looks for a script to
    execute in the read property of the node.
4.  The located script is invoked. The script sets the send property of
    the node to the command to send to the device. It returns the name
    of the property holding the script to execute with the next reply.
5.  The device serializer passes the command on to the protocoll
    handler.
6.  The protocoll handler sends the command to the device.
7.  The device serializer keeps polling the device. Eventaully either a
    reply or a timeout is detected with the help of the protocoll
    handler.
8.  Again the actionHandler of the AsyncAction is called with the reply
    provided. The AsyncAction remembers the name of the next script to
    invoke and invokes it with the reply. The script evaluates the
    reply. Depending on how well it likes the reply the following can
    happen:
    1.  It returns Idle, thereby indicating that ths script chain ended.
        If it is a read script it may either update the geterror
        property or the node value.
    2.  It sets a new command to send in the nodes send property and
        returns the name of property holding the script to evaluate with
        the next reply.

9.  Automatically the nodes read\_time or write\_time property are set
    to the time of the response.

##### Drive Adapter

1.  The drive command calls the CheckLimits function.
2.  The CheckLimits function calls the checklimits script
3.  On success, devexec calls the SetValue function. SetValue does:
    1.  Sends a set to some node.
    2.  Changes the scheduling for one or more nodes to update
        frequently and with progress priority.

4.  Behind the scenes the DeviceSerializer will process the node setting
    scripts and keep on updating the nodes scheduled with high priority.
5.  The device executor keeps calling CheckStatus which in turn calls a
    checkstatus script.
6.  The checkstatus script will try to make sense of the progress nodes
    and translates their states into HWBusy, HWFault and HWidle, as
    required by the device executor. This has to be done by a script as
    termination criteria may differ: sometimes the device provides a
    nice status code, sometimes one has to check if the parameter is
    close to the target for a certain amout of time. Similarly the
    detection of error conditions may require an intelligent script.
    When the device becomes idle, this script also has to reset the
    update parameters of the progress nodes.
7.  GetValue keeps returning cached values.
8.  Halt sets the nodes state to halt and enqueues this with the very
    high priority halt.

The script necessary for this may be stored as node properties for the
node which is actually driven. Then the drive adapter can be initialized
with the name of this node alone.

* * * * *

#### Device Serializer Use Case

This is some information how to use the asynchronous I/O infrastructure
from C-code.

1.  A node receives a set or read request.
2.  In reponse the node creates an AsyncTransaction and queues it with
    the DeviceSerializer.
3.  Eventually the DeviceSerializer comes down to executing the
    AsyncTransaction.
4.  It calles the AsyncTransaction.actionHandler method. This does what
    it needs to do and returns a command to be sent.
5.  The DeviceSerilaizer formwards this command to protocoll handler.
6.  The protocoll handler converts the command into a valid command
    string and sends it to the device.
7.  The DeviceSerializer in its main loop keeps checking for responses
    from the device.
8.  Response data is processed by the protocoll handler until it decides
    that either a timeout or a complete response has been read.
9.  The DeviceSerializer calls AsyncTransaction.actionHandler with the
    reponse as a parameter.
10. AsyncTransaction.actionHandler evaluates the response and:
    1.  formats a new command and returns it.
    2.  Returns NULL in order to indicate that the transaction is
        finished and the DeviceSerializer can try to schedule the next
        AsyncTransaction pending on the queue.

To make this extra clear: this is a loop: The DeviceSerializer keeps
calling AsyncAction.actionHandler until it returns NULL and thus
indicates that the chain of commands and responses is finished.
