## Hipadaba Properties

### General

- **priv** privilege required
- **sicsdev** associated SICS device when appropriate
- **type** Type of thing: drivable, countable, etc
- **__save** This parameter is to be saved into the status file
- **sicscommand** The SICS command for setting this parameter. Not used all over the place but some. 

### ScriptContext Related

- **read** contains script to call for reading.
- **write** contains script to call for writing. As above, this states the start of the respective script chain
- **geterror** A possible reading error. Will be checked for by code that read the value. Is asynchronously set when an error reading the value occurred
- **read_time** last time stamp read
- **write_time** last time stamp written

### Graphics Related

These are instructions for GTSE for discovering graphics elements and associating the appropriate viewer with it.

- **type=graphdata** says that this node contains graphics data 
- **viewer** The GTSE viewer to use for this graphics
- **type=data** annotates the data for the graph
- **type=axis** annotates an axis for drawing
- **dim** holds to which dimension the axis belongs
- **transfer** annotates the transfer type of the data
- **mirrory** for mirroring the data in y
- **mirrorx** for mirroring the data in x


### NeXus File Writing Related

This never went beyond a proof of concept but can be revived. A hipadaba tree is annotated with properties and these are used to control data file writing. The controlling routine is **hdbstorenexus path pass**. Often, NeXus file writing goes through stages: values to write at the start of the file, values to update while or after counting etc. To this purpose hdbstorenexus has the pass argument which specifics which stage of data file writing is currently being run. Hipadaba nodes are annotated with properties:

- **nxpass** a komma separated list of stages when this parameter will be written. Normal writing will occur.
- **nxslab** indicates that this value is to be written as slab. The value is a script to run which returns the start and end values of the slab
- **nxalias** an alias to use against the NeXus dictionary for retrieving where the data item is to be written too. 

There needs to be a wrapper around this which opens the NeXus file and the dictionary, then processes one or more trees with hdbstorenexus and the appropriate pass. Then finishes by closing the NeXus file. I fear that this is not yet complete and needs extensions for scanning. 


