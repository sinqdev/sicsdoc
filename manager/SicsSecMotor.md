Second Generation Motors
========================

This describes the new, revised motor handling concept for SICS. The new
motor object is compatible with the old one to other SICS code. From the
old motor object it collected the logic for handling zero points,
permissions etc. The whole access to the hardware is different though
and left to actual instances of a second generation motor. Instances can
implement hardware access in two different ways:

1.  In C code through the addition of additional parameters and
    callbacks.
2.  Through the use of the scriptcontext system and the addition of
    parameters and properties to the second generation motor object.

Obligations of a Second Generation Motor Instance
-------------------------------------------------

In order to make a second generation motor tick, the instance has to
provide a couple of things.

1.  Connect the hardposition parameter with the hardware. When a motor
    is driven, all the checks are made and finally the hardposition
    parameter is set. Likewise, reading the motor maps to reading the
    hardposition and applying the necessary corrections.
2.  The hardposition script chain must look like this: send set command
    - read set reply - send status read command - evaluate status read
    response. The general idea is that targetposition/set should not
    come back before the motor is actually running. Of course, error
    checking is to be added.
3.  Provide callbacks and scripts to update an status parameter on the
    motor. Possible motor status values are: idle, run, error, poserror
4.  Provide a halt function to the motor which tells the hardware to
    stop.
5.  Add motor specific hardware parameters, line encoder assignements,
    gearing ratios etc. to the raw second generation motor object.
6.  Provide set and getter scripts or callbacks , as appropriate, for
    all motor driver parameters. These especially include:
    -   hardupperlim: The hardware upper limit
    -   hardlowerlim: The hardware lower limit


