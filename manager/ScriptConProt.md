### ScriptContext Protocol Drivers

This is documentation for the scriptcontext protocol drivers. They are implemented in ANSII-C and control the low level line discipline of the device. 

There are some standard placeholders used in the description of the various protocols. These are:

* **sctname** The name of the scriptcontext controller in SICS
* **host:port** The network address of the device in the form hostname:portnumber 
* **sendterminator** The terminator characters used to end a message when sending
* **timeout** The maximum time to wait for a reply
* **replyterminator** The terminator character which ends a message from the device


#### The Standard crlf Protocoll

This is the protocol used by the many devices which use terminators such as cr or lf to delimit messages. It is installed by the command: 

    makesctcontroller sctname std host:port sendterminator timeout replyterminator

Everything after sendterminator is optional and is set to defaults applicable to the common crlf style protocols.

When using this protocoll in driver scripts some special control  strings are recognized when passed to **sct send**:

* **@@NOSEND@@** prevents sending new data to the device. Use this to read multiline replies.
* **command{n}** Another way to read multi line responses. N is the number of lies to read. A line count of 0 can be used to read not all. This is useful for devices which do not respond occasionally. 

#### The Char by Char Protocol

This is another variant of the crlf protocol for a device which expects an echo character to be read for each character sent. This protocol handles this. The initialisation requires:

    makesctcontroller sctname charbychar host:port sendterminator timeout replyterminator


#### The Term Protocol

This is a variant of the standars crlf protocol which can deal with variable reply terminators as some badly designed devices send. The only difference to the standard protocol is that it expects **sct send** strings of the form replyterminator:payload. 

It is initialized with:

    makesctcontroller sctname varterm host:port sendterminator timeout 


The parameters have the same meaning as described above.     

#### The ZwickRoll Protocol

This is a a variant of the standard crlf protocol which does not clear the line between command/response pairs. This is to receive unsolicited messages which this controller sends in copious amounts.  This is yet another badly implemented protocol used by the Zwick Roll Test Rig at POLDI. The initialisation is:

    makesctcontroller sctname zwickroll host:port sendterminator timeout replyterminator


#### The Juelich Chopper Protocol

This is a variant of the standard protocol which appends the special checksum required by the Juelich Chopper System before sending. The Juelich chopper system is used at MARS. It is initialised with:

    makesctcontroller sctname julcho host:port sendterminator timeout replyterminator

The parameters are the same as with the standard protocol described above. 

#### The Astriumnet Protocol

This protocol is used by the newest version of Astrium chopper control software running on a Windows PC. This is quite a complicated protocol with autorisation and a XML encoded message format. 

The installation is easy:

    makesctcontroller sctname astriumnet host:port password

where **password** is the controller password for the chopper.

This protocol uses special strings for **sct send** They have the form: keyword:keyword:keyword: or keyword:keyword=newvalue: The latter form is for writing. Where keyword is one of the many parameters or system designators of the chopper. See the chopper documentation for details. 

#### The HTTP Protocol

This protocol is used to communicate with HTTP servers. It is optimised for use with the SinqHM WWW-server which is a mainstay at SINQ. 

The protocol is initialized with:

    makesctcontroller sctname sinqhttpopt host:port transferdata timeout username password

There are some special parameters:

* **transferdata** is the name of a SICSData array object which is used for storing bulk array data from the SinqHM server
* **username** is the WWW sever user name needed to authenticate for accessing some features
* **password** is the WWW-server password needed to authenticate for using some features. 

There is some special syntax for **sct send** when using this protocoll:

* For a normal GET request the path on the WWW server to access is sent.
* A POST request looks like: **post:path:postdata** This is the keyword post follwed by a colon and tha path to which to post. Follwed by another colon and the post data
* For storing histogram memory array data into a Hipadaba node there is the syntax: **node:/hipadaba-path:path-on-WWW-server**. 

#### The PMAC Protocol

This protocol is understood by the second generation delta tau motor controllers. The protocol wraps command strings into binary messages.
The protocol operates via TCP/IP sockets.

The PMAC protocol is installed with:

    makesctcontroller sctname pmac host:port timeout


#### The Pfeiffer Protocol

This is a command protocol variant understood by the Pfeiffer vacuum measurement devices. The protocol does not do very much expcept sending the right terminators and the ENQ character any now and then. 

The protocol is initalized like this:

    makesctcontroller sctname pfeiffer host:port

#### The Phytron Protocol

This is the protocol spoken by the seconadary, portable phytron two axes motor controller. This controller if often used with sample stick rotations in cryostats. The protocol requires  messages and replies to be enclosed into special binary characters. 

The protocol is installed with the command:

     makesctcontroller sctname phytron host:port timeout 

The phytron is connected via a terminal server. Please note that the default baud rate of the phytron controller is 49600. Normally the electronics guys change that to the SINQ standrad of 9600 baud. But if it does not work, keep this in mind.

#### The SLS Echo Protocol

This protocol allows access to yet another variant of the SLS magnet controller communication. It is for the shiny small silvery magnet controller boxes with a TCP/IP port on one side and 8 fibre optic links to magnets at the other side. Currently it is used at TASP for the mupad. This is an all binary protocol. It expects **sct send** strings of the form:

    channel:mode:code:val:convertflag

with mode being either r or w and convertflag being one of none,read,write. The code refers to the SLS-DSP register addresses 
used to control the magnet. 

The protocol is initialized with: 

     makesctcontroller sctname slsecho host:port timeout 


#### The Binary Protocol Driver

This protocol driver translates an ASCII encoding of a binary protocol into the actual binary protocol. The translation rules:

space separated items, the command first, a slash and then the read format
 
     commandItem ... / formatItem ...
 

where a commandItem one of the following

* a number   converted according to the choosen format, i1 is default
* int<n>     changing the format to <n> byte integers
* hex        changing the format to hexadecimal (1 byte at a time)
* float      changing the format to 4 byte ieee float
* crc        send crc

and formatItem is one of the following

* skip       skip one byte
* skip<n>    skip <n> bytes
* code       returned function code, when bit7 is set, the response is recogized as an error message and the response is dumped in the result
* int<n>     convert <n> bytes to integer (most significant byte first) and append the (decimal coded) number to the result if <n> is omitted, <n> = 1 is assumed
* hex        convert 1 byte to hexadecimal coded integer and append it to the response
* float      convert 4 bytes from ieee float and append the number to the response
* crc        check crc (if wrong, "badCRC" is added to the response)
 
 Multiple items in the response are space separated
 

 Usage example: Modbus read (float)
 
 * command:  address 250, function 3, start-address 8, size 2, crc
 * response: address, function, byte-count, float, crc
 
     sct send 250 3 int2 10 2 crc / skip code skip float crc

The initialisation is achieved by:

     makesctcontroller sctname binprot host:port crckey timeout

The crckey determines the algorithm used for  the calculation of possible CRC values. Currently the following algorithms are implemented:

* **keller-crc**
* **sycon-crc**
* **rsport-crc**
* **modbus-crc**

#### The Sea Client Protocol

This protocol is used for the communication between SICS and a Sea Server or with another SICS server. The protocol is initialized with:

     makesctcontroller sctname seaclient host:port


#### The JVL Protocol

This is a protocol drive for the JVL RS-485 bus motors. This protocol 
is not used anymore as the VL motors proved to be unreliable hardware and the accompanying software was badly implemented. The protocol is weirdly binary. 

The initialisation:

     makesctcontroller sctname jvl host:port timeout


#### The Sputter Protocol

This is a protocol spoken by the Munich sputter machine which is sometimes used at AMOR. The server is Labview (Argghhhh!) and instable. The protocol uses a command length prefix and a crc. Responses are always two lines: a success indicator and the actual data. 

This protocol is initialized with:

     makesctcontroller sctname sputter host:port


#### Test Protocol

This is a protocol for software testing. In its simplest form it just returns an echo of the command sent. When initialized with a filename, it reads a dictionary of pre programmed responses from the file. An escape to Tcl allows to call scripts in order to compile the response. 
The dictionary file contains pairs of:

    command=response

where command is the command given to **sct send** and response is what should be returned. A special entry:

    tclescape=something

allows to configure the escape string in response which indicates that a Tcl script should be invoked to compile the response. 

The initialsation is achieved like this:

     makesctcontroller sctname testprot filename

where filename is the option file name for the dictionary. 

