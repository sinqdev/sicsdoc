### SICS Options and Users

The SICS server has an internal options database which holds such values
as port number to listen too and the like. Additionally there exists a
user database. Currently these values are configured from the
initialisation file. The commands to do this are:

-   **ServerOption name value** defines the server option name to be
    value.
-   **SicsUser name password accesscode** defines a user with name and
    password and an access right accesscode. Accesscode is an integer
    from 1 to 3 for User, Manager and Spy rights. A user with Spy right
    may look at everything but cannot change anything. A user with user
    privilege may change most of the SICS parameters, perform
    measurements etc. A user with manager privilege may do everything to
    the system.

The Sics server currently uses the following options:

-   **ReadTimeOut** The server checks in each cycle of the main loop fro
    pending commands. Het waits for new commands for a specific period
    of time. This value is the ReadTimeOut. It should be as short as
    possible. This value may need to be increased if there are network
    performance problems. A good value is 100.
-   **ReadUserPasswdTimeout** This is the time a client has to send a
    username password pair after a connection request. If nothing comes
    in in that time, the connection request is terminated.
-   **LogFileBaseName** defines the path and the base name for the
    server log file.
-   **ServerPort** defines the port number the server is listening to.
    Should be greater than 1024 and less than 64000. The port number
    should also be different from any other server port number in use on
    the system. Otherwise evil things may happen.
-   **InterruptPort** The SICS server can handle interrupts coming in as
    UDP messages on a special port. This option defines this UDP port
    number. The choice of possible port numbers is limited by the
    constraints given above in the ServerPort section. Espacillay this
    port number MUST be different from the ServerPort number. The UDP
    messages accepted are expected to consist of the string SICSINT
    followed by an interrupt number. For interrupt numbers see file
    interrupt.h.
-   **DefaultTclDirectory** specifies where Tcl defined commands are
    stored. When this is properly defined Tcl will autoload commands.
-   **statusfile** defines the file to which he current state will be
    saved on close down of the server and restored from at startup time.
-   **TelnetPort** The port number where the SICS server will be
    listening for telnet connections. If this option is missing login
    via telnet to the SICS server is disabled.
-   **TelWord** In order to login to SICS via telnet a magic word is
    needed. The login word, This option defines this word. If this
    option is missing telnet login to SICS is disabled.
-   **LogFileDir** This server option defines the directory where
    commandlog log files are kept.
-   **RedirectFile** This defines a filename to which all output to
    stdout and stderr is logged by the SICS server. This is mainly a
    debugging feature.
-   **TecsPort** The port number at which the Tecs temperature control
    server is listening.

