Initialization of General Commands
==================================

This section gives details on the initialization of commands which are
common to many different instruments. The command set of SICS can be
tailored to cover many specific needs. Moreover this system allows to
replace functionality by other implementations suited to another users
taste. This is a list of common command initialization commands.

MakeRuenBuffer

MakeRuenBuffer makes the RünBuffer system available.

MakeBatchManager [name]

Installs the new batch buffer management system. If no name is given,
the default will be exe.

MakeDrive

MakeDrive creates the drive and run command.

Publish name access

The SICS server uses Tcl as its internal macro language. However, it was
felt that the whole Tcl command set should not be available to all users
from the command line without any protection. There are reasons for
this: careless use of Tcl may clog up memory, thereby grinding the
system to a halt. Invalid Tcl statements may cause the server to hang.
Last not least, Tcl contains commands to manipulate files and access the
operating system. This is a potential security problem when the server
is hacked. However, in order to make macro procedures available the
Publish command exists. It makes a Tcl command name available to SICS
users with the access code access. Valid values for access are:
Internal, Mugger, User and Spy.

TokenInit tokenpassword

This command initialises the token control management system with the
token command. The single parameter tokenpassword specifies the password
for the token force command.

MakeOptimise name countername

This command installs the Peak Optimiser into the SICS server. The Peak
Optimiser is an object which can locate the maximum of a peak with
respect to several variables. The arguments are: name, the name under
which the Peak Optimiser can be accessed within SICS and countername,
which is the name of an already configured SICS counter box.

MakeO2T nam OM 2TM

creates an omega 2Theta virtual motor with name nam for omega 2Theta
scans. OM defines an omega motor, 2TM a two theta motor.

MakeDataNumber SicsDataNumber filename

This command makes a variable SicsDataNumber available which holds the
current sequential data file number. filename is the complete path to
the file were this data number is stored. This file should never, ever
be edited without good reason, i.e. resetting the sequential number to 0
at the beginning of a year.

MakeXYTable myname

Creates a XYTable object with the name myname. This object can store a
list of x-y values.

MakeSinq

Install the listener module for the accelerator divisions broadcast
messages. This creates a command sinq.

MakeMaximize counter

Installs a command max into SICS which implements a more efficient
algorithm for locating the maximum of a peak then scanning and peak or
center.

MakeMaxDetector name

Installs name into SICS which implements a command for locating maxima
on a two dimensional histogram memory image.

MakeLin2Ang name motor

Creates a virtual motor name which translates an input angle into a
translation along a tangent to the rotation axis. The distance of the
translation table can be configured as variable: name length once this
is established.

MakeSWHPMotor realmotor switchscript mot1 mot2 mot3

Creates switched motors mot1, mot2 and mot3 for real motor realmotor.
For switching the script switchscript is used. This can be used when
several motors are operated through the same motor driver. This
implementation is not completely general now.

Monochromators
--------------

A monochromator is represented in SICS through a monochromator object
which holds all the parameters associated with the monochromator and
virtual motors which drive wavelength or energy. The commands to
configure such a monochromator are:

MakeMono name M1 M2 M3 M4

This command creates a crystal monochromator object. Such a
monochromator object is necessary for the usage of the wavelength or
energy variables. The parameter name defines the name of the
monochromator object in the system. M1 and M2 are the names of the Theta
and two Theta motors respectively. M3 is an optional parameter defining
a motor for driving the horizontal curvature. M4 is an optional
parameter defining a motor for driving the vertical curvature of the
monochromator.

MakeWaveLength nam mono

creates a wavelength variable nam. The monochromator mono is used for
adjustment.

MakeEnergy nam mono

creates a energy variable nam. The monochromator mono is used for
adjustment.

MakeOscillator name motor

Installs a module name which oscillates motor between the software
limits of the motor. This is useful for suppressing preferred
orientation effects on powder diffractometers.name then supports the
commands: start, stop, status which are self explanatory.

Reoccuring Tasks
----------------

Sometimes it may be necessary to execute a SICS command at regular time
intervalls. This can be achieved with the sicscron command:

sicscron intervall bla blab blab

This command installs a reoccuring task into SICS. The first parameter
is the intervall in seconds between calls to the SICS command.
Everything behind that is treated as the command to execute. The script
has to be designed such that it does not return errors. Errors in a cron
script cause the cron task to be stopped.

The SICS Online Help System
---------------------------

SICS has a simple built in help system. Help text is stored in simple
ASCII text files which are printed to the client on demand. The help
system can search for help files in several directories. Typically one
would want one directory with general SICS help files and another one
with instrument specific help files. If help is invoked without any
options, a default help file is printed. This file is supposed to
contain a directory of available help topics together with a brief
description. The normal usage is: help topicname . The help system will
then search for a file named topicname.txt in its help directories.

A SICS manager will need to configure this help system. A new directory
can be added to the list of directories to search with the command:

    help configure adddir dirname

The default help file can be specified with:

    help configure defaultfile filename 

Each of these command given without a parameter print the current
settings.

Aliases in SICS
---------------

SICS knows three different kinds of aliases: object aliases, runtime
aliases and command aliases. This is confusing but finds its explanation
in the structure of SICS internals.

### Object Aliases

An object alias is another name for a first class object installed into
the SICS interpreter. For instance a second name for a motor. For
instance the motor twotheta is quite often aliased to a4. Such an alias
can be used like a normal SICS objects. Even in commands which access
internal SICS interfaces like the drive command or others. Object
aliases are installed into SICS with the SICSAlias command:

SicsAlias oldname newname

This command installs newname as alias for the object oldname.

SicsAlias can only be used within initialization scripts. SicsAlias is
considered deprecated and can be replaced with the superior runtime
aliases described below.

### Runtime Aliases

Runtime aliases are full object aliases which can be configured into the
system at run time by a SICS manager. The syntax looks like this:

DefineAlias aliasname SICSobject

This defines aliasname to be the alias for the SICS object SICSobject.
It is not needed that SICSobject already exists. If SICSobject is
already an alias, it is translated before definition. Multiple
translation is possible, depending on the order of definition. When an
alias is used, and it does not point to an existing object, the
behaviour is the same as if an unknown object would have been used.

DefineAlias aliasname

This command deletes the alias aliasname.

### Command Aliases

Command aliases are shortcuts for lengthy commands. For instance one
might want to define A4LL as a shortcut for "a4 softlowerlim". This is
just to save typing or adapt SICS to MAD users who appear to have an
unlimited memory for 2-4 letter acronyms. It is possible to redefine a
SICS object with this for instance to define tt as an alias for
temperature. However if one tries to use tt in a drive command it will
fail because it is just a text replacement. A command alias can be
installed into SICS at any time with manager privilege and the command:

alias shortcut bla bla bla ....

This define shortcut as an alias for everything behind it.

A shortcut may take parameters.

The AntiCollision Module
------------------------

In some cases motors have to be drive in a coordinated way. For
instance, at TRICS, the chi motor may need to move first before omega
can be driven in order to avoid collisions. Or at the ECB instruments,
only one of eight motors in a rack can be driven at any given time. The
anti collision module now allows to implement this. Anticollisions
pattern of operation: Each collaborating motor is registered with the
anti collision module. When trying to start such a motor, the anti
collider just takes a note where it shoud go. On the first status check,
a program is called which has to arrange the running of the motors into
a sequence. This sequence then is executed by the anti collision module.
The program which arranges the motors into a sequence is a configurable
parameter and usually implemented as a script in SICS own marco
language. In the SICS initialization file this requires the commands:

AntiCollisionInstall

Creates the anitcollision module.

anticollision register motorname

Registers motorname with the anti collision module.

anticollision script scriptname

This command configures the script which is responsible for arranging
the sequence of operations.

The script configured into anticollision is called with pairs or motor
names and targets as parameters, Example:

    sans2rack mot1 target1 mot2 target2 .....

Within the anticollision script, the following command may be used in
order to define the sequence.

anticollision clear

Clears the sequence list

anticollision add level motor target

Add motor with target to level in the sequence.
