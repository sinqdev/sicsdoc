Hklscan and Hklscan2d (Obsolete)
================================

Hklscan is a command which allows to scan in reciprocal space expressed
as Miller indizes on a four circle diffractometer. Hklscan operates with
a single detector. Hklscan2d does the same as hklscan but for the
position sensitive detectors, saving data into NeXus files. Hklscan and
Hklscan2d share the same syntax. Prerequisite for this is the existence
of a scan object and the hkl-object for doing crystallographic
calculations. Make sure the properties of the hkl object (UB,
wavelength, NB) have some reasonable relation to reality, otherwise the
diffractometer may travel to nowhere. Also it is a good idea to drive
the diffractometer to the end points of the intended scan in reciprocal
space. hklscan will abort if the requested scan violates diffractometer
limits. The commands implemented are quite simple:

hklscan start fH fK fL

sets the start point for the HKL scan. Three values required, one for
each reciprocal axis.

hklscan step sH sK Sl

sets the step width in reciprocal space. Three values required, one for
each reciprocal axis.

hklscan run NP mode preset

executes the HKL scan. NP is the number of points to do, mode is the
counting mode and can be either timer or monitor and preset is the
preset value for the counter at each step.

hklscan2d sim NP mode preset

This command only for hklscan2d. It tries to calculate all points in the
hkl scan and complains if it cannot reached or stays silent. Use this to
test if your hklscan2d can be performed.

Data is written automatically into a slightly modified TOPSI data format
file for hklscan. The status display with topsistatus or scanstatus
might be slightly erratic as it uses two theta as x-axis. Hklscan2d
writes data into NeXus files.

hklscan is obsolete. As of 2005, h, k, l are normal virtual motors and
can be used with the SICS built in scan commands.
