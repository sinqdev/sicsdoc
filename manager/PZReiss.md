### Poldi Tensile Device

#### Initialization

1.  In the instrument control file, poldi.tcl, there is a flag near the
    top of the file which determines if the tensile device is configured
    or not. The flag is called zugmaschine and must read: set
    zugmaschine 1
2.  Restart SICS: monit restart sicsserver
3.  On pc2530:
    1.  Start the Tensile application
    2.  Hit the button labelled **Remote**
    3.  Hit the left arrow button at the top left corner to start the
        Labview application
    4.  Wait for a number to appear in the status filed besides the port
        number

4.  In a SICS command line client issue the command: kraft connect

#### Usage


All in all three objects will be initialized for the tensile device:

    kraft

as representation for the controller

    force

a virtual motor for the force applied

    forcepos

a virtual motor position of the tensile device.

The latter two can be operated upon like any normal motor. For instance
the commands:

    forcepos

will print the current position

    drive forcepos newvalue

will drive the position to a new value.

The force virtual motor handles in a similar way. Please note, that both
motors cannot be interrupted as of May 2007.

The kraft object supports the following commands:

    kraft connect

tries to connect to the tensile device

    kraft velocity [val]

queries or sets the velocity used by the tensile device

    kraft mode [val]

queries or sets the current mode. Possible values are: force for
operating on force and pos for position.

   kraft

prints the current value, depending on the mode.

The two virtual motors **force** and **forcepos** are wrappers around
kraft which switch mode and act accordingly.
