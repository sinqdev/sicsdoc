### XY Tables

This module provides for a table in which pairs of X and Y values can be 
stored for later saving or processing. This is mostly used at SANS for adhoc 
scans. 

The command set:

    MakeXYTable tablename

creates a new xy table under the name of tablename. The created object, 
tablename, understands further commands. In this documentation 
tablename will be used as the placeholder for the actual object name. 

    tablename clear

Removes all entries from the xy table.

    tablename add x y

Appends a new entry with values x and y to the xy table.

    tablename write filename

Writes the content of the xy table to file filename.

    tablename uuget

Prints  the table in a UU encoded form. This is for graphical clients. The 
format is: first int = length of the table. Then pairs of floating point 
values in fixed point format i.e. multiplied with 65536.  

    tablename list

Lists the current content of the table on the screen. 