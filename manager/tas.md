### Triple Axis Spectrometer Specific Commands

One aim for the implementation of the triple axis spectrometer in SICS
was to implement as closely as possible the command set of the ILL
program MAD. For this, there are two implementations: an older one where
most things werde done in C-code. And a newer one which implements a
relaxter MAD emulation. The problem with the MAD emulation is that MAD
relies on the order of variables and motors in memory. The newer MAD
emulation obeys this only for the qh, qk, ql and en variables. This
section describes the newer more portable TAS setup. There are some
C-modules and a lots of scripts which implement the MAD emulation.

The TAS requires the following initializations in its instrument file:

    MakeTasUB tasub

Installs the TAS crystallographic calculation module into SICS. It will
have the name tasub (recommended).

    MakeTasUB tasub a1 a2 mcv mch a3 a4 sgu sgl a5 a6 acv ach

Installs the TAS crystallographic calculation module into SICS. It will
have the name tasub (recommended). This versions allows to specifiy
motor names for functions. If there is no motor for a function it can be
replaced with a placeholder in the parameter list, like dummy. This is
only allowed for the curvature motors. The motor functions:

a1

monochormator rotation

a2

monochromator two theta

mcv

monochromator vertical curvature

mch

monochromator horizontal curvature

a3

sample rotation

a4

sample tow theta

sgu

sample tilt

sgl

second sample tilt

a5

analyzer rotation

a6

analyzer two theta

acv

analyzer vertical curvature

ach

analyzer horizontal curvature

MakeTasScan iscan tasub

Installs the module with the TAS specific scan functions into SICS. The
TAS implements its own data format resembling the ILL TAS data format.

Moreover it is required to add the peak center module, drive, exe and a
lot of variables: polfile, alf1-4, bet1-4, ETAM ETAS ETAA.

The scripts for the MAD emulation live in the file tasubcom.tcl. This
file will need little editing in order to cope with another triple axis
machine, just a couple of path variables will need to be changed.
