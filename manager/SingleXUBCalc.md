#### UB Calculation

This module can do UB matrix calculations for four circle
diffractometers in bisecting mode.

    ubcalcint ub2ref id id2

caclulates a new UB from 2 reflection, defined by id1 and id2, and the
cell.

    ubcalcint ub3ref id id2 id3

caclulates a new UB from 3 reflection defined by id1, id2 and id3.

    ubcalcint list

prints the new UB

    ubcalcint activate

make the new UB the active UB

    ubcalcint cellub

calculates the cell constants from the UB matrix

    ubcalcint index [stt]

suggest a matching index based on two theta and the cell constants. If
no two theta is given, the value of the current two theta motor is used.

    ubcalcint difftheta [val]

set or retrieve the maximum allowed two theta difference for suggesting
a reflection in index.

    ubcalcint maxindex [val]

sets or retrieves the maximum index value considered for searching
indices. Index just searches the reflection space from hkl max t hkl
-max. With max beining the maxindex.

    ubcalcint maxlist [val]

sets or gets the maximum number of suggestions for index.

    ubcalcint fromcell

Calculates a prvisional UB matrix from the cell constants in singlex

Index is a very primitive indexing tool. Starting from -max,-max,-max to
+max, +max, +max for h,k , l it calculates the theoretical two theta
value for each reflection. This list is then sorted according to the
difference to the measured two theta. Maxlist values of this list are
then printed out. For small molecule work this helps a lot for assigning
indices to reflections. Usually the two theta differences are pretty
large in order to allow for a good separation. But you can go wrong if
your two theta zero point is very wrong.
