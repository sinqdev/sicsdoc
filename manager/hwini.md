### SICS Old Hardware Configuration

This documentation is for initializing SICS old generation objects. See 
[SICS Second Generation Initialisation](secgenini.md) for initializing new style 
SICS objects. 

Hardware is configured into the SICS system by executing special
hardware configuration commands from the server initialisation file.
These commands are described here. Much SICS hardware is hooked up to
the system via RS-232 interfaces. The SICS server communicates with such
devices through a serial port server program running on the instrument
computer. All such devices require on initialisation the following
parameters:

-   **hostname** The name of the instrument computer.
-   **port** The port number where the serial port server program is
    listening for requests. It is usually 4000.
-   **channel** The number of the RS-232 interface port on the terminal
    server.

#### Bus Access

SICS and its internals cover many common usage cases. However there are
always situations where SICS has to be bypassed and commands to be sent
to a controller directly. This can happen in order to satisfy a special
request or as first step in the integration of a special piece of
hardware into SICS. In order to do such things the following facilities
are available:

##### Direct Access to RS232 Controllers or TCP/IP Controllers.

Both controllers listening on a TCP/IP port or RS-232 devices connected
to a terminal server can be directly accessed through the RS232
commands. The first step in using this system is always to accounce the
controller to SICS using the command:

    MakeRS232Controller name terminalserver port

in the SICS initialization file. For example:

    MakeRS232Controller hugo psts213 3004

name is the SICS name for the controller, terminalserver is the name of
the terminal server the device is connected to and port is the port
number at which the terminal server publishes the RS232 channel to which
the device is connected. This is usally the port number plus 3000.

Now various commands are available for interfacing with the RS232
controller. In the following description the SICS name of the controller
is replaced by the symbol rs232name.

    rs232name sendterminator

prints the current terminator used when sending data to the device as
hexadecimal numbers.

    rs232name sendterminator h1h2..hn

sets the current terminator used when sending data to the device to the
characters described by the hexadecimal numbers h1 to hn. The numbers
are in the format 0xval, where val is the hex number.

    rs232name replyterminator

prints the current terminator expected to terminate a response from the
device as a hexadecimal number.

    rs232name replyterminator h1h2..hn

sets the current terminator expected to terminate a response from the
device to the characters described by the hexadecimal numbers h1 to hn.
The numbers are in the format 0xval, where val is the hex number.

    rs232name timeout

prints the current timeout when waiting for a reponse from the device.

    rs232name timeout val

sets the timeout for waiting for responses from the device. The value is
in microseconds.

    rs232name send data data data

sends the remainder of the line to the RS232 device and waits for a
response terminated with the proper reply terminator specified. This
commands waits at maximum timeout microseconds for a response. If a
valid response is obtained it is printed, otherwise an error message
occurs.

    rs232name write data data data

writes the remainder of the line after write to the device without
waiting for a response.

    rs232 available

checks if data is pending to be read from the device.

    rs232 read

reads data from the device.

##### Accessing Serial Ports (Old System)

In addition to the system describe above there is another system for
accessing serial ports which uses the SerPortServer program. The use of
the system is deprecated, new software should use the commands describe
above. Nevertheless the older sytem is described here for reference.

Serial port access is implemented as an extension to the Tcl macro
language. Essentially this is the same implementation as used in the
program psish. This section describes how to use serial port access.
Several steps have to be performed:

1.  Install the serialport command into the SICS server. This requires
    two lines to be added to the server startup script:
    -   SerialInit
    -   TclPublish serialport UserRights

    Where UserRights stands for one of the possible SICS user rights.
    See documentation for TclPublish above.
2.  Each separate serial port will be represented by a name in the SICS
    server after it has been initialized. This name is also a command.
    These port names live in the Tcl interpreter and must be made
    accessible with TclPublish. For example for a port named p1 include
    this line in the server startup script:
    -   TclPublish p1 User

    Replace User with the correct access code you want for a serial
    port. It is recommended to restrict serial port access to SICS
    managers only.
3.  After starting the SICS server the command serialport is now
    available.
4.  Now a serial port can be initialized with a command like this:
    -   serialport name1 SerPortServer.host port channel force
    -   Example: serialport p1 localhost 4000 5

    Such a command creates the command name1 and links it with serial
    port channel channel on the instrument computer (localhost) running
    the SerPortServer program . Port is the port number on which the
    SerPortServer is listening for connections (usually 4000). The last
    flag force is optional. If something is there, the connection to
    that port is done on a separate socket of its own. This has to do
    with some feature of the software interface to the SerPortServer
    serial port server. This software interface tries to direct messages
    for multiple channels through one socket connection between the host
    and the Macintosh server. This is perfectly fine as long as none of
    the commands through this socket takes a long time to execute.
    However, if a RS-232 device takes a long time to respond, the whole
    socket is blocked. Fortunately, the serial port server runs a
    separate thread of execution for each different socket. By forcing a
    new socket it can be achieved that such a slow device is decoupled
    from the rest. Exactly this is achieved with the force flag.
5.  Once the port has been initialised (for example p1) it is ready to
    operate. The port object allows to send data to the serial port and
    receive data from it. Furthermore some configuration is possible.
    The syntax is like this:
    portname -tmo number
    Sets the timeout for the serial port. This is the maximum amount of
    time the serial port server waits for data to arrive from the RS-232
    device. Increase this if a lot of `_BAD_TMO` error messages creep
    up.
    portname -sendterm string
    Sets the terminator which will be automatically added to the string
    which is sent. Some RS-232 devices require special terminators in
    order to accept a command. The serial port implementation ensures
    that such a terminator is sent after each message. This command
    allows to configure this terminator. Please note, that the
    terminator string needs to be enclosed in quotes. An example:
    -   `p1 -sendterm "\r\n"`

    This sets the terminator to carriage return - line feed.
    portname -replyterm string.
    The serial port server expects the RS-232 device to send a
    terminator when it is done with sending answers. It even supports
    multiple lines to be sent as a reply. This expected reply terminator
    is set with this command. The string may may be four characters
    long. An example: `1\r\n` sets the expected terminator to one of
    `\r\n`. One of them is expected. Thus the first character is the
    count of terminators to expect, followed by the characters possible
    as terminators. This string must usually be quoted.
    portname blablalakjdl
    When none of the options -tmo, -replyterm, -sendterm, is found
    everything after portname is sent to the RS-232 device. The reply
    from the RS-232 device is printed.

The defaults set for the configuration parameters of the serial port
connection are suited for the EL734, EL737 and ITC4 devices usually
encountered at SINQ. For other RS-232 devices consult the manuals
hopefully delivered with the device. The defaults are: 100 for timeout,
`1\r\n` for the reply terminator and `\r\n`for the send terminator.

##### GPIB Controller Access

GPIB is yet another bus system. Up to 30 devices can share the bus and
transfer data on it. SICS likest to speak to GPIB devices through the
National Instrument ENET-100 TCP/IP bridge. In order for this to work
the National Instruments driver software must have been installed on the
computer running SICS. SICS has to be compiled with the define HAVENI
defined and the proper paths to the header file and library configured.
Then an GPIB controller can be installed into SICS with the command:

    MakeGPIB name drivertype

Name is the name under which the GPIB controller is addressable within
SICS afterwards. drivertype is the driver to use for the GPIB device.
Supported values are:

* **sim** Simulation
* **ni** National instruments driver, see above.

The GPIB controller supports a couple of commands for communicating with
devices on the GPIB bus directly. Use with extra care because it is very
easy to lock things up on the GPIB bus. In the following documentation
of the command set it is assumed that a GPIB controller has been
configured into the system under the name **gpib**. Please note, that
managers privilege is required in order to be allowed to wrestle with
this controller.

    gpib attach controller-no gpib-address gpib-secondary timeout eos eot

This attaches the GPIB controller to a certain device at a certain
address for later communication. The return value is an integer handle
which will be used later on a s a handle devID when referring to the
connection. The parameters are:

**controller-no**

The number of the GPIB controller on the computer. There may be more
then one GPIB controllerinstalled on a given system. Usually this is 0.

**gpib-address**

The GPIB address of the device on the bus.

**gpib-secondary**

GPIB devices may have a seconadry address. This can be specified with
this parameter. Usually this is 0.

**timeout**

The time to wait for answers on the GPIB bus. 13 is 10 seconds and
ussually a good value.

**eot**

A parameter determining the termination mode on this connection. Consult
NI documentation for this or leave at 0.

**eoi**

A terminator. Set to 1 or understand NI documentation for this
parameter.

    gpib detach devID

Breaks the connection described through devID. devID is the return value
from attach.

    gpib clear devID

Tries to clear the GPIB buffers for the conenction described through
devID. Usually in vain.

    gpib send devID bal bla bla

sends data to the device at devID.

    gpib sendwithterm devID string terminator

Sends string to the device at devID. The terminator character identified
through the integer terminator is automatically appended. Use this to
send things which require a terminator. Terminators included in strings
sent by send get messed up through Tcl!

    gpib read devID

Reads data from the device at devID and returns it as a string.

    gpib readtillterm devID terminator

Read from teh device devID unti the terminator character described
through the interger terminator is read. Then return the data read as a
string.

#### Controllers

For the same reason as stated above, it is necessary to represent
controllers within SICS. Controllers implement more then only device
access but also maintain common state or implement special behaviour.

##### ECB Controllers

ECB controllers are at the heart of the Risoe data aquisition system.
These are essentially Z80 processors wired to the GPIB bus. Functions
can be invoked in this processor by sending a function code followed by
the contents of 4 8 bit registers. As a result the contents of the
registers after the function call are returned. A ECB can be made knwon
to SICS through the initialisation command:

    MakeECB name gpib-controller gbib-controller-number gpib-address

The parameters:

**name**

The name used as a token for this controller later on.

**gpib-controller**

the name of the GPIB interface to use. See above.

**gbib-controller-no**

The number of the GPIB board in the system

**gpib-address**

The GPIB address of the ECB on the GPIB bus.

Once installed, the ECB controller understands a few commands:

    ecb1 func funcode d e bc

Invoke ECB function funcode with the registers d e b c.Returns the
contents of the registers d e b c. Function codes and register contents
are documented, if at all, in the ECB documentation. In fact, as ECB
documentation is not available, the only documentation on ECB is the
source code of tascom.

    ecb1 clear

Tries, usually in vain, to clear the communications interface to the
ECB.

    ecb1 toint char

A helper function which converts the character char to an integer. Tcl
does not seem to be able to do that.

##### Siematic SPS Controllers

Siematic SPS controllers are used at SinQ for handling all the things
which fit nowhere else. Such as operating air cushions on some
instruments, reading variables from ADC's, reading status of shutters or
other parts of the instrument and the like. Those SPS units have an
RS-232 connector and understand a simple ASCII command protocoll. The
Siematic SPS and its command protocoll are special to PSI and this
section is probably of no interest to SICS managers outside. The SPS
basiaclly support three operations:

-   Push a button (Set a Digital I/O Bit).
-   Read a status of instrument status packed into a bit (Read Digital
    I/O) .
-   Read an ADC.

This is so user unfriendly that the usage of the SPS will mostly be
packaged into Tcl-macros.

A SPS unit can be configured into the SICS server with the command:\
 **MakeSPS name macintosh port channel** \
 The parameters are: the name of the SPS in SICS, the serial port server
computer, the port where the serial port server is listening and the
channel number of the SPS unit at the serial port server computer. An
example: \
 MakeSPS sps1 lnsp25.psi.ch 4000 6 \
 configures a SPS unit at lnsp25.psi.ch at channel 5. The serial port
server is listening at port number 4000. The SPS unit will be accessible
as sps1 in SICS.

After configuartion the following four commands are understood by name,
shown with sps1 as example:

    sps1 push byte bit

Corresponds to pushing the button mapped to the bit bit in the byte
byte.

    sps1 adc num

Reads the value in the ADC num. num can be between 0 to 7 for a maximum
of eight ADC's. Please note, that the values read are raw values which
usually must be converted in some way to physically meaningful values.

    sps1 status bit

Reads the status of the bit bit. bit can be in the range 0 - 128.

    sps1 stat2 byte bit

Reads the status bit bit in status byte byte. Is equivalent to status,
but adds some syntatctic sugar.

For all conversion factors, for all mappings of bytes and bits, consult
the electronician who coded the SPS.

##### General Controller Object and Choppers

Chopper systems are handled via a generic controller object. This
basicly consists of two components: One object represents the actual
controller. This basic object allows to query parameters only. Then
there is for each parameter which can be controlled from SICS in this
controller an adapter object. These adapter object are virtual motors
which can be driven with the normal run or drive commands. Currently two
drivers for this scheme exists: one for a simulated device, the other
for the Dornier Chopper Controller at FOCUS. The first step when
initializing this system is the installation of the general controller
object into SICS. This is done with the commands:

    MakeChopper name sim
    MakeChopper name docho mac port channel

The first command simply installs a simulated controller. The second
command install a controller with a driver for the FOCUS Dornier Chopper
system. Mac, port and channel are the usual Macintosh terminal server
parameters which describe where the chopper controller is connected to
through its RS-232 interface. After both commands the controller is
available as command name within SICS.

A drivable parameter at this controller is installed with a command
similar to this:

    ChopperAdapter vname cname pname lower upper

vname is the name under which the virtual motor will appear in SICS.
cname is the name of the controller object installed into SICS with the
commands in the previous paragraph. pname is the name of the drivable
parameter in the controller. upper and lower are the upper and lower
limits for this parameter. More then one of these commands can be given
for each general controller.

After this, the parameter can be modified by a command like:

    drive vname newvalue

#### Motors

The following commands are available to install motors into the system:

    Motor name SIM lowlim uplim err speed

This command creates a simulated motor with the lower limits lowlim, the
upper limit uplim, an ratio of randomly generated errors err and a
driving speed of speed. Use this for testing and instrument simulation.
If err is less then 0, the motor will not create failures and thus can
be used in a instrument simulation server.

    Motor name EL734 host port chan no

This command creates a stepper motor named name which is controlled
through a El734 motor controller. The parameters host, port, chan have
the meanings defined above. no is the number of the motor in the EL734
motor controller.

    Motor name EL734DC host port chan no

This command creates an analog motor named name which is controlled
through a El734DC motor controller. The parameters host, port, chan have
the meanings defined above. no is the number of the motor in the EL734DC
motor controller.

    Motor name el734hp rs232controllername motornum

Creates a motor object name using the newer motor drivers which access
the motor controller directly, without the serial port server.
rs232controllername is the name of a connection to the motor controll
which has been set up with MakeRS232, above. Motornum is the number of
the motor in the controller.

    MakePIMotor name c804 pararray

Creates a motor name connected to a C804 motor controller from the
manufacturer Physik Instrumente. Pararray is a Tcl array holding the
initialization information. The follwoing elements are required in this
array:

**Computer, port, channel**

The standard connection parameters.

**upperlimit, lowerlimit**

The limits for this motor.

**motor**

The number of the motor in the motor controller.

    Motor name pipiezo pararray

Creates a piezo electric positioning device. Again the controller is a
Physik Instrumente controller. pararray has the same meaning as for the
C804 controller given above.

    Motor name ecb ecbcontroller ecb-number lowerlimit upperlimit

This creates a motor which is controlled through the Risoe ECB
electronic. The parameters:

ecbcontroller

The ECB controller to which this motor is connected to. See below for
more on ECB controllers.

ecb-number

Number of the motor in the ECB system.

lowerlimit

The lower hardware limit for this motors operation

upperlimit

The upper hardware limit for this motors operation

In contrast to normal motors, the ECB motors have quite a number of
hardware parameters which must be configured. The general syntax to
configure them is: motorname parametername value. The following
parameters are available:

encoder

0 if there is no encoder for this motor, 1-3 for the encoder used for
this motor.

control

The control bit flag. This falg determines if the motor sets a control
bit in the ECB controller. This control bit can be used to drive air
cushions and the like. If required set to 1, else leave at 0.

delay

Delay time to wait after setting a control bit.

range

The speed range for the motor: 0 for slow, 1 for fast

multi

The ECB controller supports up to 24 motors. In some instances this is
not enough. Then one ECB channel can be multiplexed into several motors.
This flag (),1) determines if this is the case.

multchan

The multiplexer channel for a multiplexed motor.

port

The ECB port a multiplexed motor is using.

acceleration

The speed with which the motor accelerates to its final speed.

rotation\_dir

Rotation direction of the motor.

startspeed

Starting speed of the motor.

maxspeed

The maximum speed for this motor.

auto

Speed in automatic mode

manuell

Speed used when driving the motor through the manual control box.

offset

When using an encoder: the offset between the motor zero and the encoder
zero.

dtolerance

hardware tolerance of the motor.

step2dig

conversion factor from encoder steps to physical values.

step2deg

Conversion factor from motor pseudo encoder steps to physical values.

backlash

In order to correct for backlash, Risoe motors always approach a target
position from the same direction. In order to do this the motor has to
overshoot and drive back when driving in the wrong direction. The
parameter backlash determines how much to overshoot.

ECB motors have another quirck: 8 motors in a rack share a power supply!
This has the consequence that only one of the 8 motors can run at any
given time. In SICS this is directed through the anticollider module
described elsewhere.

#### Counting Devices

    MakeCounter name SIM failrate

This command creates a simulated single counter accessible as object
name. Failrate is the per centage of invocations at which the counter
will generate a random failure for testing error treatment code. If
failrate is less then 0, there are no failures. This can be used in a
instrument simulation server.

    MakeCounter name mcstas

Creates a counter which interoperates with a [McStas](mcstas.htm)
simulation. Please note, that the McStas module mccontrol must have been
initialized before this initialization can work.

    MakeCounter name EL737 host port chan

This command creates a single counter name, using an EL737 driver. The
counter is at host host, listening at port port and sits at serial port
chan.

    MakeCounter name EL737hp terminalserver port

Creates a counter object name which uses the faster driver which
connects directly to the terminal server without the serial port server
program. Terminalserver is the name of the instruments terminal server.
Port is the port number at which the terminal server publishes the port
at which the EL737 controller is connected. Usually this 3000 + port
number.

    MakeCounter name ecb ecb-controller

Installs a counetr on top of the Risoe ECB hardware. The only parameter
is the name of the ECB controller to use.

    MakeHMControl name counter hm1 hm2 hm3

At some instruments (for instance TRICS) multiple counters or histogram
memories are controlled by a master counter which watches over presets
and the like. This command installs a virtual counter which does exactly
that. The parameters are:

name

The name of the virtual counter in SICS

counter The name of the master counter

hm1, hm2, hm3

Up to three slave counting devices.

#### Histogram Memory

Due to the large amount of parameters, histogram memories are configured
differently. A histogram memory object is created using a special
creation command. This command is described below. Then a lot of options
need to be configured. The commands used for setting these options and
their meanings are defined in the [user](../user/histogram.htm)
documentation because histogram memories may be reconfigured at runtime.
The sequence of configuartion options is ended with the command hmname
init. This last command actually initialises the HM. Histogram memory
objects can be created using the command:

    MakeHM name type

The parameter name specifies the name under which the HM will be
avialable in the system. type specifies which type of driver to use.
Currently these drivers are supported:

**SIM**

for a simulated HM

**SINQHM**

for the SINQ histogram memory

**tdc**

for the Risoe histogram memory.

**mcstas**

for the integration with the [McStas](mcstas.htm) simulation.

Please care to note, that the SINQHM requires a counter box for count
control. This counter must have been defined before creating the HM
object. As an example the configuration of a SINQHM HM with the name
banana will be shown:

    MakeHM banana SINQHM         
    banana configure HistMode Normal
    banana configure OverFlowMode    Ceil
    banana configure Rank     1
    banana configure dim0   400
    banana configure BinWidth 4
    banana preset 100.
    banana CountMode Timer
    banana configure HMComputer psds04.psi.ch
    banana configure HMPort 2400
    banana configure Counter counter
    banana init

#### Velocity Selectors

A velocity selector is configured in a three step process. First a Tcl
array is filled with the necessary configuration options for the actual
velocity selector driver. In a second step the velocity selector is
created with a special command. In a third step the forbidden regions
for the velocity selector are defined. Currently two drivers for
velocity selctors are known: a SIM driver for a simulated velocity
selector and a DORNIER driver for a Dornier velocity selector hooked to
a SINQ serial port setup. The last one needs a parameter array
containing the fields Host, Port, Channel and Timeout. Host, Port and
Channel have the meanings as defined at the very top of this section.
Timeout is the maximum time to wait for responses from the velocity
selector. A large value is required as the dornier velocity selector is
very slow. The second step is performed through the following commands:

    VelocitySelector name tilt-motor SIM

This command installs a simulated velocity selector with the name name
into the system. tilt-motor is used for driving the tilt angle of the
selector. tilt-motor must exist before this command can be executed
successfully.

    VelocitySelector name tilt-motor DORNIER arrayname

This command installs a dornier velocity selector into the system. name
and tilt-motor have the same meanings as described above. arrayname is
the Tcl-array with the driver configuration parameters.

As an example the configuration of a dornier velocity selector named nvs
is shown:

    set dornen(Host) lnsp25.psi.ch
    set dornen(Port) 4000
    set dornen(Channel) 6
    set dornen(Timeout) 5000
    VelocitySelector nvs tilt DORNIER dornen
    nvs add -20 28800
    nvs add 3800 4500
    nvs add 5900 6700
    nvs add 8100 9600
