### TASUB: The Triple Axis Calculation Module

On a triple axis instrument the parameters incoming energy, Q-position
in 3D and analyzed energy have to be changed frequently. These
calculations are the task of the TASUB module. This module uses the
calculus described by M. Lumsden, J. L. Roberston and M. Yethiraj in J.
Appl. Cryst. (2005), 38, 405-411. The special feauture of this algorithm
is that the tilt cradles of the sample table are used to help during
alignment and in order to drive out of plane (within the limits of the
tilt cradles). For alignment, two reflections must be located and their
angles and Q-E parameters entered into the module. Then a UB matrix can
be calculated. With a UB matrix, the Q-E variables ei, ki, ef, kf, en,
qh, qk and ql can be driven as virtual motors in SICS.

#### Commands understood by Tasub

##### Monochromator and Analyzer Parameters

Incident and scattered energies are defined by monochromator crystals.
In order for the calculations to work, some parameters need to be
configured. Monochromator and analyzer parameters can be accessed with
the prefixes:

-   tasub mono
-   tasub ana

The parameter syntax used is as usual: giving only the parameter name
queries the value, giving the parameter plus a value sets the parameter
to the new value. The following parameters are supported:

* **dd** The d-spacing of the reflection used
* **ss** The scattering sense, 1 or -1 are possible.
* **hb1, hb2** Parameters for calculating the horizontal curvature
* **vb1, vb2 ** parameters for calculating the veritical curvature 

An example:

    tasub mono dd

will print the current d-spacing of the monochromator

    tasub mono dd 4.3

Will set the d-spacing of the monochromator to 4.3

##### Cell Parameters

In order for the UB matrix calculation to work, the cell constants must
be known:

    tasub cell

This command prints the current cell parameters.

    tasub cell a b c alpha beta gamma

This command sets the new cell parameters. All six values must be given.

##### Reflection Management

In order to calculate a UB matrix a list of reflections must be
maintained. This is done with the commands in this section:

    tasub clear

Clears the reflection list

    tasub listref

Prints a list of all known reflections.

    tasub del num

Delete the reflection number num from the list

    tasub addref qh qk ql

Adds a reflection to the list. The indices of the reflections are given.
The angles and energy values are read from the motors. Use this command
only when the instrument is positioned right on a reflection.

    tasub addref qh qk ql a3 a4 sgu sgl ei ef

Add a new reflection to the list. Besides the indices all angles are
given: a3, the sample rotation, a4, sample two theta, sgu, upper tilt
cradle, sgl, lower tilt cradle and incoming energey ei and outgoing
energy ef.

    tasub addauxref qh qk ql

Adds an auxiliary reflection with indices qh, qk, ql to the list. A4 is
calculated from cell constants. A3 is either left alone or is calculated
to have the correct angular difference to a previous reflection. This is
a help for setting up the instrument or running powder mode. When a UB
has been generated from auxiliary reflections, a3, sgu and sgl angles
will be incorrect.

    tasub repref id qh qk ql a3 a4 sgu sgl ei ef

Modifies the reflection with id id to have the values given.

##### Calculations

This section covers the parameters and commands to use to make the
module do calculations for you.

     tasbub const ki | kf | elastic

Sets a parameter to determine if KI or KF is fixed when the energy
transfer en is being driven. Allowed values: ki, kf, elastic. In elastic
mode the analyzer is disregarded. This is useful for two circle
diffractometers.

     tasub const

Prints if ki or kf is fixed.

    tasub ss

Prints the sample scattering sense.

    tasub ss 1 | -1

Sets the sample scattering sense. Allowed values are either 1 or -1.

    tasub silent 0 | 1

Prints or sets the silent flag. If this is 0, the messages Driving motor
.. from .. to .. are suppressed.

    tasub outofplane 0 | 1

Prints or sets the outofplane flag. If this flag is 0, the instrument
will stay in the scattering plane and not move out of it. This is here
in order to protect those bloody magnets which cannot be tilted.

    tasub makeub r1 r2

Calculate a new UB matrix from the current cell constants and the
entries r1 and r2 in the reflection list. r1 and r2 are integer numbers.
This command will not only print the new UB matrix but also the results
of various back and forth calculations performed with the new UB matrix.
This can be inspected in order to check the new UB. WARNING: The
calculation will go wrong if the scattering sense at the sample has
changed since the reflections used for the UB matrix determination have
been entered.

    tasub listub

prints the current UB matrix.

    tasub calcang qh qk ql ei ef

Will calculate new angles for the Q-E position specified. The angles
will be printed in the order: monochromator two theta, sample rotation,
sample two theta, lower tilt cradle, upper tilt cradle and analyzer two
theta.

    tasub calcqe a2 a3 a4 sgu sgl a6

Calculates and prints the Q-E position from the angles given: a2 =
monochromator two theta, a3 = sample rotation, a4 = sample tow theta,
sgu = upper tilt cradle, sgl = lower tilt cradle and a6 = analyzer two
theta. The Q-E position is printed in the sequence: qh, qk, ql, ei, ef.

##### Virtual Motors

The tasub module also installs the following virtual motors into SICS:
ei, ki, qh, qk, ql, en, ef, kf and qm. All these motors can be used in
SICS drive, run or scan commands like real motors. Driving them triggers
a recalculation of angles and the drives the real motors to appropriate
values. The virtual motors have a very limited command set (shown at the
example of qh):

    qh

The name of the motor alone will print its current position.

    qh target

This will print the last requested target position for this virtual
motor.

The virtual motor qm implements **powder mode**. In this mode, only the
sample two theta and energy motors will be driven, sample rotation and
tilt cradles will be left at their respective positions. THis is
commonly used to analyze the energy transfer of powder samples.

There are other important command:

    tasub update

This command will force a recalculation of the current Q-E position for
the virtual motors from angles. Normally tasub will take care of this.
However, if any of the angle motors are moved directly or manualy, this
command might be required. The SICS dr wrapper command, however, even
takes care of this.

    tasub updatetargets

This command makes the QE targets macth the current position. This is
useful after initialization in the instrument.tcl file.

##### Internal Commands

The tasub module supports some more commands which are used by SICS in
order to restore the tasub configuration between instantiations of SICS.
These commands are documented here for the sake of completeness:

    tasub setub ub11 ub12 ub13 ub21 ub22 ub23 ub31 ub32 ub33

Sets the UB matrix. Nine values are required.

    tasub setnormal n1 n2 n3

This command sets the plane normal which is required in calculations.
Normally this plane normal is automatically generated during the
calculation of the UB matrix.

    tasub settarget qh qk ql qm ki kf

Sets the Q-E target.

    tasub r1 qh qk ql a3 a4 sgu sgl ki kf

    tasub r2 qh qkl ql a3 a4 sgu sgl ki kf

These commands set the values for the two reflections used for
generating the UB matrix.
