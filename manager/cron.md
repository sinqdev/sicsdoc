### Reoccuring Tasks

Sometimes it may be necessary to execute a SICS command at regular time
intervalls. This can be achieved with the sicscron command:

    sicscron intervall bla blab blab

This command installs a reoccuring task into SICS. The first parameter
is the intervall in seconds between calls to the SICS command.
Everything behind that is treated as the command to execute.

    sicscron list

List the content of the sicscron list. 

    sicscron stop scriptname

Stops the script specified as scriptname to be called from sicscron

    dolater intervall bla bla bla 

This command installs a task to run once in SICS. The first parameter
is the intervall in seconds until the command is to be run.
Everything behind that is treated as the command to execute.

