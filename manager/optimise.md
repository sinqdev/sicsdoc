#### The Old Peak Optimiser

This module is superseded by the faster maximize command. But some 
instruments still use the optimiser...

In instrument control the need may arise to optimise a peak with respect
to several variables. Optimising means finding the maximum of the peak
with respect to several variables. This is useful during instrument
calibration, for example. Four circle diffractometers use this facility
on a day to day basis for finding and verifying the exact position of
reflections. In order to support both usages a more general module has
been implemented. The algorithm is like this:

    while errors gt precision and cycles lt maxcycles
      for all variables 
          do a scan
          Try find the maximum, two halfwidth points and the peak center.
          if failure extend the scan.
          if success shift the variable, remember last shift. 
          If shift lt precicison mark this variable as done
       end for
    end while

Possible outcomes of this procedure are: success, the peak was lost or
the maximum number of cycles was reached. This routine requires that the
instrument is currently placed somewhere on the peak and not miles away.

The peak optimiser supports another optimisation algorithm which is
faster but may not be as accurate. This is hill climbing:

    while errors gt precision and cycles lt maxcycles
      for all variables 
          find the direction into which the intensity rises
          step into this direction until the intensity drops
       end for
    end while

The Peak Optimiser is implemented as an object with the name opti. It
understand the following commands:

    opti clear

clears the optimiser.

    opti addvar name step nStep precision

This command adds a variable to optimise to the optimiser. The user has
to specify the name of the variable, the step width to use for scanning,
the number of steps needed to cover the full peak when scanning and the
precision which should be achieved when optimising the peak. The step
width and number of steps parameters should cover the whole peak.
However, the Optimiser will extend the scan is the specified range is
not sufficient.

    opti run

Starts the optimiser. It will then optimise the peak. This may take some
time as it uses a time consuming scan based algorithm.

    opti climb

Starts the optimiser in hill climbing mode. Hill climbing is faster but
may not be as accurate as a scan based optimization.

The behaviour of the optimiser can be configured by modifying some
parameters. The synatx is easy: **opti parameter** prints the value of
the parameter, **opti parameter newval** sets a new value for the
parameter. The following parameters are supported:

maxcycles

The maximum number of cycles the optimiser will run when trying to
optimise a peak. The default is 7.

threshold

When a peak cannot be identified after a scan on a variable, the
optimiser will check if there is a peak at all. In order to do that it
searches for a count rate higher then the threshold parameter. If such a
rate cannot be found the optimiser will abort and complain that he lost
the peak.

channel

The counter channel to use for scanning. The default is to use the
counter. By modifying this parameter, the optimiser can optimise on a
monitor instead.

countmode

The counting mode to use when scanning. Possible values are **timer** or
**monitor**.

preset

The preset value to use for counting in the scan. Depending on the
status of the countmode parameter this is either a preset time or a
preset monitor.

It is the users reponsability to provide meaningful step widths. Usually
this is dependent on the instrument resolution and thus fairly constant.
Also these optimisation algorithms will fail if the instrument is not
positioned at the flank of a peak. Probaly the best will be to do
several cycles of hill climbing first, followed by one cycle of scan
optimisation for extra accuracy.
