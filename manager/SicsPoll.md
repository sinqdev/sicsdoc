### SICS Polling

This is a generic polling module for SICS. In its current implementation it can 
update Hipadaba nodes at regular time intervalls and it can run polling scripts 
at regular intervalls. It can be expanded to support other poll techniques through 
polldrivers. In SICS the system is represented by the command **sicspoll**. 


#### Installation

In order to be polled objects need to be registered with sicspoll. 

    sicspoll add name drivertype driverpar .....

The syntax is sicspoll, the keyword add, a name for the sicspoll entry, a delector which defines the 
poll driver and then driver specific parameters. 

As of 2016 there are two poll drivers: **hdb** for polling Hipadaba nodes and **script** for running 
arbitrary poll scripts. Thus the registration syntax is:

    sicspoll add nodepath hdb intervall

with nodepath being the path to the Hipadaba node and intervall the polling intervall in seconds. An 
example:

    sicspoll add /instrument/counter/countmode hdb 30

For scripts the registration is:

    sicspoll add some-name script intervall script-name scriptpar....

where some-name is a sensible name provided by you, intervall is the poll intervall, 
script-name is the name of the Tcl script to execute and scriptpar being optional parameters 
to the script to be run. An example:

    sicspoll add hdbdetuppi script 90 updatehdbdet

#### Runtime Interface

Sicspoll understands a number of object commands. 

    sicspoll list

prints a listing of all the entries in the poll list.

    sicspoll del name

deletes an entry from the poll list. 


    sicspoll intervall name [value]

queries or sets the polling intervall for entry name. 


    sicspoll poll name

manually invokes a single polling operation on the entry name. 

