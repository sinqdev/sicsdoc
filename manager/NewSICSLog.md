### SICS Logging

As of 2016 SICS logging has been reworked. Before 2016 SICS had three different log facilities namely 
the server log, the command log and the trace log. All had different file formats. This has now been 
consolidated into a single log with a standardized structured file format. A log entry now looks like 
this:

    timestamp:subsystem:severity:message

The *timestamp* is a human readable timestamp which denotes the date and time of the log entry.

*Subsystem* annotates to which sub system the log message belongs to.  The following sub systems 
are known now:

     sys

System messages from SICS. Things like connects/disconnects and internal messages.

     com

This sub system contains the log entries which used to be printed into the command log. This is 
possibly the most important sub system for a user.

     asqio

Messages from the ANSTO asynqueue system

     io

This is the sub system used when logging of device I/O has been enabled. 

     dev

General device messages

     par

This sub system is for logging parameter changes.

     notify

This is for event notifications. Events like value changes or other SICS events.

     history

Successfull commands are logged into this sub system. This log can provide the data for a future 
implementation of command line completion.


The *severity* determines the importance of the log message. A number of severity codes are understood: 

     fatal

A fatal error which means that SICS has given up on something. 

    error

An error occurred

    warn 

A warning was issued

     info

An informational message

     verbose

More verbose output giving more details

     debug

Full output for logging everything.


*Message* is the actual text of the log message. This may contain further fields separated by colons. 


In order to control log output the default log level is set to *info*. User commands allow to set the global 
log level or the logging level for specific sub systems. 


The default (and guaranteed) output of logging is an ASCII text log. As usual the log files live in the 
log directory of the instrument account. The file names of the log files follow a naming scheme:

     instlog-timestamp.log

where inst stands for the instrument and timestamp for the date and time when the log file was started. 
The logging system creates a new log file either when SICS is restarted or after 100000 entries have been 
logged. In order to control disk space, log files older that a month may be deleted. 

Log entries will also be written into a Mongo NoSQL database. This allows for easier querying of the 
log. Logging to MongoDB is considered experimental. 


#### SICS Logging Commands  

These are commands which can be excuted within SICS in order to control log output and other things. 


     log severity subsystem message

This is a command with which a user can add log messages from the command line or from scripts to the 
SICS log. Severity and subsystem must be given, with values as described above. All the rest of the 
command line is concatenated to constitute the message.

    logconfig filename

This command shows the name of the log file which is currently in use.

    logconfig flush

Unix buffers file output for performance reasons. This command flushes the bufers and forces all pending 
log output to file.

    logconfig close

Forces a close of the log file and the start of a new log file. 


    logconfig level [val]

Queries or configures the default log level. Valid values are the servity codes given above.


    logconfig enable [subsystem]

Enables full logging for the specified sub system. Valid values for sub system are listed above in the 
description of sub systems.

    logconfig disable [subsystem]

Disables full logging for the specified sub system. Valid values for sub system are listed above in the 
description of sub systems. After this command the default log level applies again. 


    logconfig listenabled

This command lists the enabled sub system.

    logconfig maxlines [val]

Sets or queries the maximum number of lines before a log file is rotated.

    logconfig logtemplate [val]

Queries or defines the template for log file names. The times stamp and the extension are automatically 
added  by SICS. As a side effect, this commands starts logging.


Log output to the mongo database is the same as to the log file as configured via the logconfig 
commands. Some commands are provided to configure and to interact with the mongo database. 

    mongoconfig open mongourl instname

Configures logging to the Mongo database. mongourl is the URL of the mongo database server, instname 
is the instrument name. The latter defines the database to which logging entries are written on the 
MongoDB server.

    mongoconfig reopen

Reopens a MongoDB connection

    mongoconfig close

Closes a connection to the mongo database. Do not use, the command just exists as a fix for the 
case that we experience a breakdown of the mongo database.

    mongonfig status

Displays the status of the mongo database connection. 


    showlog -l val -f fromtime -t totime -c severity -s subsystem -h -e expression

This interesting command allows to query the mongo database for log entries from within SICS. It is 
operated with options, in the traditional unix style. The following options are supported:

    -h 

Help, prints a short help summary.

    -l val

Prints the last log entries. Val is a value in minutes. By appending h to val, the value is in hours, 
by appending d it is in days. 

    -c severity 

List only log entries from severities higher or equal then the severity specified. Valid severity 
values are given above.

    -s subsystem

Prints only log entries from the subsystem given. Valid sub system values aer given above. 

    -f fromtime

prints only log entries starting from fromtime. Fromtime can be a date and time specification. This is 
programmed to be fairly lenient about date/time format encodings. But this leniency has limits. 

    -f totime

prints only log entries until totime. Is to be used in conjunction with fromtime to select a time intervall. 
The same information about date/time strings holds true to.

    -e expression

Allows to search for a text morsel in the log messages. Only matching entries will be printed. 


    commandlog tail val

This is short cut for showlog -s com -l val in order to mimic the operation of the older commandlog tail 
command.


#### Unix Commands for SICS Logging

Having the SICS log in a database allows to have some useful unix tools to interact with the log. 
This section documents such commands. Please note, that these commands will not work anywhere in 
PSI. Because I firewalled the mongo database server in such a way that it is only accessible within 
SINQ and the WHGA office network. Thus if any commands below fail with a rejected connection to 
the mongo database, try again on one of the SINQ machines. 


    sicslog  -i instrument -l val -f fromtime -t totime -c severity -s subsystem -h -e expression 

Works and acts the same as the showlog command described above. There is one additional option, -i 
which selects the instrument for which the log shall be queried. 


    errorsummary

Prints a count of errors which occured on the previous day for each instrument.  

    errorstatistics instrument

Prints a frequency count of errors in the previous 24 hours sorted by message for the specified instrument. 
Helps to locate the culprit when excessive errors turn up in the log. 




 

 
