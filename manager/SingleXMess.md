#### Four Circle Measurement Module

One of the problems of the old four circle setup was that measuring data
sets was rather inflexible as everything was done in C code. This new
module tries to solve this by just providing the basic services
necessary to support scripts which do the actual work. In order to do
so, the new module provides for:

*   A list of reflections to measure, available in SICS as **messref**
*   Some methods to manage the old style SICS files.
*   The table of scan parameters to allow for two theta dependent scans.

The general idea is that the list of reflection to measure is loaded
from possibly various sources and then processed in different ways by
scripts.

This module has by default the SICS name **fmess**. This module has the
following parameters:

**weak**

a flag which can be 0 or 1 to indicate if one shall check and remeasure
wek reflections

**weakthreshold**

a threshold for considering a reflection weak. The algorith searches the
min and the max in the scan data and then compares max - 2\*min \>
wekathreshold.

**mode**

The count mode: Monitor or Timer

**scanobj**

The name of the scan object for scanning.

**template**

The name of the template file for generating the header of the profile
file.

**hkllim**

Limits in HKL for index generation in the sequence: hmin kmin lmin hmax
kmax lmax.

**sttlim**

Limits in two theta for index generation. In the sequence sttmin sttmax.

The following commands are supported:

    fmess start filename

opens a new set of TRICS data files deriving the filename from the
filename given.

    fmess close

closes a set of TRICS data files.

    fmess store h k l stt om chi phi

store a measured reflection in a TRICS data file.

    fmess scanpar twotheta

get the scan parameters for a given two theta as a comma separated list.

    fmess weak

tests if a reflection is to be considered weak and has to be remeasured.
This works only for single detector mode.

    fmess table list

print the table of scan parameters

    fmess table add sttend scanvar step np preset

configures a two theta range. which ends at sttend. Scanvar can be
either om, o2t (for omega two theta) or o2t:fac where fac is the factor
to use for the coupling between two theta and omega. Step is the step
width, np is the number of points and preset is the count preset to use.

    fmess table del num

deletes the num's entry of the table.

    fmess table clear

deletes the whole table.

    fmess indgen 0|1|2

generate indices with the current parameters. If the parameter is 1 then
symmetrically equivalent reflections are suppressed if it is 0 they stay
in the list. If the parameter is 2, then a list containing the
reflections forbidden by the space group are generated.

    fmess genw hw kw lw

generate additional reflexions based on the indgen reflections with the
vector hw, kw, lw added. This is for handling inconsumerate structures.
