### Special FOCUS Initializations

These initailizations are special to the FOCUS instrument:

    InstallFocusmerge datafile

Installs the module which is responsible for merging focus data into
merged detector banks.

    MakeFocusAverager average hmc

Installs the average command and the focusraw command into SICS which
are used by the FOCUS status client in order to display processed
histogram memory data.

#### Special Internal FOCUS Support Commands

    focusraw bankid

Dumps in UUencoded form the content of the detector bank bankid. This is
required in order to add the TOF-binning to the data and in order to
handle the virtual merged detector bank which is built from
contributions of the three physical detector banks.

    average start stop bankid

Sums the detectors between start and stop of detector bankid into a
histogram. A standard display in the status client.

    focusmerge puttwotheta nxscriptmod bankname alias

Writes two theta values for the detector bank bankname into the file
described by the nxscript module nxsciptmod to the alias alias. A helper
function for data file writing.

    focusmerge putmerged nxscriptmod alias

Writes the virtual merged detector bank into alias in nxscriptmod.

    focusmerge putsum nxscriptmod bankname alias

Writes the summed counts for detector bank bankname under alias into
nxscriptmod.

    focusmerge putelastic nxscriptmod alias theoelastic

Calculate the position of the elastic line and write it to alias in
nxscriptmod. If no elastic line can be calculated, the theoretical
elastic line, theoelastic, is used.
