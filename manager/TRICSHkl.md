#### Index and Angles Calculations

TRICS originally had the hkl module which is responsible for calculating
angles. This is still true, though some functionality is now with the
singlex module. Driving reciprocal space now happens through driving the
virtual motors h,k, and l. Still, for reasons of backwards
compatability, the hkl commandset has been maintained.

    hkl list

prints the UB and other important information.

    hkl current

prints the current position in reciprocal space.

    hkl setub ub11 ub12 ub13 ub21 ub22 ub23 ub31 ub32 ub33

sets the UB matrix. Deprecated, use singlex ub.

    hkl nb [val]

sets the normal beam flag. Deprecated, replaced by singlex mode.

    hkl quadrant [val]

sets or prints the chi quadrant value.Can be 0 for low chi, or 1 for
high chi.

    hkl calc h k l [psi] [hamil]

prints angles for the indices h, k, l

    hkl drive h k l [psi] [hamil]

Drive h, k, l. Deprecated, use drive h val k val l val:

    hkl run h k l [psi] [hamil]

starts driving to h,k,l. Deprecated, use run h val k val l val.

    hkl hm [val]

sets or prints the histogram memory flag. This is special for TRICS. If
1, it checks if a reflection is visible on any of the three detectors at
0, 45 and 90 two theta offset. This is also deprecated, as TRICS has
lost many of its detectors.

    hkl fromangles stt om chi phi

calculates the indixes h,k,l from angles.

    hkl bitonb stt om chi phi

calculates normal beam geometry angles from bisecting angles.

    hkl calctth h k l

calculates two theta from the cell constants and HKL
