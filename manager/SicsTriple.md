### SICS Triple Axis Software

This is an description of how the SICS triple axis software works
internally. SICS triple axis software is quite complex because of the
various layers of software involved, the need to humour the oldtimers
who ask for the ILL TASMAD syntax and because of the inherent complexity
of the instrument. RITA-2 with ist multiple modes of measurement adds
another layer of complexity.

#### The C Language Layer

The triple axis machine needs sophisticated crystallographic
calculations in order to adjust monochromator and analyzer energies and
in order to handle reciprocal space. All this is implemented in C. The
following files are relevant:

-   tasublib.h,c The implemenation of the calcuations.
-   tasub.h,c The [SICS interpreter interface](tasub.md) to tasublib.

If the triple axis calculations go wrong, this is the place to look. But
beware: triple axis calculations are sensitive to bad parameters. Thus
before trying to fix a problem here, first verify that all parameters
affecting the calculations have appropriate values. There is also a
known non-feauture: if you try to calculate a UB matric from two
reflections which were measured with different values for the sample
scattering sense, this will fail.

Normal scanning at the triple axis instruments adds the TASMAD ILL data
format, polarization handling and means to control the scan output to
the normal SICS scanning codes. The individual operations of the SICS
scan module can be overloaded. This must be configured for TAS. The
actual code for the above mentioned operations is implemented in
tasubscan.h, c. So this must be touched in order to fix problems with
the ILL data format or standard scanning in general.

#### The Tcl Scripting Layer

One requirement was to have the command syntax of SICS mimic the ILL
TASMAD syntax as closely as possible. This syntax adaption was realized
in server side Tcl. Most of this stuff is implemented in the file
tasscript.tcl. This code handles some complicated things which of course
can go wrong:

-   The syntax of the **dr** and **sc** commands is translated into SICS
    syntax here. This code also implements the Q-E energy grouping of
    parameters. For example: **dr qh 1 1 0** is translated into drive qh
    1 qk 1 ql 0.
-   Setting zero points the MAD way is complicated too. Look at routine
    madZero if you do not like the results.
-   Changing the scattering sense involves some tricky changes to zero
    points and limits. This is in routine scatSense.
-   The initialization code to make SICS run the TAS way is mostly in
    this file too.

#### NeXus-TAS Files

SICS TAS can be configured to write NeXus files in the new NeXus TAS
file format. Most of this code lives in nxtas.tcl. NeXus requires that
arrays with positions to be written for scannable parameters. In order
to do this, writepoint functionality of scan was replaced by Tcl version
which appends all required positions to internal tcl arrays, besides
doing its normal job, printing scan progress. When a scan starts all old
scan data must be discareded, this is done in xmlprepare. When the scan
is finished all this data is dumped into a NeXus file through the
routine xmltasfinish and the routines it calls. TODO: this must be
changed to dump after any scan point.

#### RITA-2

RITA-2 adds several other issues to the already complex TAS software.
RITA-2 has an analyzer with 9 blades and a PSD instead of a single
counter. Together this allows for energy dispersive measuring modes. All
this is implemented in server side Tcl in files rita2com.tcl,
tassscript.tcl which for RITA should really be renamed to
ritatasscript.tcl.

One of the first issues is with the RISOE ECB hardware: 8 motors share a
rack which has only one power supply. This has the consequence that only
one out of the eight motors can run. Now, the problem arises how to
serialze driving motors in the same rack properly without the user
needing to worry about this. This is done in SICS by registering all
motors affected with the anticollision module which in turn calls a
script which does the serializing. All this code lives in the instrument
control file rita2.tcl. One caveat of this is that all affected motors
AND their aliases have to be registered with the anticollision module.

There is a special command to deal with the ECB manual motor box. This
is called manbox and lives in rita2com.tcl.

The ECB systems supports a parameter TV. The configuration of that can
be loaded, when required, through the command **loaddisplay**. This
script loads a suitable lof of ECB commands as emitted by tascom. No
hacking can be done here if it does not work as there is no
documentation, neither at PSI or RISOE, about how this works.

RITA uses a PSD for counting. Thus the original data to be stored is a
3D array with the detector dimensions and the scan point number as the
third dimension. The ILL TASMAD file format cannot handle this;
therefore RITA writes NeXus files only. The 3D detector dataset has to
built up during a a scan. To this purpose ritawritepoint in rita2com.tcl
contains code to store the HM data from the last scan point into a
sicsdata object. This is a general purpose data container in SICS. This
sicsdata is then dumped to file later on.

The PSD has another consequence: all counting operations must operate on
the HM. This has been done through modifications to the implementations
of the **count** and **co** commands. For scans this is implemented
through ritacount which is configured to replace the normal scan count
operation. Please note, that ritacount currently does not handle
polarization mode.

Another consequence of the RITA design is that the analyzer blades and
the analyzer assembly rotation have to be adjusted according to the
current RITA measurement mode when driving in Q-E space. The analyzer
assembly rotation would be the A5 motor in a conventional TAS. In order
to implement the RITA modes this has been mapped to a simulated motors
in order to trick the tasub module which needs this motor. Driving the
RITA analyzer is achieved by calling the routine adjustritaanalyzer from
the code for the **dr** command and from the routine ritadrive for
driving during scans. WARNING: when using the traditional SICS **drive**
command and not **dr** adjustritaanlyzer is NOT called and the analyzer
may be off! The RITA analyzer mode is set through the routine ritamode.
Possible modes are:

* **flat** flat monochormator
* **focus** focussing monochromator
* **monoim** monochromatic imaging mode

The first two modes resemble traditional TAS operations and pose little
problems. In monchromatic imaging mode, however, each analyzer blade
illuminates a different area on the PSD and needs to be treated
differently. In order to locate the detector counts for each analyzer
blade windows can be defined for each of them. The counts for an
analyzer blade is then the sum of the PSD counts in the matching window
for this blade. All this has a couple of consequences:

-   In all modes the counts in window 5 must replace the counts in the
    scan data structure. This is required in order to make scan data
    processing like locating peaks work. Moreover this allows the
    topsistatus to display a reasonable plot during scans. The code to
    do this is in ritacollect which replaces the normal collection
    routine of the scan module.
-   In monochromatic imaging mode arrays with the blade positions and
    the counts in the analyzer blade windows must be stored. This
    happens through routine ritawritepoint.
-   The analyzer blade data must be stored in the NeXus file. This
    happens in writeRitaDetector.

The analyzer calculation depends on a lot of variables. If they are
wrong: shit happens! Check first before trying to fix the code.

In order to support the WWW-status display for RITA, the wwwsics
commands formats a html table with interesting values. This can be
changed if other information shall be displayed there.

In order to use the SansStatus with RITA-2 the routines hmframe and
statusinfo have to work properly. The latter controls the data in the
text display of SansStatus. This can easily be edited if other
information is required.

In order to make TopsiStatus work for RITA-2 the scan Tcl procedure in
tasscript.tcl and scaninfo in rita2com.tcl must be there. The Tcl scan
procedure shall not be confused with the real scan module, it just
implements an interface to scan which TopsiStatus expects.

##### AutoWin

For the capricous RITA there is an autowindow command. It is installed
into SICS with:

    MakeRitaWin name RitaWin

Autowin analyzes a detector image and tries hard to find suitable
windows for the Rita analyzer blades. This, of course, only gives
sensible results in monochromatic imaging mode. Two subcommands are
supported:

    autowin calc [hmname]

calculates the windows from the data currently in the detector. The
optional parameter hmname is the name of the histogram memory and
defaults to hm which is right for RITA. The command outputs a list of
windows: one window per line, per line a komma separated list holding
xmin, xmax, and the center of the window.

    autowin load y1 y2

recalculates the windows and updates the window2-8 with the values
found. The parameters y1 and y2 descibe the extent of the window in the
y direction. This load command is a scripted command which may need
modification when another analyzer is installed at the Rita.

Autowin does the following steps:

1.  It sums the detector in y to give a 1D histogram
2.  Peaks are searched in that histogram
3.  Foreach peak found, the COG is calculated.
4.  The windows are the peak centers thus located +- 5 bixel.

The whole thing has an accuracy of +- 1 pixel.
