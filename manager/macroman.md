### Programming SICS Macros

The SICS server has a built in macro language. This macro language is
basically John Ousterhout's Tool Command Language Tcl. Tcl is described
elsewhere. A sound knowledge of Tcl is required for programming SICS
macros. The SICS macro language can be used for the following purposes:

-   Add hoc measurement procedures.
-   Trial measurement procedures.
-   Syntax adaptions to one's own favourite syntax.
-   Building of more complex commands from the SICS primitives.

The general procedure for defining a macro requires defining the macro
in a new file, source this file from the configuration script and the
use of the Publish command to make it available. New commands can best
be defined as Tcl procedures, but the obTcl object oriented extension to
Tcl is known to work as well. The SICS macro language allows to access:

-   Most Tcl commands.
-   All SICS commands.

In the following sections a few pecularities of the SICS macro system
will be discussed.

#### Input/Output

It would be quite verbose and confusing for the user if all output from
SICS commands called from a macro would appear on the screen during
macro execution. Therefore all normal SICS output to a client is
suppressed while executing a macro. Except error messages and warnings
which will always be written to the client executing the macro. The
output of a SICS command is available within the macro script through
the normal Tcl mechanism as a return value. This allows for processing
of SICS output within a macro. If the output to the client executing the
macro is required this can be done with the ClientPut command, detailed
in the user documentation.

#### Error Handling

Tcl has the feature that it aborts execution of a script when an error
occurs. If a macro script needs to handle errors either from Tcl or from
SICS commands this can be achieved by using the Tcl catch mechanism.

If things are seriously wrong or the users wishes to interrupt an
operation SICS interrupts are used. Scripts implementing measurement
procedures may need to test and even modify interrupt values. A script
can inquire the current interrupt value of the connection with the
command **GetInt**. If a script can handle an error condition it may set
the interrupt on the connection object with the **SetInt** command. The
textual representations of interrupts for these commands are: continue,
abortop, abortscan, abortbatch, halt, free, end.

#### Interacting with SICS within a Script

There exist a few commands which allow to inquire or manipulate SICS
internals. Most of these commands are only available in macro scripts.

    SICSType thing.

SICSType lets SICS find out if thing has some meaning within SICS.
Possible return values are: DRIV for a drivable variable, COM for a SICS
command, NUM for a numeric value and TEXT for anything else.

    SICSBounds var newval

SICSBounds checks if newval violates the hardware or software limits of
the variable var.

    SetStatus newval

SetStatus sets the SICS status line to a new value. Possible values for
newval are: Eager, UserWait, Count, NoBeam, Paused, Driving, Running,
Scanning, Batch, Halt, Dead.

    SICSStatus var

SICSStatus returns a integer value representing the current status of
the object var. var must be a drivable or countable object. The integer
code returned are defined in the SICS programmers documentation.

#### SICS Interfaces in Tcl

Work has begun to implement SICS internal interfaces in Tcl. This opens
the port for writing even device drivers in Tcl. Another use is to
define virtual motors quickly in Tcl. At the time of writing, July 2005,
this is only developed for the object interface and the drivable
interface. For the meaning of internal SICS interfaces please consult
the SICS programmers documentation. Be warned: with the feautures
described in this section, you can mess up SICS badly.

#### The Object Interface

    MakeTclInt name

Creates an object name. This object then understands the following
commands:

    name savescript scriptname

Configures a script which will be called when it is time to dump the
status of the SICS server. This script will be called with the name of
the object as its only parameter.

    name backup bla bla bla....

To be used from savescripts. Writes everything behind backup into the
status file.

The use of this facility is to place special commands into the status
file which may, for instance, request calculations to be made or drive
parameters not caught in the standard SICS objects to special values.
For example: at SANS2 this is used in order to store attenuator and
collimator values. Both are implemented as scripted commands and thus do
take part in the standard SICS object saving scheme.

#### Overriding the Drivable Interface with Tcl

The drivable interface of any given drivable object can be overriden
with tcl functions. This includes an object created with MakeTclInt. The
syntax is:

    TclReplaceDrivable objname key scriptname tclName

This replaces the drivable interface function defined by key with the
script scriptname in the driveable interface of the SICS object object.
tclName is the name of an arbitrary Tcl object which can hold user data.
Possible function keys and their function signatures are:

    halt

haltscript, no parameters

    checklimits

checklimitsscript targetvalue

    setvalue

setvaluscript targetvalue

    checkstatus

checkstatusscript, no parameters

    getvalue

getvaluescript, no parameters

All procedures, except getvaluescript, are supposed to return the
approriate SICS return codes (HW\*) as integer numbers. Getvaluescript
is supposed to return the position of the device.

    TclDrivableInvoke objname key

A debugging aid: Invokes the scripted function denoted by key of the
object objname and prints the results. The function keys are the same as
given above.
