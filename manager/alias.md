### Aliases in SICS

SICS knows three different kinds of aliases: object aliases, runtime
aliases and command aliases. This is confusing but finds its explanation
in the structure of SICS internals.

#### Object Aliases

An object alias is another name for a first class object installed into
the SICS interpreter. For instance a second name for a motor. For
instance the motor twotheta is quite often aliased to a4. Such an alias
can be used like a normal SICS objects. Even in commands which access
internal SICS interfaces like the drive command or others. Object
aliases are installed into SICS with the SICSAlias command:

    SicsAlias oldname newname

This command installs newname as alias for the object oldname.

SicsAlias can only be used within initialization scripts. SicsAlias is
considered deprecated and can be replaced with the superior runtime
aliases described below.

#### Runtime Aliases

Runtime aliases are full object aliases which can be configured into the
system at run time by a SICS manager. The syntax looks like this:

    DefineAlias aliasname SICSobject

This defines aliasname to be the alias for the SICS object SICSobject.
It is not needed that SICSobject already exists. If SICSobject is
already an alias, it is translated before definition. Multiple
translation is possible, depending on the order of definition. When an
alias is used, and it does not point to an existing object, the
behaviour is the same as if an unknown object would have been used.

    DefineAlias aliasname

This command deletes the alias aliasname.

#### Command Aliases

Command aliases are shortcuts for lengthy commands. For instance one
might want to define A4LL as a shortcut for "a4 softlowerlim". This is
just to save typing or adapt SICS to MAD users who appear to have an
unlimited memory for 2-4 letter acronyms. It is possible to redefine a
SICS object with this for instance to define tt as an alias for
temperature. However if one tries to use tt in a drive command it will
fail because it is just a text replacement. A command alias can be
installed into SICS at any time with manager privilege and the command:

    alias shortcut bla bla bla ....

This define shortcut as an alias for everything behind it.

A shortcut may take parameters.
