#### Simple Indexing

This is a very simple indexing module for neutron scattering. If a user
arrives at a neutron scattering single crystal diffractometer she
usually knows the lattice constants and other crystallographic
parameters quite well. Thus a full blown indexing routine which tries to
find the cell constants and the spacegroup symmetry as used at normal
four circle diffractometers is not needed. Especially as such routines
usually require 10-20 reflections in order to be work properly. In
neutron scattering one rathere wishes to find a minimum of two
reflections and have a UB quickly. This is the purpose of this module.

The algorithm is simple.

1.  From the list of reflections three non coplanar reflections at low
    two theta are selected. Low two theta because then the number of
    possible indices is low.
2.  For each reflection possible indices are generated for which the two
    theta calculated from the indices and the cell constants matches the
    measured two theta within a user defined limit.
3.  Each combination of indices is then checked for two conditions:
    *   The angles between the scattering vectors must match the angles
        calculated from the lattice constants and the indices.
    *   The indices must build a right handed coordinate system.

4.  Each matching set of indices constitutes a possible solution to the
    indexing problem.

The algorithm works with two reflections as well. But the test for right
handeness is not possible then.

##### Prerequisites

1.  The cell constants were given through *singlex cell*
2.  The spacegroup was defined through *singles spacegroup*
3.  The lambda value is correct
4.  Two, better three, reflections had been centered and entered into
    the reflection list.

##### Commands

    simidx sttlim [val]

sets and queries the two theta limit for considereing an index set.

    simidx anglim [val]

sets and queries the angle limit for considereing a match between
calculated and observed angles between vectors.

    simidx run

runs the indexing algorithm. If all is well a listing of possible
solutions is printed. The list incudes a goodness of fit, GOF. This
number is a sum of all differences in two theta and angle multiplied by
100. Smaller is better in this case.

    simidx choose n

Choose one of the solutions resulting from a previous run of simidx. A
UB is calculated and set.

    simidx dirax filename

write a dirax reflection file to filename.

    simidx indxref

indexes the current reflection list with the current UB.
