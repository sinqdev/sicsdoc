### The SICS Online Help System

This help system is installed at SANS. For all other instruments the help 
files were never written......

SICS has a simple built in help system. Help text is stored in simple
ASCII text files which are printed to the client on demand. The help
system can search for help files in several directories. Typically one
would want one directory with general SICS help files and another one
with instrument specific help files. If help is invoked without any
options, a default help file is printed. This file is supposed to
contain a directory of available help topics together with a brief
description. The normal usage is: help topicname . The help system will
then search for a file named topicname.txt in its help directories.

A SICS manager will need to configure this help system. A new directory
can be added to the list of directories to search with the command:

    help configure adddir dirname

The default help file can be specified with:

    help configure defaultfile filename 

Each of these command given without a parameter print the current
settings.
