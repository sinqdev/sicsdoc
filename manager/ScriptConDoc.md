### ScriptContext Controller Documentation


ScriptContext describes a system for asynchronous I/O in SICS. The
scheme is tightly integrated with the new SICS object model based on the
hipadaba parameter model. ScriptContext tries to solve a couple of hard
problems:

-   The performance of the SICS server drops if it has to wait for a
    hardware device to respond. As some devices take up to 10 seconds or
    more to respond, this is significant. If SICS has to wait for a
    response then a device going offline may cause SICS to hang. All
    these problems can be solved when all I/O is asynchronous. This
    means a request is sent and SICS handles the reply only when it is
    there. SICS does not block for I/O.
-   For slow devices, parameters must be cashed in SICS in order to
    gurantee a quick request response time. This implies that a
    mechanism is required which updates cached parameters in the
    background.
-   Most devices can handle only one command at a time: they follow a
    request-response pattern. This implies that the access to the device
    has to be serialized. It is also desired, that a set request for a
    parameter takes precedence over the update request for a cached
    value. Thus an intelligent, prioritized serialization is required.
-   Many devices share the same line protocoll, for example \<cr\>\<lf\>
    terminated strings. What is always different is the exact formatting
    of the command and the interpretation of the replies. Rather then
    coding this in C for any device, the idea is to defer the generation
    of command strings and the interpretation of replies to scripts.
    This makes it much easier to develop drivers for new hardware.
-   In an asynchronous I/O world we already need at minimum two scripts
    for any one transaction: a command generating script and a response
    evaluation script. These scripts form a script chain because they
    are called in sequence.
-   Some devices need to go through a series of state changes in order
    to do a given operation. If we already have the concept of a script
    chain, why not lengthen the chain to handle such operations too? Or
    send additional commands to correct errors?

All these complex tasks are handled by the scriptcontext system. The
remainder of this document details the SICS interface to the script
context system. This is targeted towards SICS managers trying to
introduce new devices into SICS. There is another
[document](NewSciptContext.md) which describes internals of the
implementation.

#### Initialisation

Two objects are needed at any given time:

1.  A SICS object which holds parameter nodes.
2.  A scriptcontext controller object. Parameter nodes can be registered
    with the scriptcontext controller. The scripcontext controller then
    takes care of all the communication with the hardware: forwarding
    set commands to the hardware, updating the node value from the
    hardware etc. It does so without ever blocking.

The commands in detail:

    makesctcontroller name protocol args

make a new SctController object with name name. Initialize protocol
protocol with the arguments given. The protocol std with
 crlf is the default and serves as an example.

    makesctcontroller name std hostport (sendterminator timeout replyterminator)

creates a scriptcontext controller for the crlf protocol
family. Hostport is a combination of computer:port which are the
connection details for TCP/IP. Optionally specifiy a timeout and a
sendterminator.

More information on protocol drivers can be found in this [document](ScriptConProt.md). 


    MakeSICSobj objname classname

make a new SICS object with the name objname. More information on second generation SICS objects 
can be found in [this document.](ScriptDriverReference.md)

#### SctController Commands

Each command must be prefixed with the name specified in the
initialisation. The placeholder for this is sctcon below.

    sctcon poll nodename (intervall prio action)

add the node nodename to the polling queue. Optionally specify a polling
intervall in seconds, a proprity and an arbitray string, action,
indicating the initial state. Defaults are 5 for intervall, read for
priority and action. Possible values for prio are: read, progress, write
and halt. This can be called multiple times for a given node and then
changes the intervall, priority etc.

    sctcon unpoll nodename action

removes the node nodename from the polling queue for action action. 

    sctcon queue nodename (prio action)

adds the node nodename to the queue for one time execution. Optionally
specify a priority and an arbitray string, action, indicating the
initial state. Defaults read for priority and action. Possible values
for prio are: null, slow, read, progress, write and halt. This has no
effect if the node has already been queued with the same priority and
action.

    sctcon write nodename

Add the necessary callbacks to node nodename so that set commands on the
node happen through this controller.

    sctcon transact data

Send data directly to the controller and print result. Please note that
multi word commands must be quoted. This command waits until the reply
has been received.

    stcon send data [prio]

Queues data as a command for later execution. The default is to use
write priority, but the priority can be changed by specifyng an optional
priority parameter.

    sctcon debug (0|n|-1)

debug flag, -1 = off, 0 = scriptcontext verbose , \> 0: make n steps
only.

    sctcon disconnect

disconnects the controller. It will automatically try to reconnect ASAP.

    stcon statistics

prints statistics about the communication: average and max times per
transaction, per invocation of the handler etc.

    stcon log filename|close

causes a debug log to be written to filename. The special filename close
closes the log file again. This is for debugging.

    sctcon processnode nodepath [action]

Starts processing of a scriptcontext connected node and waits for the
processing of it to end. The default action is read, but others can be
specified by setting the second parameter.

#### Node Processing

In order to link nodes with hardware parameters, the node is registered
with one of the commands above with an appropriate scriptcontext
controller. The programming model is a state machine living at the node
responsable for a parameter. Each state corresponds to a node property.
The value of such a property corresponding to a state is the name of a
script to evaluate when this state is active. These scripts can do
several things:

-   Scriptcontext places the last response from the device in the nodes
    result property. A script can evaluate the response and update the
    node value or error fields or whatever from it.
-   A script can decide to format a command to send to the hardware. The
    formatted command must be stored in the send property of the node.
-   A script must decide what to do next. It does so by returning the
    name of the next state. Idle stands for stopping the state machine.

In order to facilitate script writing scripts can use a special command
with the name **sct** in scripts. Sct can do:

    sct

returns the path of the current node

    sct controller

returns the name of the controller who controls this script.

    sct print

in write scripts: print a message to the connection which started the
operation.

    sct update value

updates the current node with the data in value. Also deletes the
geterror property.

    sct utime propertyname

store a time stamp in the property propertyname.

    sct propname

returns the value of the node property propname

    sct propname value

sets the node property propname to value.

#### Polling Nodes

In order to make nodes represent hardware values and to have them
updated through the scriptcontext mechanism node reading has to be
implemented. The following steps have to be performed:

1.  The nodes **read** property has to be set to the name of a script as
    described above. This script has to format an appropriate command
    for reading the value from the hardware and has to store this
    command in the nodes send property. It also has to return the next
    state to process with the reply from the device. The name of the
    state is arbitrary, as example let us take parread.
2.  The node property with the same name as the state defined in, for
    example parread, must be set to a script which evaluates the
    response from the device. This script has two tasks:
    1.  It reads the response from the device from the result proprty of
        the node. If the reponse indicates an error, the script must set
        the nodes geterror property to represent the error. If the
        response is OK, it has to update the node value with hupdate.
    2.  It must decide upon further actions to take as describead above.

3.  The node must be connected to the controller with the sct poll node
    command described above.

The system automatically sets the property read\_time on the node to the
time when the last read operation completed. The time is seconds
expressed as a double value with microseconds resolution.

Here an example. This is for a simple protocoll where the device expects
the name of the paramter and returns either: OK: value or ERROR: error
text.

    makesctcontroller farmser std localhost:7070
    MakeSICSObj farm TestObj
    #---------------------------
    proc farmparcom {par} {
    #------ set command
        sct send $par
    #----- return next state
        return parread
    }
    #------------------------
    proc farmparread {} {
        set rply [sct result]
    #--------- check and set error
        if {[string first ERR $rply] >= 0} {
        sct geterror $rply
        return idle
        }
    #-------- update node value
        set data [string range $rply 3 end]
        sct update $data
        sct utime readtime
    #--------- return to idle state
        return idle
    }
    #--------------------------
    hfactory /sics/farm/hase plain spy int  
    hsetprop /sics/farm/hase read farmparcom hase
    hsetprop /sics/farm/hase parread farmparread

    farmser poll /sics/farm/hase

#### Writing Node Values to Hardware

Having node values written to hardware follws a similar approach as
polling described above. The following things are required:

1.  If the node property check is set, its value is taken as a script
    which checks if the new value is possible. The check script can read
    the target property of the node and can either return an error or OK
    if the value is possible. Again: the check script is optional.
2.  A write script has to be stored as the node property write. This
    script is supposed to format a command for the hardware and store it
    in the send property of the node. This script also has to return the
    name of the next state which will process the reply.
3.  The reply processing script can set the seterror property if there
    are errors, issue other commands, or return idle when the operation
    is finished.
4.  The node has to be registered for writing with the sct controller
    with the **sctcon write nodename** command.

Again an example is in order. The protocol is again simple: par value is
the command understood by the device. This is added to the reading code
in the example above.

    #--------------------------
    proc farmcheck {} {
        set val [sct target]
        if {$val < -100 || $val > 100} {
        error "Value out of range"
        }
        return OK
    }
    #---------------------------
    proc farmset {par} {
        set val [sct target]
        sct send "$par $val"
        return setreply
    }
    #-------------------------
    proc farmsetreply {} {
        set rply [sct result]
        if {[string first ERR $rply] >= 0} {
           sct print $rply
        }
        return idle
    }
    hsetprop /sics/farm/hase check farmcheck
    hsetprop /sics/farm/hase write farmset hase
    hsetprop /sics/farm/hase setreply farmsetreply

    farmser write /sics/farm/hase

#### The Drivable Adapter

Some parameters under the control of the scriptcontext may need some
time to do what they are asked to do. In order for SICS to handle and
monitor this properly, a drivable interface must be provided. In order
to make this work some scripts have to be stored as node properties.
these are:

    checklimits

which is a script which checks if the operation is possible at all.

    halt

a script which stops the parameter.

    checkstatus

a script which tests if the parameter is still running and returns one
off idle, busy, posfault or fault depending on th results.

For such a node a drivable adapter can be created with:

    makesctdrive name path-to-node controller

creates a drivable object, name, connected to the node path-to-node. The
third parameter is the scriptcontext controller responsable for the
node.

    makesctdriveobj name path class controller

makesctdriveobj name type priv class controller

An example, based on the parameters given above. The slow parameter is
schnegge. While schnegge is changing, schneggerunning is 1, else 0.

    proc schneggechecklimits {} {
        return [farmcheck]
    }
    #-----------------------------
    proc schneggestatus {} {
        farmser queue /sics/farm/schneggerunning progress read
        set status [sct writestatus]
        switch $status {
        commandsent {
            set runtime [SICSValue "hgetprop /sics/farm/schneggerunning read_time"]
            set starttime [sct write_time]
            if {$runtime > $starttime} {
            sct writestatus evalcheck
            }
            return busy
        }
        evalcheck {
            set tst [hval /sics/farm/schneggerunning]
            if {$tst == 1} {
            return busy
            } else {
            return idle
            }
        }
        default {
            error "schneggestatus called in bad state $status"
        }
        }
    }
    #---------------------------------------------
    hsetprop /sics/farm/schnegge checklimits schneggechecklimits
    hsetprop /sics/farm/schnegge checkstatus schneggestatus
    makesctdrive schnecke /sics/farm/schnegge farmser

The basic principle is that the checkstatus function should look at some
status variable and based on that decide if the thing is still running
or not. In this example the status variable is
/sics/farm/schneggerunning. In order to do that, this variable must be
updated frequently. This is being taken care of by the **farmser queue
/sics/farm/schneggerunning progress read** command. This causes
schneggerunning to be updated with high priority. Please note that
multiple invocations of queue for the same parameter will be ignored. So
there is no harm done in doing this to many times.

Before checkstatus can look at the values of a status variable, two
conditions must be met:

1.  The command must actually have been sent
2.  After the command has been sent, the status variable must have been
    updated at least once.

The sctdriveadapter takes care of the first condition and sets a
property **writestatus** to **commandsent** when this is the case. Then
the checkstatus script must take over. Thus the checkstatus script reads
the property and acts accordingly: If the status is commandsent it tests
if the status variable has been updated and if so, sets the writestatus
to evalcheck. Please not that you may choose an arbitrary name here. In
the second half, in mode evalcheck, checkstatus checks the variable and
if it indicates success, terminates by returning idle. Other possible
return values of checkstatus are: busy, fault, posfault and idle.

#### Remarks

-   If you need to read one value but set another as with the Dornier
    choppers speed, the best way to accomplish this is to use a separate
    node. The appropriate reader routine must then also update that
    separate node
-   If things need to be done in sequence, for instance you want
    parameters updated after the end of a count or drive then this
    should be accomplished by adding this stuff into the script chain.

