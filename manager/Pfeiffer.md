### Pfeiffer Vacuum Measuring Device

This is installed at FOCUS to check the vacuum. This has a funny serial
protocol:

#### Normal Command

Host sends: command lf

Pfeiffer replies: ACK or NACK followed by cr lf

#### Read Command

Host sends command lf

Pfeiffer replies: ACK or NACK folloed by cr lf

Host sends: ENQ

Pfeiffer replies: data followed by cr lf

ACK is ASCII 6, hex 6

NACK is ASCII 21, hex 15

ENQ is ASCII 5, hex 5

The pfeiffer in SICS is supported through a special protocol handler
whose main purpose is support for sending the \<ENQ\> and a
scriptcontext driver in pfeiffer.tcl.
