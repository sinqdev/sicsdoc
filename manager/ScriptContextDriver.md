### ScriptContext Driver Overview

#### Introduction

So, you want to integrate a piece of hardware into SICS using the scripcontext 
system? Good luck! Go away, read the hardwares manual; you will need the info. 
You did this and you are back? Two things are needed:

1. A scriptcontext controller object. See below how to create one.
2. A second generation SICS object. There are several types: general purpose ones but also specific ones 
   like for motors or counters. See the section below for how to create such objects.

The second generation SICS object will have parameters. These parameters will be called nodes in this context. 
You can always configure additional nodes/parameters in order to add to a second generation SICS object. Now, these 
nodes have to be connected to the scriptcontext controller.  In order to do this, three things need to be done:

1. A scriptchain must be generated by writing little morsels of Tcl code. The Tcl codes formats the commands to send to 
   the device, analyses responses, updates nodes and such.  
2. The scriptchain must be connected to their respective nodes. This happens by setting appropriate node properties.
3. The node has to be registered with the scriptcontext controller. 

#### Scriptchains

This section answers what a scriptchain is and how it works. To this purpose let us trace a read request.

1. The scriptcontext controller wishes to update the value of a node by reading data from the hardware
2. The scriptcontext controller then looks for the **read** property of the node to be read. The value of the read 
    property must be the name of a Tcl script to call. 
3. The scriptcontext controller executes the read script. The read script formats a suitable command for reading the node value 
   from the hardware and tells it to the scriptcontext controller through **sct send**. The read script returns a name. This is the 
   name of a node property which as its value holds the name of the script to call for processing the response. This will be named 
   **responseproperty**.
4. The scriptcontext controller goes away, sends the formatted command to the device and waits for a reply from the device. 
5. In some stage the reply from the device is available. The scriptcontext controller then looks for the **responseproperty** property 
    at the node.
6. The scriptcontext controller then calls the script stored as a value of the **responseproperty**. This script can retrieve the reply through 
    a call to **sct result**. It then should scan the reply and update its node value through a call to **sct update**.  Now, the reponse script 
    has choices. If it is done, it returns idle and the scriptcontext controller finishes the processing of this node. But it can also choose 
    to format  new command and pass it to the scriptcontext controller with **sct send**. Then it has to return the name of a new **responseproperty** 
    to evaluate the result of this second command. 

Writing values to the device works in a similar way. Just the staring point is different: it is the value of the **write** property of the node. 

Thus the scriptchain is the sequence of scripts to call for processing a hardware operation. Please note that the chain may branch. This scriptchain is 
encoded in the return values of the scripts and the associated node properties. 

An example may be in order. The example assumes a simple server listening on port 8080 which allows to set and retrieve values.

      makesctcontroller farmser std localhost:8080 
      MakeSICSObj farm TestObj
      #---------------------------
      proc farmparcom {par} {
          sct send $par
          return parread
      }
      #------------------------
      proc farmparread {} {
          set rply [sct result]
          if {[string first ERR $rply] >= 0} {
          sct geterror $rply
          return idle
          }
          set data [string range $rply 3 end]
          sct update $data
          return idle
      }
      #---------------------------
      proc farmset {par} {
          set val [sct target]
          sct send "$par $val"
          return setreply
      }
      #-------------------------
      proc farmsetreply {} {
          set rply [sct result]
          if {[string first ERR $rply] >= 0} {
              sct print $rply
          }
          return idle
      }
      #============================================
      hfactory /sics/farm/hase plain spy int  
      hsetprop /sics/farm/hase read farmparcom hase
      hsetprop /sics/farm/hase parread farmparread

      hsetprop /sics/farm/hase write farmset hase
      hsetprop /sics/farm/hase setreply farmsetreply

      farmser poll /sics/farm/hase 60
      farmser write /sics/farm/hase

A walktrough:

The first line creates a scriptcontext controller named farmser using the std protocol on computer localhost at port 8080.

The second line creates a second generation SICS object named farm of type TestObj. 

The following lines define a number of functions to be used in the scriptchain. Please notice the calls to **sct send**, **sct result**, **sct update** and the return values of the functions.


Now to the  lines following #=========: The first line creates a node hase to farm. The next two lines install the read scriptchain for hase. Please notice how the return value for farmparcom matches the name of the second property set on /sics/farm/hase.

This is followed by two line configuring a write script chain on /sics/farm/hase. 

The line farmser poll connects the read script chain of /sics/farm/hase with the scriptcontext controller farmser.

The line farmser write connects the write script chain of /sics/farm/hase with the scriptcontext controller farmser.

#### Alternatives to Script Chains

SICS second generation objects were designed with script chains and the use of scriptcontext in mind. But essentially, second generation SICS objects are empty templates. By registering callback functions on the appropriate nodes they can do I/O with hardware directly or use any other system. But callback functions and callback registration will have to be done in ANSII-C for the time being.


