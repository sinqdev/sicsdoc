### SICS Data Module

This is a support module which provides a one dimensional array together
with functions to load data into it, read it and transfer it in either
zipped or uuencoded form. It was meant to be a general means for
transacting with data and to replace all those specialized uuwrite,
zipwrite etc. routines in the source code.

#### Initialization

Sicsdata objects can be dynaimcally configured in and out of SICS:

    sicsdatafactory new dataname

creates a new sicsdata object dataname

    sicsdatafactory del dataname

deletets the sicsdata object dataname.

In this way a new sics data object is created which understands a set of
commands, too.

#### Data Object Commands

In the following section, the identifier dataname refers to a SICS data
object created with the sicsdatafactory new command detailed above. The
SICS data object will always adjust automatically to the length of the
data required from it.

    dataname clear

clears the data buffer

    dataname used

prints the number of used elements in this data buffer

    dataname dump filename

dumps the data buffer into file filename as a long row of numbers.

    dataname dumpxy filename

dumps the data buffer into file filename. A line per value, index value,
to be plottable as X-Y plot.

    dataname putint pos value

writes an integer value at index pos.

    dataname putfloat pos value

writes a float to pos.

    dataname copyscancounts pos scan-object

writes the counts collected in a scan into dataname, starting at pos.
The scan data is searched for in the scan object specified.

    dataname copyscanmonitor pos monitor scan-object

writes the counts collected on monitor monitor into dataname starting at
pos. The values are searched for in the scan object specified.

    dataname copyscanvar pos id scan-object

writes the scan positions for scan variable id into dataname starting at
pos. The values are searched for in the scan object specified.

    dataname copytimebin pos hm

copies a time binning array from the histogram memory hm to dataname,
starting at pos.

    dataname copyhm pos hm start end

copies histogram memory data from hm to dataname, starting at pos.
Optionally start and end can be specified in order to copy a subset.

    dataname copydata pos data start end

copies data from another SICSdata named data into this array. The data
is copied starting at pos in dataname. The data between start and end is
copied from data.

    dataname copynode pos path [length]

copies data from a hipadaba node at path into hugo. Starting at pos.
Optionally, only length data items are written.

    dataname copytonode path start length

copy data from the sicsdata to the node described by path. Data in the
range from start till start+length is copied.

    dataname get pos

prints the data at pos.

    dataname divideby name

divides each element in dataname with the corresponding element in the
SICS data object name.

    dataname scale val

scale all values with a the scale factor val.

    dataname addto val

adds value to each data element in data.

    dataname intify

convert the data to integer.

    dataname writezipped name

write the content of dataname in compressed format to the current client
connection using name as identifier.

    dataname writeuu name

write the content of dataname in uuencoded format to the current client
connection using name as identifier.
